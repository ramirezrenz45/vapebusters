using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Initialization : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("StartGame", 0.5f);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("WorldMap-Draft");
    }
}
