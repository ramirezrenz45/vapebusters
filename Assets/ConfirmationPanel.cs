using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
public class ConfirmationPanel : MonoBehaviour
{

 
   
  public  UnityEvent EvtConfirm =new UnityEvent() ;
    public UnityEvent EvtCancel = new UnityEvent();

    public TextMeshProUGUI text;
  
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Oncancel()
    {

        EvtCancel?.Invoke();
        EvtConfirm.RemoveAllListeners();
        EvtCancel.RemoveAllListeners();
    }

    public void onConfirm()
    {
        EvtConfirm?.Invoke();
        EvtCancel.RemoveAllListeners();
        EvtConfirm.RemoveAllListeners();
    }
}
