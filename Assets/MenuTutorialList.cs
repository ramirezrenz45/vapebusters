using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MenuTutorialList : MonoBehaviour
{
    public List<DictionaryItems> TutorialList;
    public List<Sprite> sprites;
    public GameObject ButtonPrefab;
    public Image image;
    public GameObject backButton;
    public GameObject NextButton;
    public int index=0;
    List<GameObject> ButtonInstatiated= new List<GameObject>();
    [SerializeField] GameObject listPanel;
    // Start is called before the first frame update
    void Start()
    {
        sprites = TutorialList[0].sprites;
        foreach (var item in TutorialList)
        {
            GameObject buttons = Instantiate(ButtonPrefab);
            buttons.transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>().text = "<indent=15%>" + item.name;
            buttons.transform.SetParent(listPanel.transform);
            buttons.GetComponent<Button>().onClick.AddListener(() => Onselect(item));
            ButtonInstatiated.Add(buttons);
        }
    }

    public void OnOpen()
    {
        sprites = TutorialList[0].sprites;
        OnSetUp();
    }

    public void OpenTutorial()
    {

    }

    public void Onselect(DictionaryItems items)
    {

        sprites = items.sprites;

      
        OnSetUp();
    }
    public void OnSetUp()
    {
        index = 0;
        if (sprites.Count >= 0)
        {

            image.sprite = sprites[index];
            backButton.SetActive(false);
            NextButton.SetActive(true);
        }


        if(sprites.Count <= 1)
        
        {
            backButton.SetActive(false);
            NextButton.SetActive(false);
        }
     
    }
    public void MovePage(int x)
    {
        index += x;
        if (index >= 0 && index < sprites.Count)
        {
            image.sprite = sprites[index];
        }
        if (index <= 0)
        {
            backButton.SetActive(false);
        }
        else
        {
            backButton.SetActive(true);
        }
        if (index >= sprites.Count - 1)
        {
            NextButton.SetActive(false);
        }
        else
        {
            NextButton.SetActive(true);
        }
    
    
    
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[System.Serializable]
public class DictionaryItems
{
  public  List<Sprite> sprites= new List<Sprite>();
  public string name;
  public int x;
}
