using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingStation : MonoBehaviour
{
    GameObject confirmationPanel;
    GlobalEvents globalEvents;
    UIManager uIManager;
    BattleInfo battleInfo;
   [SerializeField]DialogSet dialogSet;
    private void Awake()
    {
        globalEvents = FindObjectOfType<GlobalEvents>();
      
    }

    private void Start()
    {
        battleInfo = FindObjectOfType<BattleInfo>();
        //if (!battleInfo.EscapeLocations.Contains(new Vector3(transform.position.x, 2, transform.position.z)))
        //{
        //    battleInfo.EscapeLocations.Add(new Vector3(transform.position.x, 2, transform.position.z));
        //}
        uIManager = FindObjectOfType<UIManager>();
        confirmationPanel = uIManager.confirmationPanel;
    }

    // Start is called before the first frame update
    public void InteractHealingStation()
    {
        confirmationPanel.SetActive(true);
        globalEvents.OnPause();
        ConfirmationPanel  confirm= confirmationPanel.GetComponent<ConfirmationPanel>();
        confirm.EvtConfirm.AddListener(Heal);
        confirm.EvtConfirm.AddListener(uIManager.CloseAllPanel);
        confirm.EvtCancel.AddListener(uIManager.CloseAllPanel);
        confirm.EvtCancel.AddListener(globalEvents.OnUnpaused);
        confirm.EvtConfirm.AddListener(globalEvents.OnPause);
        confirm.text.text = "Use healing station?";
    }

    public void clear()
    { 
   
    }
    public void Heal()
    {
        List<PlayerStatsTest> playerStats = battleInfo.PlayerStats;
        
        confirmationPanel.SetActive(false);
        foreach (PlayerStatsTest unit in playerStats)
        {
 if (unit)
        {
        
            unit.currentPlayerStats.CurrentHP = (int)unit.currentPlayerStats.MaxHP;
            unit.currentPlayerStats.CurrentSP = (int)unit.currentPlayerStats.MaxSp;
            Debug.Log("Player now has " + unit.currentPlayerStats.CurrentHP);

                FindObjectOfType<DialogSystem>().EvtDialogStart.Invoke(dialogSet);

        
            //increasestat
        }
        }
       
        confirmationPanel.GetComponent<ConfirmationPanel>().text.text = "";
     
     

    }
}
