using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyIfSave : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (SaveLoad.SaveExists("SavedData"))
        {
            Destroy(this.gameObject);
        }
    }

}
