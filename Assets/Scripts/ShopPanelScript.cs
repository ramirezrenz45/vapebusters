using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ShopPanelScript : MonoBehaviour
{
    public UnityEvent<ItemType> EvtShopSlotSelect = new UnityEvent<ItemType>();
 public   ItemType Item;

    private void Start()
    {

    }
    // Start is called before the first frame update
  
    public void OnSlotPressed()
    {
        EvtShopSlotSelect?.Invoke(Item);
    }
}