using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseUnit : MonoBehaviour
{
    BattleInfo battleInfo;
    [SerializeField]GameObject Panel;
  [SerializeField]  GameObject ButtonPrefab;
    List<GameObject> Buttons= new List<GameObject>();
   public ItemType item;
public    InventoryUI inventoryUI;
    // Start is called before the first frame update
    void Start()
    {
      //  battleInfo = FindObjectOfType<BattleInfo>();

      
    }
    public void SetupUIs(ItemType _item)
    {
        battleInfo = FindObjectOfType<BattleInfo>();
        item = _item;
        foreach (GameObject item in Buttons)
        {
            Destroy(item);

        }
        Buttons.Clear();
        if (battleInfo)
        {
            Debug.LogError(battleInfo.PlayerStats.Count + "players");



            foreach (PlayerStatsTest item in battleInfo.PlayerStats)
            {
                Debug.Log(item.currentPlayerStats.UnitName);
                GameObject x = Instantiate(ButtonPrefab);
                Buttons.Add(x);
                x.transform.SetParent(Panel.transform);
                ConsumableUsed consumable = x.GetComponent<ConsumableUsed>();
                consumable.SetUp(item);
                consumable.onCharacterChosen.AddListener(UseItem);

            }
        }
    
    }

    public void GetinventoryUI(InventoryUI _inventoryUI)
    {
        inventoryUI = _inventoryUI;
    }
    public void UseItem(PlayerStatsTest stats)
    {
       ConsumableItemType x= item as ConsumableItemType;
        x.UseConsumable(stats);
        inventoryUI.CloseCharacterSelectPanel();
        inventoryUI.setUpStats();
        Player.Instance.RemoveIteminInventory(x);
    //close Panel
    }

    public void CancelSelectCharacter()
    {
        inventoryUI.CloseCharacterSelectPanel();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
