using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum RecoverType{
    Health,
    SkillPoints
};

public enum RecoverAmount {
    Slightly,
    Moderate,
    Large,
    Full
};
public class RecoveryItem : ItemType
{
    public RecoverType RecoType;
    public RecoverAmount RecoAmount;


    public override bool Use()
    {
        //make item recover target's hp 
        return true;
    }
}
