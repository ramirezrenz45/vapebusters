using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// make a stats struc
[System.Serializable]
public class Stats{
    public float MaxHP = 10;
    public float Attack = 1;
    public float Defense;
    public int Speed;
}


// might turn this into a holder instead and have the stats and stuff come from a scriptable object instead for transitionings
public class Unit : MonoBehaviour
{
    public string name;
    public bool IsStunned = false;
    public bool IsKOd = false;
    public float currHP;
    public float currSP;
    public float AtkModifier;
    public float DefModifier;
    public int Attack;
    public int Defense;
    public int currSpeed;
    public int Position; // 0 for front position, 1 for back position
    public UnitMovement Movement;
    public ResourceBarManager ResourceBar;
    public AnimationManager animator;
    public Outline outline;
    public Dictionary<BuffStat,List<float>> CurrentBuffs = new Dictionary<BuffStat, List<float>>();
    public GameObject BuffBar;
    public UnitStats Unit_Stats;
    public Stats stats;
    

    void Start()
    {
        CurrentBuffs.Add(BuffStat.Attack, new List<float>());
        CurrentBuffs.Add(BuffStat.Defense, new List<float>());
    }

    public virtual void Setup(UnitStats unitData)
    {
        Unit_Stats = unitData;
        UpdateStats();
        GameObject prefabModel = Instantiate(Unit_Stats.CharVisuals.PrefabModel) as GameObject;
        prefabModel.transform.SetParent(this.transform);
        prefabModel.transform.localPosition = Vector3.zero;
        prefabModel.transform.Rotate(0, 90, 0);
        animator = prefabModel.GetComponent<AnimationManager>();
        Movement = prefabModel.GetComponent<UnitMovement>();
        outline = prefabModel.GetComponent<Outline>();
        Movement.UnitBase = this;
        animator.animator.SetFloat("Health", ((currHP / Unit_Stats.MaxHP) * 100));
        Debug.Log(name + " " + (currHP / Unit_Stats.MaxHP) * 100);
    }

    public void UpdateStats()
    {
        name = Unit_Stats.UnitName;
        currSpeed = Unit_Stats.Speed.GetValue();
        Attack = Unit_Stats.Attack.GetValue();
        Defense = Unit_Stats.Defense.GetValue();
        PlayerStats p = Unit_Stats as PlayerStats;
        if (p != null)
        {
            currHP =Mathf.Max( 0.0f,p.CurrentHP);
            currSP = p.CurrentSP;
            ResourceBar.SetupBars(Unit_Stats.MaxHP, Unit_Stats.MaxSp);
            ResourceBar.UpdateBars(currHP, currSP);
        }
        else
        {
            currHP = Mathf.Max(0.0f, Unit_Stats.MaxHP) ;
            currSP = Unit_Stats.MaxSp;
            ResourceBar.SetupBars(currHP, currSP);
        }
        Position = Unit_Stats.Position;
    }

    public virtual void TurnFinished()
    {
        outline.enabled = false;
        foreach(Transform t in BuffBar.transform)
        {
            Buff buffs = t.gameObject.GetComponent<Buff>();
            if (buffs != null)
            {
                buffs.TurnPassed();
            }
        }
        RegainSP();
    }

    public virtual void RegainSP()
    {
        currSP += Unit_Stats.MaxSp * 0.10f;
        if (currSP > Unit_Stats.MaxSp)
        {
            currSP = Unit_Stats.MaxSp;
        }
        currSP = Mathf.Floor(currSP);
        ResourceBar.UpdateBars(currHP, currSP);
    }

    public int GetAttack()
    {
        float finalValue = 0;
        finalValue = Attack + 0.0f;
        foreach (float f in CurrentBuffs[BuffStat.Attack])
        {
            finalValue += f * Attack;
        }
        return Mathf.FloorToInt(finalValue);
    }

    public int GetDefense()
    {
        float baseValue = 0;
        float finalValue = 0;
        baseValue = Defense + 0.0f;
        foreach (float f in CurrentBuffs[BuffStat.Defense])
        {
            finalValue += f * baseValue;
        }
        return Mathf.FloorToInt(finalValue);
    }

    public virtual void TakeTurn()
    {
        outline.enabled = true;
        if (!IsStunned)
        {
            animator.animator.SetBool("Stunned", false);
            BattleUIManager.instance.OpenBattleMenu();
        }
        else
        {
            BattleUIManager.instance.EventOccured(this.name + " is stunned and can't move!");
        }
        //Player Shows UI 
        //Enemy Has Coded AI Choosing
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log(GetAttack());
        }
    }

    public virtual void TakeDamage(float damage, AttackType damageType)
    {
        Debug.Log("TakeDamage is called");
        Debug.Log(name + "takes " + damage + " damage.");
        damage -= Defense;
        if(damage <= 0)
        {
            damage = 1;
        }
        damage = Mathf.Ceil(damage);
        currHP -= damage;
        animator.PlayAnimation("Damaged");
        BattleUIManager.instance.EventOccured(this.name + " takes " + damage + " damage");
        ResourceBar.UpdateHealth(Mathf.Max(0.0f,currHP ));
        animator.animator.SetFloat("Health", ((currHP / Unit_Stats.MaxHP) * 100));
        if (currHP <= 0)
        {
            Invoke("Knockout", 0.2f);
        }
    }


    public void ApplyBuff(Buff buffToApply)
    {
        buffToApply.Owner = this;
        buffToApply.transform.SetParent(BuffBar.transform);
        CurrentBuffs[buffToApply.StatToBuff].Add(buffToApply.ModStrength);
        
    }

    public void RemoveBuff(Buff buffToRemove)
    {
        CurrentBuffs[buffToRemove.StatToBuff].Remove(buffToRemove.ModStrength);
        buffToRemove.Dispel();
    }

    public void Knockout()
    {
        IsKOd = true;
        BattleManager.instance.UnitCheck(this.tag,this);
        transform.Find("UnitResourceBars").gameObject.SetActive(false);
        BattleUIManager.instance.EventOccured(name + " Lost their Convinction!");
        animator.PlayAnimation("Lose");
        // Play Death Animation
    }

    public void Heal(float healing)
    {
        Debug.Log(name + "heals " + healing + " damage.");
        currHP += healing;
        if(currHP >= Unit_Stats.MaxHP)
        {
            currHP = Unit_Stats.MaxHP;
        }
        ResourceBar.UpdateBars(currHP, currSP);
        animator.animator.SetFloat("Health", ((currHP / Unit_Stats.MaxHP) * 100));
    }

    public void ConsumeSP(float consumption)
    {
        if(consumption <= currSP)
        {
            currSP -= consumption;
        }
        else
        {
            currSP -= consumption;
            Debug.Log("Error Skill used while SP is too low");
        }
        currSP= Mathf.Ceil(currSP);
        ResourceBar.UpdateBars(currHP, currSP);
    }

    public void ChangePosition(Vector3 newPosition)
    {
        Position++;
        if(Position > 1)
        {
            Position = 0;
        }
        transform.position = newPosition;
    }
}
