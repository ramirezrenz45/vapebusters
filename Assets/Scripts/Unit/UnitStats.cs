using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Visuals {
    public GameObject PrefabModel;
    public Sprite UnitImage;
}



[CreateAssetMenu]
public class UnitStats : ScriptableObject
{
    public string UnitName;
    public Stat Attack;
    public Stat Defense;
    public Stat Speed;
    public float MaxHP;
    public float MaxSp;
    public int Level;
    public int Position;
    public List<Skill> SkillList;
    public Visuals CharVisuals;
    public Sprite unitImage;
    public int Exp; //Enemy Gives this much/ Player has this much


    public void getStat(CharacterData data)
    {
        Attack = data.Attack;
        Defense = data.Defense;
        Speed = data.Speed;
        MaxHP = data.MaxHP;
        MaxSp = data.MaxSp;
        Level = data.Level;
        Position = data.Position;

        Exp = data.Exp;
    }

}
