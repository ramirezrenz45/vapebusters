using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DefenseType
{
    Persistent = 1, // Ask for ideas for this counterpart is Educative
    Apathetic = 2,
    Ignorant = 3,
}

[CreateAssetMenu]
public class EnemyStats : UnitStats
{
    public EnemyAI currentAI;
    public List<ItemType> DropList;
    public List<Visuals> UnitVisualsList;
    public int GoldDrop;
    public DefenseType defenseType;


    public ItemType GetDrop()
    {
        int DropChance = Random.Range(0, 100);
        
        ItemType drop = (DropChance >= 25) ?  DropList[Random.Range(0, DropList.Count)] : null;
        return drop;

    }


 
}
