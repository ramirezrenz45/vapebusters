using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stat
{
    public List<int> EquipmentModifiers;
    [SerializeField]
    private int baseValue;


    public int GetValue()
    {
        int totalValue = baseValue;
        EquipmentModifiers.ForEach(x => totalValue += x);
        return totalValue;
    }

    public void AddValue(int addedValue)
    {
        baseValue += addedValue;
    }

    public void AddModifier(int modToAdd)
    {
        if (modToAdd != 0)
        {
            EquipmentModifiers.Add(modToAdd);
        }
    }

    public void RemoveModifier(int modToAdd)
    {
        if (modToAdd != 0)
        {
            EquipmentModifiers.Remove(modToAdd);
        }
    }

}
