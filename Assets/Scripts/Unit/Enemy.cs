using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class Enemy : Unit
{
    EnemyStats enemyStats;
    public ItemType Drop;
    public int GoldDrop;
    public DefenseType defenseType;
    public GameObject TypeIndicator;
    public TextMeshProUGUI TypeText;
   
    public override void Setup(UnitStats unitData)
    {
        Unit_Stats = unitData;
        enemyStats = Unit_Stats as EnemyStats;
        UpdateStats();
        enemyStats.CharVisuals = enemyStats.UnitVisualsList[Random.Range(0, enemyStats.UnitVisualsList.Count)];
        GameObject prefabModel = Instantiate(enemyStats.CharVisuals.PrefabModel) as GameObject;
        prefabModel.transform.SetParent(this.transform);
        prefabModel.transform.localPosition = Vector3.zero;
        prefabModel.transform.Rotate(0, -90, 0);

        animator = prefabModel.GetComponent<AnimationManager>();
        Movement = prefabModel.GetComponent<UnitMovement>();
        outline = prefabModel.GetComponent<Outline>();
        // enemyStats.unitImage = enemyStats.unitImageList[rand];
        defenseType = enemyStats.defenseType;
        ChangeIndicator(defenseType);
        Drop = enemyStats.GetDrop();
        GoldDrop = enemyStats.GoldDrop;
        Movement.UnitBase = this;
        animator.animator.SetFloat("Health", ((currHP / Unit_Stats.MaxHP) * 100));
        Debug.Log(name + " " + (currHP / Unit_Stats.MaxHP) * 100);
    }

    public void ChangeIndicator(DefenseType typeGiven)
    {
        if (TypeIndicator)
        {
            switch (typeGiven)
            {
                case DefenseType.Persistent:
                    {
                        TypeText.text = "Persistent";
                        break;
                    }
                case DefenseType.Apathetic:
                    {
                        TypeText.text = "Apathetic";
                        break;
                    }
                case DefenseType.Ignorant:
                    {
                        TypeText.text= "Ignorant";
                        break;
                    }
            }
        }
    }

    public void ActivateTypeIndicator(bool activationYes)
    {
        TypeIndicator.SetActive(activationYes);
    }

    public override void TakeTurn()
    {
        ResourceBar.UpdateBars(currHP, currSP);
        outline.enabled = true;
        BattleUIManager.instance.CloseBattleMenu();
        if (!IsStunned)
        {
            animator.animator.SetBool("Stunned", false);
            enemyStats.currentAI.DetermineAction(Position);
            VapeDamage();
            //switch (Position)
            //{
            //    case (0):
            //        {
            //            BattleManager.instance.ReturnSkillToDefault();
            //            BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
            //            break;
            //        }
            //    case (1):
            //        {
            //            if (currHP < 5)
            //            {
            //                BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[2];
            //                BattleManager.instance.CurrentSkill.Activate(stats.Attack, this);
            //                BattleManager.instance.CurrentUnit.animator.StartAttackAnimation(BattleManager.instance.CurrentSkill.AnimationName, () => { CheckSP(); });
            //            }
            //            else
            //            {
            //                BattleManager.instance.ReturnSkillToDefault();
            //                BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[Random.Range(0, 2)]);
            //            }
            //            break;
            //        }
            //}
        }
        else
        {
            BattleUIManager.instance.EventOccured(this.name + " is coughing too much and can't act!");
            BattleUIManager.instance.OpenEvent();
            RegainSP();
        }
    }


    public override void TurnFinished()
    {
        Debug.Log("EnemyTurnFinished");
        CheckSP();
        base.TurnFinished();
    }

    public override void TakeDamage(float damage,AttackType damageType)
    {
        Debug.Log("TakeDamage is called");

        damage = CalculateDamage(damage, damageType);
        base.TakeDamage(damage, damageType);
    }


    float CalculateDamage(float damage, AttackType damageType)
    {
        Debug.Log("AttackType: " + damageType);
        Debug.Log("Defense Type : " + defenseType);
        int type = (int)damageType;
        if (type == 0) { }
        else
        {
            int comp = Mathf.Abs(type - (int)defenseType) % 3;
            Debug.Log(comp);
            switch (comp)
            {
                case 1:
                    Debug.Log("Neutral Type ");
                    damage *= 1.00f;
                    break;
                case 2:
                    Debug.Log("Weak Type ");
                    damage *= 1.20f;
                    break;
                default:
                    Debug.Log("Same Type ");
                    damage *= 0.80f;
                    break;
            }
        }
        Debug.Log("Multiplied Damage: " + damage);
        damage = Mathf.Ceil(damage);
        Debug.Log("Final Damage:" + damage);
        return damage;
    }

    public override void RegainSP()
    {
        currSP = Unit_Stats.MaxSp * 0.50f;
        currSP = Mathf.Ceil(currSP);
    }

    public void VapeDamage()
    {
        float damage = (1.5f * enemyStats.Level);
        damage = Mathf.Round(damage);
        if (currHP <= 1)
        {
            damage = 0;
        }
        currHP -= damage;
        BattleUIManager.instance.EventOccured(this.name + " lost " + damage + " points of conviction from vaping");
        currHP = (Mathf.Max(1.0f, currHP));
        ResourceBar.UpdateHealth(currHP);
        animator.animator.SetFloat("Health", ((currHP / Unit_Stats.MaxHP) * 100));
    }




    public void CheckSP()
    {
        if(currSP < 0)
        {
            Debug.Log("CheckingSP");
            Buff g = Instantiate(BattleManager.instance.StunDebuff);
            animator.animator.SetBool("Stunned", true);
            this.ApplyBuff(g);
            IsStunned = true;
        }

    }
}
