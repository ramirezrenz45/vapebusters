using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LockedSkillReqs {
    public int LevelReq;
    public Skill LockedSkill;
}

[CreateAssetMenu]
public class PlayerStats : UnitStats
{
    public int ExpRequirement;
    public float CurrentHP;
    public float CurrentSP;
    public List<int> AtkModifiers;
    public List<int> DefModifiers;
    public List<int> SpdModifiers;
    public List<LockedSkillReqs> LockedSkills;
    public int MaxLevel;

    //Make LevelUpfunction here


    public void Start()
    {
        CurrentHP = MaxHP;
        CurrentSP = MaxSp;
    }

    public void GoToLevel(int levelToGo)
    {
        for (int x = Level; x < levelToGo; x++) {
            Attack.AddValue(AtkModifiers[x]);
            Defense.AddValue(DefModifiers[x]);
            Speed.AddValue(SpdModifiers[x]);
            MaxHP += (5 + DefModifiers[x]);
            MaxSp += (2 + (AtkModifiers[x] / 2));
        }
        Level = levelToGo;

    }


    void LevelUp()
    {
        Level++;
        if (BattleUIManager.instance != null) BattleUIManager.instance.ExpText.text = BattleUIManager.instance.ExpText.text + "\n" + UnitName + " Leveled up to Level " + Level;
        CheckForUnlockedSkills(Level);
        Attack.AddValue(AtkModifiers[Level]);
        Defense.AddValue(DefModifiers[Level]);
        Speed.AddValue(SpdModifiers[Level]);
        MaxHP += (5 + DefModifiers[Level]);
        MaxSp += (2 + (AtkModifiers[Level] / 2));
        CurrentHP = MaxHP;
        CurrentSP = MaxSp;
    }

    public void GainExp(int ExpGained)
    {
        if(Level < MaxLevel)
        {
            Exp += ExpGained;
        }
        if(Exp >= ExpRequirement)
        {
            Exp -= ExpRequirement;
            LevelUp();
            ExpRequirement = (50 * Level);
        } 
    }

    public void LoadData(CharacterData newData)
    {
        Debug.Log(UnitName + " Data Loaded");
        UnitName = newData.UnitName;
        Attack = newData.Attack;
        Defense = newData.Defense;
        Speed = newData.Speed;
        MaxHP = newData.MaxHP;
        CurrentHP = newData.currHP;
        CurrentSP = newData.MaxSp;
        MaxSp = newData.MaxSp;
        Level = newData.Level;
        Position = newData.Position;
        Exp = newData.Exp;
        CheckForUnlockedSkills(Level);
        ExpRequirement = (50 * Level);
    }


    public void CheckForUnlockedSkills(int level)
    {
        bool checkingSkills = true;
        while (checkingSkills)
        {
            if (LockedSkills.Count > 0)
            {
                if (LockedSkills[0].LevelReq <= level)
                {
                    SkillList.Add(LockedSkills[0].LockedSkill);
                    if (BattleUIManager.instance != null) BattleUIManager.instance.ExpText.text += "\n" + UnitName + " Learned: " + LockedSkills[0].LockedSkill.SkillName;
                    LockedSkills.RemoveAt(0);
                }
                else
                {
                    checkingSkills = false;
                }
            }
            else
            {
                checkingSkills = false;
            }
        }
    }
}
