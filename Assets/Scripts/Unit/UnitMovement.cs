using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitMovement : MonoBehaviour
{
    public Unit UnitBase;

    public Vector3 targetPosition;
    public enum State{
        Idle,
        Moving,
        Busy,
        }
    public State currentState;
    Animator animator;
    private Action onMoveComplete;



    void Start()
    {
        animator = GetComponent<Animator>();
    }

   public Vector3 GetPosition()
    {
        return transform.position;
    }

    void Update()
    {
        switch (currentState) {
            case State.Idle:
                break;
            case State.Busy:
                break;
            case State.Moving:
                transform.position += (targetPosition - GetPosition()) * 3f * Time.deltaTime;
                animator.SetFloat("Speed", 3);
                float reachedDistance = 0.5f;
                if(Vector3.Distance(GetPosition(), targetPosition)< reachedDistance)
                {
                    animator.SetFloat("Speed", 0);
                    transform.position = targetPosition;
                    onMoveComplete();
                }
                break;
        }

    }

    public void Attack(string animationName, Unit Target, Action OnAttackComplete)
    {
        Vector3 startingPosition = GetPosition();
        Vector3 TargetPosition = Target.Movement.GetPosition() + (GetPosition() - Target.Movement.GetPosition()).normalized * 1f;
        Debug.Log("Moving To Target");
        MoveToPosition(TargetPosition, () =>
         {
             currentState = State.Busy;
             UnitBase.animator.StartAttackAnimation(animationName, () =>
             {
                 Debug.Log("Moving Back to Position");
                 OnAttackComplete();
                 MoveToPosition(startingPosition, () =>
                 {
                     BattleUIManager.instance.OpenEvent();
                     currentState = State.Idle;
                 });
             });
         });
    }


    public void MoveTo(Vector3 TargetPosition, Action OnSwitchComplete)
    {
        MoveToPosition(TargetPosition, () =>
         {
             currentState = State.Idle;
             UnitBase.ChangePosition(TargetPosition);
             this.transform.localPosition = Vector3.zero;
             if (this.transform.parent.gameObject.tag =="Player") BattleUIManager.instance.OpenWindow(BattleUIManager.instance.BattleMenu);
         });
    }

    private void MoveToPosition(Vector3 TargetPosition, Action OnMoveComplete)
    {
        targetPosition = TargetPosition;
        onMoveComplete = OnMoveComplete;
        currentState = State.Moving;
    }
}
