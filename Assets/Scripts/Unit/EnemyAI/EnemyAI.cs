using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyAI/GeneralEnemy")]
public class EnemyAI : ScriptableObject
{
    public virtual void DetermineAction(int Position)
    {
        int actionCheck = Random.Range(0, 101);
        switch (Position)
        {
            case (0):
                {
                    if (actionCheck < 10)
                    {
                        BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[0];
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                    else if (actionCheck < 30)
                    {
                        BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[1];
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                    else
                    {
                        BattleManager.instance.ReturnSkillToDefault();
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                }
            case (1):
                {

                    if (actionCheck < 30)
                    {
                        Unit Target = null;
                        foreach (Unit u in BattleManager.instance.EnemyList)
                        {
                            if (50 > ((u.currHP / u.Unit_Stats.MaxHP) * 100))
                            {
                                if (Target == null || u.currHP < Target.currHP)
                                {
                                    Target = u;
                                }
                            }
                        }
                        if (Target == null)
                        {
                            BattleManager.instance.ReturnSkillToDefault();
                            Target = BattleManager.instance.PlayerList[Random.Range(0, BattleManager.instance.PlayerList.Count)];
                            BattleManager.instance.UseSkill(Target);
                            return;
                        }
                        else
                        {
                            BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[1];
                            BattleManager.instance.UseSkill(Target);
                            return;
                        }
                    }
                    else
                    {
                        BattleManager.instance.ReturnSkillToDefault();
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[Random.Range(0, BattleManager.instance.PlayerList.Count)]);
                        return;
                    }

                }
        }

    }
}

