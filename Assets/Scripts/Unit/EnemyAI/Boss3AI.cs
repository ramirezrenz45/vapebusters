using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "EnemyAI/FinalBoss")]
public class Boss3AI : EnemyAI
{
    public override void DetermineAction(int Position)
    {
        int actionCheck = Random.Range(0, 101);
        switch (Position)
        {
            case (0):
                {
                    if (actionCheck < 20)
                    {
                        BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[0];
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                    else if (actionCheck < 40)
                    {
                        Unit target = null;
                        bool buffingUnit = false;
                        foreach (Unit u in BattleManager.instance.EnemyList)
                        {
                            foreach (Transform child in u.BuffBar.transform)
                            {
                                if (child.name == "AttackBuff")
                                {
                                    buffingUnit = false;
                                }
                            }
                            if (buffingUnit)
                            {
                                target = u;
                                break;
                            }
                            //Check if unit has defense buff if has keep buffing unit false if not set to true and set target
                        }
                        if (buffingUnit)
                        {
                            BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[1];
                            BattleManager.instance.UseSkill(target);
                            return;
                        }
                        else
                        {
                            BattleManager.instance.ReturnSkillToDefault();
                            BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        }
                        return;
                    }
                    else if (actionCheck < 70)
                    {
                        BattleManager.instance.CurrentSkill = BattleManager.instance.SkillList[2];
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                    else
                    {
                        BattleManager.instance.ReturnSkillToDefault();
                        BattleManager.instance.UseSkill(BattleManager.instance.PlayerList[0]);
                        return;
                    }
                }
        }
    }
}
