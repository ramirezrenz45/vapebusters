using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialUnit : Enemy
{

    bool FinalMoveIsCasted = false;
    bool IsDoubledOver = false;
    public int HPThreshold;

    public override void TakeTurn()
    {
        if (FinalMoveIsCasted)
        {
            if (IsDoubledOver)
            {
                animator.animator.SetBool("Stunned", true);
                BattleUIManager.instance.EventOccured(this.name + " falls over coughing violently!");
                BattleUIManager.instance.EventOccured(this.name + " can't fight anymore...");
                BattleUIManager.instance.EventWindow.GetComponent<EventButton>().TutorialOngoing = true;
                BattleUIManager.instance.OpenEvent();
                Invoke("EndBattle", 4.0f);
            }
            else
            {
                animator.StartAttackAnimation("Vaping", () =>
                {
                    BattleUIManager.instance.EventOccured(this.name + " is taking more puffs to charge up!");
                    BattleUIManager.instance.OpenEvent();
                });
                IsDoubledOver = true;
            }
        }
        else
        {
            base.TakeTurn();
        }

    }

    public void EndBattle()
    {
        BattleManager.instance.GameOver("Enemy");
    }

    public override void TurnFinished()
    {
        base.TurnFinished();
        if(currHP <= HPThreshold)
        {
            FinalMoveIsCasted = true;
        }
    }


}
