using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class CutSceneManager : MonoBehaviour
{

    public UnityEvent EvtDialogEnd = new UnityEvent();
    public CutSceneDialogSet dialogSet;
    BattleInfo battleInfo;
    int index;
    [SerializeField] Image image;
    [SerializeField] TextMeshProUGUI Speaker;
    [SerializeField] TextMeshProUGUI text;
    [SerializeField] Image BackGround;
    public AudioClip CutsceneMusic;
  
    bool OnPressed;
    bool DialogMode;


    [SerializeField] string NextSceneName;
    // Start is called before the first frame update
    public void NextScene()
    {
        SceneManager.LoadScene(NextSceneName) ;
    }
    private void Start()
    {
        StartDialog(dialogSet);
        EvtDialogEnd.AddListener(NextScene);
        VolumeManager.instance.PlayBGM(CutsceneMusic);
    }
   
    public void StartDialog(CutSceneDialogSet dialog)
    {

        index = 0;
        if (dialog.dialogs.Count > index)
        {
            text.text = dialog.dialogs[index].Dialog;
            AvatarType avatarType = dialog.dialogs[index].avatarType;
            if (avatarType)
            {
                Speaker.text = dialog.dialogs[index].avatarType.Name;
            }
            else
            {
                Speaker.text = "";
            }
            if (avatarType && avatarType.sprite)
            {
                image.enabled = true;
                image.sprite = avatarType.sprite;
            }
            else
            {
                image.enabled = false;
                image.sprite = null;

            }


            if (dialog.dialogs[index].BG)
            {
                BackGround.sprite = dialog.dialogs[index].BG;
            }
            DialogMode = true;
            StartCoroutine(dalayDialog(dialog));
        }

    } 

    public IEnumerator dalayDialog(CutSceneDialogSet dialog)
    {


        while (DialogMode)
        {
            yield return new WaitForSeconds(0.001f);

            if (OnPressed)
            {
                OnPressed = false;
                if (dialog.dialogs.Count > index)
                {  text.text = dialog.dialogs[index].Dialog;
                    AvatarType avatarType= dialog.dialogs[index].avatarType;
                    if (avatarType)
                    {
                        Speaker.text = dialog.dialogs[index].avatarType.Name;
                    }
                    else
                    {
                        Speaker.text = "";
                    }
                    if (avatarType&&avatarType.sprite)
                    {
                        image.enabled = true;
                        image.sprite = avatarType.sprite;
                    }
                    else
                    {
                        image.enabled = false;
                        image.sprite = null;

                    }

                    if (dialog.dialogs[index].BG)
                    {
                        BackGround.sprite = dialog.dialogs[index].BG;
                    }
                    index++;


                }
                else
                {
                  
                    OnPressed = false;
                   
                    Speaker.text = "";
                    text.text = "";
                    EvtDialogEnd.Invoke();
                    GlobalEvents.UnPause?.Invoke();
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DialogMode)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                OnPressed = true;
            }
        }
    }
}
