using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitTurnCard : MonoBehaviour
{
    public Image CardImage;
    public Text CardLvl;
    public Text CardName;

    public void SetupCard(Sprite unitImage, int unitLvl, string unitName)
    {
        if (unitImage != null) { CardImage.sprite = unitImage; };
        CardLvl.text = unitLvl.ToString();
        CardName.text = unitName;
    }
}
