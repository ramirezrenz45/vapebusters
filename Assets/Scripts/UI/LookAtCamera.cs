using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.forward = Camera.main.transform.forward;
        transform.LookAt(Camera.main.transform);
        transform.rotation = Quaternion.Euler(-transform.rotation.eulerAngles.x, 0, 0f);
    }
}
