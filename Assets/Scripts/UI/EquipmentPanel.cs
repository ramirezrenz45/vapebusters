using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class EquipmentPanel : MonoBehaviour
{
    public GameObject EquipmentInventoryPanel;
    public Text CharacterNameText;
    public List<Text> CurrentEquipmentText;
    public List<Image> CurrentEquipmentSprites;
    BattleInfo battleInfo;
    public Image CharImage;
    public TextMeshProUGUI Atk;
    public TextMeshProUGUI Spd;
    public TextMeshProUGUI Def;
    public Image AtkBar;
    public Image SpdBar;
    public Image DefBar;
    public TextMeshProUGUI LVL;
    public TextMeshProUGUI Exp;
    public GameObject DescriptionPanel;
    public TextMeshProUGUI DescriptionText;
    int index;
    public  Button[] buttons; 
    // Update is called once per frame
    void Start() 
    {

        StartCoroutine(delay());
      
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.01f);
        battleInfo = FindObjectOfType<BattleInfo>();
        for (int x = 0; x < buttons.Length; x++)
        {
            buttons[x].transform.Find("Text").GetComponent<TextMeshProUGUI>().text = battleInfo.PlayerStats[x].currentPlayerStats.UnitName;
        }
        EquipmentManager.instance.GetUI(this,EquipmentInventoryPanel, CurrentEquipmentText, CurrentEquipmentSprites, CharacterNameText);

        buttons[0].interactable = false;


        EquipmentManager.instance.ChangeSelection(1);

        index = 0;
        UpdateStat();
    }

    public void UnEquipSlot(int SlotNumber)
    {
        EquipmentManager.instance.Unequip(SlotNumber);
        UpdateStat();
    }

    public void CharacterButtonPress(int CharacterID)
    {
        int x = index == 1 ? 0 : 1;
        buttons[index].interactable = true;

        buttons[x].interactable = false;

        
        EquipmentManager.instance.ChangeSelection(CharacterID+1);

        index = CharacterID;
        UpdateStat();
    }

    public void UpdateStat()
    {

        CharImage.sprite = battleInfo.PlayerStats[index].currentPlayerStats.unitImage;
        Atk.text= battleInfo.PlayerStats[index].currentPlayerStats.Attack.GetValue().ToString();
        Def.text = battleInfo.PlayerStats[index].currentPlayerStats.Defense.GetValue().ToString();
        Spd.text = battleInfo.PlayerStats[index].currentPlayerStats.Speed.GetValue().ToString();

        AtkBar.fillAmount = (battleInfo.PlayerStats[index].currentPlayerStats.Attack.GetValue()/ 50.0f);
        DefBar.fillAmount = (battleInfo.PlayerStats[index].currentPlayerStats.Defense.GetValue() / 50.0f);
        SpdBar.fillAmount = (battleInfo.PlayerStats[index].currentPlayerStats.Speed.GetValue() / 50.0f);
          LVL.text = battleInfo.PlayerStats[index].currentPlayerStats.Level.ToString() ;
         Exp.text = ((battleInfo.PlayerStats[index].currentPlayerStats.ExpRequirement - battleInfo.PlayerStats[index].currentPlayerStats.Exp).ToString());
        if((battleInfo.PlayerStats[index].currentPlayerStats.ExpRequirement - battleInfo.PlayerStats[index].currentPlayerStats.Exp) < 0)
        {
            Exp.text = " 0 ";
        }

    }

    public void TurnOnEquipmentDescription(string description)
    {
        DescriptionPanel.SetActive(true);
        DescriptionText.text = description;
    }
    public void TurnOffEquipmentDescription()
    {
        DescriptionPanel.SetActive(false);
        DescriptionText.text = " ";
    }

}
