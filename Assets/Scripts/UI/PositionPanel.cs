using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PositionPanel : MonoBehaviour
{
    public TextMeshProUGUI Position1Text;
    public TextMeshProUGUI Position2Text;

    public Image Position1Image;
    public Image Position2Image;
    public List<PlayerStats> players = new List<PlayerStats>();
    GameObject PlayerAvatar;

    // Start is called before the first frame update
    void Start()
    {
        PlayerAvatar = GameObject.FindWithTag("Player");
        players.Clear();
        foreach (PlayerStatsTest p in GameObject.FindObjectOfType<BattleInfo>().PlayerStats)
        {
            players.Add(p.currentPlayerStats);
        }
        players.Sort((p1, p2) => p1.Position.CompareTo(p2.Position));
        ChangeUIText();
    }

    public void SwitchPositions()
    {

        players[0].Position = 1;
        players[1].Position = 0;
        players.Sort((p1, p2) => p1.Position.CompareTo(p2.Position));
        if (players[0].UnitName == "Billy")
        {
            PlayerAvatar.GetComponent<PlayerModel>().ChangePlayerModel(true);
        }
        else
        {
            PlayerAvatar.GetComponent<PlayerModel>().ChangePlayerModel(false);
        }
        ChangeUIText();
    }

    public void ChangeUIText()
    {
        Position1Text.text = players[0].UnitName;
        Position2Text.text = players[1].UnitName;
        if (Position1Image && Position2Image)
        {
            Position1Image.sprite = players[0].unitImage;
            Position2Image.sprite = players[1].unitImage;
        }
        GetComponent<StatusManager>().SetupList();
    }
}
