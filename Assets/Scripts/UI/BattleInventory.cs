using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleInventory : MonoBehaviour
{
    List<GameObject> Items = new List<GameObject>();
    public List<GameObject> InventoryButtons;

    public GameObject ConfirmationPanel;
    public GameObject ChooseUnitPanel;
    public GameObject ButtonPrefab;
    ItemType selectedItem;



    #region Singleton
    public static BattleInventory instance;


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    #endregion

    void OnEnable()
    {
        foreach (GameObject g in InventoryButtons)
        {
            g.SetActive(false);
        }
        UpdateInventory(Player.Instance.inventory);
    }

    void UpdateInventory(Inventory inventory)
    {
        Debug.Log("UpdatingInventory");
        int x = 0;
        foreach (StoredItemData item in inventory.StoredItems)
        {
            if (item.name == "Gym Key" || item.name == "Keycard") { }
            else
            {
                GameObject pic = InventoryButtons[x];
                pic.GetComponent<ItemButton>().Item = ItemDatabase.Instance.GetFromID(item.ItemTypeID);
                pic.GetComponent<ItemButton>().ItemDescription = ItemDatabase.Instance.GetFromID(item.ItemTypeID).Description;
                pic.GetComponent<TextMeshProUGUI>().text = ItemDatabase.Instance.GetFromID(item.ItemTypeID).name;
                if (item.Amount > 1)
                {
                    pic.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = item.Amount.ToString();
                }
                else
                {
                    pic.transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "";
                }
                pic.SetActive(true);
                x++;
            }
        }
    }

    public void UnitButtonSetup()
    {
        foreach (Unit u in BattleManager.instance.PlayerList)
        {
            GameObject x = Instantiate(ButtonPrefab);
            x.transform.SetParent(ChooseUnitPanel.transform.Find("Prompt Background/Panel").transform);
            BattlePlayerButton consumable = x.GetComponent<BattlePlayerButton>();
            consumable.SetUp(u);

        }
    }

    public void ItemButtonPressed(ItemType item)
    {
        BattleUIManager.instance.PlaySelect();
        selectedItem = item;
        ConfirmationPanel.SetActive(true);
        BattleUIManager.instance.Controls.SetActiveWindow(ConfirmationPanel);
    }

    public void UseButtonPressed()
    {
        BattleUIManager.instance.PlaySelect();
        ChooseUnitPanel.SetActive(true);
        UnitButtonSetup();
        BattleUIManager.instance.Controls.SetActiveWindow(ChooseUnitPanel);
    }

    public void CloseCharacterSelectPanel()
    {
        BattleUIManager.instance.PlayConfirm();
        ChooseUnitPanel.SetActive(false);
        BattleUIManager.instance.Controls.SetActiveWindow(ConfirmationPanel);
    }

    public void OnCancelButtonPressed()
    {
        BattleUIManager.instance.PlaySelect();
        ConfirmationPanel.SetActive(false);
        selectedItem = null;
        BattleUIManager.instance.Controls.SetActiveWindow(BattleUIManager.instance.ItemWindow);

    }

    public void UseItem(Unit unit)
    {
        BattleUIManager.instance.PlayConfirm();
        ConsumableItemType x = selectedItem as ConsumableItemType;
        x.UseConsumable(unit);
        unit.ResourceBar.UpdateBars(unit.currHP, unit.currSP);
        ClosePanels();
        BattleUIManager.instance.EventOccured("Used a " + x.Name + " on " + unit.name);
        BattleUIManager.instance.Controls.SetActiveWindow(BattleUIManager.instance.BattleMenu);
        BattleUIManager.instance.OpenEvent();
        Player.Instance.RemoveIteminInventory(x);
        //close Panel
    }

    public void ClosePanels()
    {

        ChooseUnitPanel.SetActive(false);
        ConfirmationPanel.SetActive(false);
        selectedItem = null;
        this.gameObject.SetActive(false);
        BattleUIManager.instance.CloseDescription();
        foreach (Transform Button in ChooseUnitPanel.transform.Find("Prompt Background/Panel").transform)
        {
           Destroy(Button.gameObject);
        }
        BattleUIManager.instance.Controls.SetActiveWindow(BattleUIManager.instance.BattleMenu);
    }


}
