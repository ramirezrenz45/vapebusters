using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator : MonoBehaviour
{
    public GameObject image;

  public void MoveToPosition(Vector3 targetPosition)
    {

        transform.position = new Vector3(targetPosition.x, 2.5f, targetPosition.z);
        Invoke("On", 0.1f);
    }

    void On()
    { 
        image.SetActive(true);
    }

    public void Off()
    {
        image.SetActive(false);
    }

    public void Close()
    {
        Invoke("Off", 0.2f);
    }
}
