using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TutorialCanvas : MonoBehaviour
{
    public List<Sprite> tutorialImages;
    public Image CurrentImage;
    public GameObject ExitButton;
    public GameObject PreviousButton;
    public GameObject NextButton;
    public GameObject ConfirmationPanel;
    public bool IsBattle;
    int index;
    //exit evt
    public UnityEvent ExitEvt= new UnityEvent();

    void Awake()
    {
        if (IsBattle)
        {

            BattleUIManager.instance.EventWindow.GetComponent<EventButton>().TutorialOngoing = true;
            BattleUIManager.instance.EscapeButton.SetActive(false);
        }
        else
        {
            GlobalEvents.Pause();
        }
        if (tutorialImages.Count > 0)
        {
            CurrentImage.sprite = tutorialImages[0];
        }
        else
        {
            StartCoroutine(DelaySetup());
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ChangeImage(1);
        }
        else if(Input.GetKeyDown(KeyCode.Backspace))
        {
            ChangeImage(-1);
        }
    }

    public void ChangeImage(int indexMovement)
    {
        index += indexMovement;
        index = (index < 0) ? 0 : index;
        index = (index > tutorialImages.Count - 1) ? tutorialImages.Count - 1 : index;
        CheckButtons();
       
            CurrentImage.sprite = tutorialImages[index];
      
    }

    //delay
    IEnumerator DelaySetup()
    {
        yield return new WaitForSeconds(0.01f);

        CurrentImage.sprite = tutorialImages[0];

        Debug.LogError("Image empty");
    }

    public void CheckButtons()
    {
        if (index == 0)
        {
            PreviousButton.SetActive(false);
            NextButton.SetActive(true);
        }
        else if (index == tutorialImages.Count-1)
        {
            ExitButton.SetActive(true);
            PreviousButton.SetActive(true);
            NextButton.SetActive(false);
        }
        else
        {
            ExitButton.SetActive(false);
            PreviousButton.SetActive(true);
            NextButton.SetActive(true);
        }
    }

    public void OpenConfirmation(bool open)
    {
        ConfirmationPanel.SetActive(open);
    }

    public void ExitTutorial()
    {
        if (IsBattle) 
        {
            BattleUIManager.instance.EventWindow.GetComponent<EventButton>().TutorialOngoing = false;
        }
        else
        {
            GlobalEvents.UnPause.Invoke();
           
           // ExitEvt.RemoveAllListeners();
        }
        ExitEvt.Invoke();
        Destroy(this.gameObject);
    }




}
