using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TransitionPanel : MonoBehaviour
{
    DialogSystem dialogSystem;


 public DialogSet dialog;
 public   UnityEvent TransitionToBattleSceneEnd= new UnityEvent();
    public UnityEvent<DialogSet> TransitionToWorldSceneEnd = new UnityEvent<DialogSet>();
    // Start is called before the first frame update
    void Start()
    {
        //transform.localScale = Vector3.zero;
        StartCoroutine(transitionToWorldSceneAnimation());
        dialogSystem = FindObjectOfType<DialogSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void transitioningToBattleScene()
    {
        transform.localScale = Vector3.one*0.1f;
        dialogSystem.EvtDialogEnd.RemoveListener(transitioningToBattleScene);
        StartCoroutine(transitionToBattleSceneAnimation());
    }


    IEnumerator transitionToBattleSceneAnimation()
    {
        int x = 0;
        while (x<10)
        {
           yield return new WaitForSeconds(0.01f);
            transform.localScale = new Vector3(transform.localScale.x+0.1f, transform.localScale.y+0.1f, transform.localScale.z + 0.1f);
            x++;

        
            if (x >= 10)
            {
                TransitionToBattleSceneEnd?.Invoke();
                
                break;
               
            
            }

        }


    }
    public void transitioningToWorldScene()
    {
        transform.localScale = Vector3.one;
        dialogSystem.EvtDialogEnd.RemoveListener(transitioningToBattleScene);
        StartCoroutine(transitionToWorldSceneAnimation());
    }


    IEnumerator transitionToWorldSceneAnimation()
    {
        int x = 1;
        while (x < 10)
        {
            yield return new WaitForSeconds(0.01f);
            transform.localScale = new Vector3(transform.localScale.x - 0.1f, transform.localScale.y - 0.1f, transform.localScale.z - 0.1f);
            x++;


            if (x >= 10)
            {
                transform.localScale = Vector3.zero;
                
                TransitionToWorldSceneEnd?.Invoke(dialog);
                break;


            }

        }

        
    }

}
