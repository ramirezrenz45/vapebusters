using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BattleDialog : MonoBehaviour
{

    public Animator animator;

    public GameObject TextBox;
    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Awake()
    {
        HideText();
    }

    public void ChangeText(string textToShow)
    {
        text.text = textToShow;
    }

    public void OpenDialog(string textToShow, Vector3 position)
    {
        text.text = textToShow;
        this.transform.parent.position = new Vector3(position.x, position.y + 3f, position.z -0.1f);
        TextBox.gameObject.SetActive(true);
        Open();
    }

    public void ShowText()
    {
        text.gameObject.SetActive(true);
    }

    public void HideText()
    {
        text.gameObject.SetActive(false);
    }

    void Open()
    {
        TextBox.gameObject.SetActive(true);
        animator.Play("Open");
        Invoke("CloseAnimation", 4.0f);
    }

    public void CloseAnimation()
    {
        Debug.Log("ClosingDialog");
        animator.Play("Close");
    }

    public void Close()
    {
        TextBox.gameObject.SetActive(false);
    }
}
