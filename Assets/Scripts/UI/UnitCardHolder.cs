using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCardHolder : MonoBehaviour
{
    public List<UnitTurnCard> UnitCards;
    string SimilarName;
    public Vector3 scaleChange = new Vector3(0.2f, 0.2f, 0.2f);
    Vector3 baseScale = new Vector3(1, 1, 1);

    public void TurnOrderUpdate(List<Unit> unitList)
    {
        foreach (UnitTurnCard g in UnitCards)
        {
            g.gameObject.SetActive(false);
        }
        int i = 0;
        foreach(Unit u in unitList)
        {
            UnitStats us = u.Unit_Stats;
            
            if (i < UnitCards.Count)
            {
                UnitCards[i].SetupCard(us.CharVisuals.UnitImage, us.Level, u.name);
                UnitCards[i].transform.localScale = baseScale;
                UnitCards[i].gameObject.SetActive(true);
            }
            else
            {
                Debug.Log("Not Enough UnitCards");
            }
            i++;
        }

        UnitCards[0].transform.localScale += scaleChange;
    }

    void Update()
    {
        int x = 0;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            char c = (char)(65 + x);
            Debug.Log(c.ToString());
        }
    }
}
