using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatusManager : MonoBehaviour
{
    public List<PlayerStats> Players;
    public TextMeshProUGUI NameText;
    public TextMeshProUGUI LevelText;
    public TextMeshProUGUI ExpRequiredText;
    public TextMeshProUGUI AttackText;
    public TextMeshProUGUI DefenseText;
    public TextMeshProUGUI SpeedText;
    public TextMeshProUGUI HealthPointsText;
    public TextMeshProUGUI LungPointsText;
    public TextMeshProUGUI PositionText;

    [Header("Equipment")]
    public TextMeshProUGUI Head;
    public TextMeshProUGUI Body;
    public TextMeshProUGUI Legs;
    public TextMeshProUGUI Weapom;




    [Header("Playerstats")]
    public Image CharImage;
    public TextMeshProUGUI HP;
    public TextMeshProUGUI LP;
    public TextMeshProUGUI Atk;
    public TextMeshProUGUI Spd;
    public TextMeshProUGUI Def;

    public Image HPBar;
    public Image LPBar;
    public Image AtkBar;
    public Image SpdBar;
    public Image DefBar;
    public TextMeshProUGUI LVL;
    public TextMeshProUGUI Exp;

    [Header("Playerstats2")]
    public Image _CharImage;
    public TextMeshProUGUI _HP;
    public TextMeshProUGUI _LP;
    public TextMeshProUGUI _Atk;
    public TextMeshProUGUI _Spd;
    public TextMeshProUGUI _Def;

    public Image _HPBar;
    public Image _LPBar;
    public Image _AtkBar;
    public Image _SpdBar;
    public Image _DefBar;
    public TextMeshProUGUI _LVL;
    public TextMeshProUGUI _Exp;






    public List<Button> button;


    // Start is called before the first frame update
    void Awake()
    {
        //  Players = new List<PlayerStats>();
        // Players.Clear();
        StartCoroutine(delay());
    }


    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.1f);

    }


    public void SetupList()
    {
        Players.Clear();
        BattleInfo info = FindObjectOfType<BattleInfo>();
        foreach (PlayerStatsTest p in info.PlayerStats)
        {

            Players.Add(p.currentPlayerStats);
        }


        SetupUIText(Players[0].Position == 1 ? 1 : 0);

    }



    // Update is called once per frame
    public void SetupUIText(int PlayerNumber)
    {
        // Equipment[] equipments;
        int index = PlayerNumber == 1 ? 0 : 1;
        // button[index].interactable = true;

        //            button[PlayerNumber].interactable = false;

        if (Players[PlayerNumber])
        {
            NameText.text = "Name: " + Players[PlayerNumber].UnitName;
            LevelText.text = "Level: " + Players[PlayerNumber].Level.ToString();
            ExpRequiredText.text = (Players[PlayerNumber].Level < 20) ? ((Players[PlayerNumber].ExpRequirement - Players[PlayerNumber].Exp).ToString()) : " Max Level ";
            AttackText.text = "Attack: " + Players[PlayerNumber].Attack.GetValue().ToString();
            DefenseText.text = "Defense: " + Players[PlayerNumber].Defense.GetValue().ToString();
            SpeedText.text = "Speed: " + Players[PlayerNumber].Speed.GetValue().ToString();
            HealthPointsText.text = "HP: " + Players[PlayerNumber].CurrentHP.ToString() + " / " + Players[PlayerNumber].MaxHP.ToString();
            LungPointsText.text = "LP: " + Players[PlayerNumber].CurrentSP.ToString() + " / " + Players[PlayerNumber].MaxSp.ToString();
            PositionText.text = "Position: " + Players[PlayerNumber].Position.ToString();




            CharImage.sprite = Players[PlayerNumber].unitImage;
            Atk.text = Players[PlayerNumber].Attack.GetValue().ToString();
            Def.text = Players[PlayerNumber].Defense.GetValue().ToString();
            Spd.text = Players[PlayerNumber].Speed.GetValue().ToString();
            HP.text = Players[PlayerNumber].MaxHP.ToString();
            LP.text = Players[PlayerNumber].MaxSp.ToString();

            HPBar.fillAmount = Players[PlayerNumber].MaxHP / 140.0f;
            LPBar.fillAmount = Players[PlayerNumber].MaxSp / 140;
            AtkBar.fillAmount = (Players[PlayerNumber].Attack.GetValue() / 50.0f);
            DefBar.fillAmount = (Players[PlayerNumber].Defense.GetValue() / 50.0f);
            SpdBar.fillAmount = (Players[PlayerNumber].Speed.GetValue() / 50.0f);
            LVL.text = Players[PlayerNumber].Level.ToString();
            Exp.text = (Players[PlayerNumber].Level < 20) ? ((Players[PlayerNumber].ExpRequirement - Players[PlayerNumber].Exp).ToString()) : " Max Level ";
            if ((Players[PlayerNumber].ExpRequirement - Players[PlayerNumber].Exp) < 0)
            {
                Exp.text = " 0 ";
            }
            CharImage.sprite = Players[PlayerNumber].unitImage;
        }
        SetupPanelB(index);
    }

    public void SetupPanelB(int PlayerNumber)
    {
        if (Players[PlayerNumber])
        {



            _CharImage.sprite = Players[PlayerNumber].unitImage;
            _Atk.text = Players[PlayerNumber].Attack.GetValue().ToString();
            _Def.text = Players[PlayerNumber].Defense.GetValue().ToString();
            _Spd.text = Players[PlayerNumber].Speed.GetValue().ToString();
            _HP.text = Players[PlayerNumber].MaxHP.ToString();
            _LP.text = Players[PlayerNumber].MaxSp.ToString();

            _HPBar.fillAmount = Players[PlayerNumber].MaxHP / 140.0f;
            _LPBar.fillAmount = Players[PlayerNumber].MaxSp / 140;
            _AtkBar.fillAmount = (Players[PlayerNumber].Attack.GetValue() / 50.0f);
            _DefBar.fillAmount = (Players[PlayerNumber].Defense.GetValue() / 50.0f);
            _SpdBar.fillAmount = (Players[PlayerNumber].Speed.GetValue() / 50.0f);
            _LVL.text = Players[PlayerNumber].Level.ToString();
            _Exp.text = (Players[PlayerNumber].Level < 20) ? ((Players[PlayerNumber].ExpRequirement - Players[PlayerNumber].Exp).ToString()) : " Max Level ";
            if ((Players[PlayerNumber].ExpRequirement - Players[PlayerNumber].Exp) < 0)
            {
                _Exp.text = " 0 ";
            }
            _CharImage.sprite = Players[PlayerNumber].unitImage;
        }
    }

}
