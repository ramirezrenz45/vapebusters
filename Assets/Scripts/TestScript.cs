using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    public PlayerStatsTest playerStatsTest;
    public StatusManager statusManager;
    public int CheckingLevel;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            playerStatsTest.MakeNewStat();
            playerStatsTest.currentPlayerStats.GoToLevel(CheckingLevel);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            statusManager.Players.Clear();
            statusManager.Players.Add(playerStatsTest.currentPlayerStats);
            statusManager.SetupUIText(0);
        }
    }
}
