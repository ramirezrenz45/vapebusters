using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stun : Buff
{


    public override void Dispel()
    {
        Owner.IsStunned = false;
        Destroy(this.gameObject);
    }
}
