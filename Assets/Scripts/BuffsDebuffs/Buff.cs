using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum BuffStat { Attack, Defense, Speed }
public class Buff : MonoBehaviour
{
    // Use Percentages for this (maybe have the getter turn it into percentage)
    public float ModStrength;
    public string BuffName;
    public Unit Owner;
    public BuffStat StatToBuff;
    public int duration;
    public AudioClip BuffSFX;

    private int TurnsPassed;

    public void Awake()
    {
        transform.name = BuffName;
        VolumeManager.instance.PlaySFX(BuffSFX);
    }
    public void TurnPassed()
    {
        TurnsPassed++;
        if (TurnsPassed > duration)
        {
            Owner.RemoveBuff(this);
        }
    }

    public virtual void Dispel()
    {
        Destroy(this.gameObject);
    }
}
