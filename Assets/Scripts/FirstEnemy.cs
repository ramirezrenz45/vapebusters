using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FirstEnemy : MonoBehaviour
{
    public GameObject TutorialManager;
    public bool IsDead;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DelayDespawn());

    }

    IEnumerator DelayDespawn()
    {
   yield  return new  WaitForSeconds(0.1f);
        if (SaveLoad.SaveExists("SavedData"))
        {
            Debug.Log("no more");
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (ObjectiveTracker.instance.GetObjectiveIndex() < 1) ObjectiveTracker.instance.SetObjectiveIndex(1);
            Instantiate(TutorialManager);
        }
    }


    void OnDisable()
    {
        Destroy(this.gameObject);
    }
}
