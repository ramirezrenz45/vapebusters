using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipSlot { Head, Body, Legs, Weapon}
[CreateAssetMenu(menuName = "Item/Equipment")]
public class Equipment : ItemType
{
   public int HPMod;
   public int AttackMod;
   public int DefenseMod;
   public int SpeedMod;
    public EquipSlot Slot;
    


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Equiped()
    { 
    //
    }

}
