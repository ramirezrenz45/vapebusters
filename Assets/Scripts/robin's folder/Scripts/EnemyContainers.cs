using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyContainers : MonoBehaviour
{
    //float fadePercentage;
    public UnityEvent EvtRespawned= new UnityEvent();
    public UnityEvent<GameObject> EvtDied = new UnityEvent<GameObject>();
    public EnemyData enemyData;
  public string ID;
    public Spawner spawner;
    public bool IsBoss = false;
    [SerializeField] DialogSet PostBattleScript;
    DialogSystem dialogSystem;
    public bool isDead;
    public int BossNumber;
    // Start is called before the first frame update
    private void Awake()
    {
        isDead = false;
       
    }
    void Start()
    {
        dialogSystem = FindObjectOfType<DialogSystem>();
        ID = GetComponent<UniqueID>().ID;
        spawner = FindObjectOfType<Spawner>();
       enemyData= spawner.GetEnemyData(ID, this.gameObject,IsBoss);

        StartCoroutine(DelaySpawnercheck());
        

      //  EvtDied.AddListener(die);   
    }

    IEnumerator DelaySpawnercheck()
    {
        yield return new WaitForSeconds(0f);

        if (IsBoss)
        {
            Debug.LogError(ID);     
          //  CollectibleSet collectibleSet = FindObjectOfType<CollectibleSet>();
            if (CollectibleSet.Instance.CollectedItems.Contains(ID))
            {
               // foreach (string obj in collectibleSet.CollectedItems)
               //// {
                  //  Debug.LogWarning(obj);
               // }
                
                isDead = true;
                BattleInfo battleInfo = FindObjectOfType<BattleInfo>();
                if (battleInfo.CurrEnemyID == ID)
                {
                    battleInfo.TransitionToWorld(PostBattleScript);
                    transform.position = battleInfo.PlayerPos - new Vector3(1, 0, 1);
                    GetComponent<SphereCollider>().enabled = false;
                    dialogSystem.EvtDialogEnd.AddListener(Respawn);
                }
                else
                {


                    Respawn();
                }

            }
            else
            {
                Debug.LogError("boss is not dead");
                isDead = false;
                GetComponent<ChaserController>().CharacterSetUp();
            }
        }
        else
        {

            if (enemyData.CooldownTime > 0)
            {


                isDead = true;
                BattleInfo battleInfo = FindObjectOfType<BattleInfo>();
                if (battleInfo.CurrEnemyID == ID)
                {
                    battleInfo.TransitionToWorld(PostBattleScript);
                    transform.position = battleInfo.PlayerPos;
                    GetComponent<SphereCollider>().enabled = false;
                    dialogSystem.EvtDialogEnd.AddListener(Respawn);

                }
                else
                {


                    Respawn();
                }

            }
            else
            {

                isDead = false;
                GetComponent<ChaserController>().CharacterSetUp();
            }
        }
    }

    public void Respawn()
    {
        dialogSystem.EvtDialogEnd.RemoveListener(Respawn);
        Debug.Log("Previous =" + IsBoss);
        if (!IsBoss)
        {
            GetComponent<SphereCollider>().enabled = true;
            spawner.Respawning(this.gameObject);
            ChaserController controller = GetComponent<ChaserController>();
            if (controller.hasWaypoints())
            {
                controller.canMove = true;
            }

        }
        else
        {
            Debug.Log("Current "+ this.gameObject.name+ " " + IsBoss );
            //if (GameObject.Find("ObjectivePanel") != null) ObjectiveTracker.instance.ObjectiveComplete();
            dialogSystem.EvtDialogEnd.RemoveListener(Respawn);
            Player.Instance.SetBossDefeated(BossNumber);
            foreach (GameObject s in GameObject.FindGameObjectsWithTag("StairBlocker"))
            {
                if (s.GetComponent<StairTriggers>())
                {
                    s.GetComponent<StairTriggers>().CheckBoss(this.gameObject);
                }
            }
            spawner.Disable(this.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        spawner.RemoveFromList(this.gameObject);
        Debug.LogError("Removed" + gameObject.name);
        Destroy(this.gameObject,0.001f);
    }
}
