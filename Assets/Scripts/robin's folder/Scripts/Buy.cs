using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Buy : MonoBehaviour
{
   public ItemType item;
   public GameObject confirmationPanel; 
    public GameObject NotificationPanel;

    // Start is called before the first frame update
   public UnityEvent<ItemType> onClick = new UnityEvent<ItemType>();




  public  void onBuy()
    {
        if (Player.Instance.playerData.Gold >= item.Cost)
        {

            confirmationPanel.SetActive(true);
            ConfirmationPanel confirmation = confirmationPanel.GetComponent<ConfirmationPanel>();
            confirmation.EvtConfirm.AddListener(OnConfirm);
            confirmation.text.text = " Are you sure you want to buy " + item.Name + " ?";
        }
        else
        {
            confirmationPanel.GetComponent<ConfirmationPanel>().EvtConfirm.RemoveListener(OnConfirm);
            NotificationPanel.SetActive(true);
        }


    }

    public void OnConfirm()
    {
        
        confirmationPanel.GetComponent<ConfirmationPanel>().EvtConfirm.RemoveListener(OnConfirm);
        confirmationPanel.SetActive(false);
        onClick.Invoke(item);
    }
    
}
