using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
  [SerializeField]  GameObject InventoryUI;
    public GameObject EquipmentPanel;
    public GameObject QuitConfirmation;
    [SerializeField] GameObject MenuPanel;
    [SerializeField] GameObject MenuButton;
    [SerializeField] GameObject shop;
    [SerializeField] BattleInfo battleInfo;
    [SerializeField] GameObject Status;
    public GameObject confirmationPanel;
    [SerializeField] GameObject OptionPanel;
     ShopUI shopUI;
    [SerializeField] GameObject TutorialPanel;

    [SerializeField] GameObject NewInformationPanel;
    [SerializeField] GameObject NewItemNotification;
    [SerializeField] GameObject NewEquipmentNotification;

    [Header("Audio")]
    public AudioClip PauseSFX;
    public AudioClip UnpauseSFX;
    public AudioClip ReturnSFX;
    public AudioClip ClickSFX;

    public bool disableShortcuts = false;


    // Start is called before the first frame update
    [System.Serializable]
  public  enum Panel
    { 
    inventory,
    equipment,
    menuPanel,
    MenuButton,
    shop,
    Status,
    confirmation,
    options,
    tutorial
    }


    #region Singleton

    public static UIManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    #endregion

    public void disableShortcut()
    {
        disableShortcuts = true;
    }
    public void enableShortcuts()
    {
        disableShortcuts = false;
    }

    public void Notification(Inventory inventory)
    {
        NewEquipmentNotification.SetActive(Player.Instance.playerData.newEquipment);
         NewItemNotification.SetActive  ( Player.Instance.playerData.newItem);
        
    }

    public void FactBoardNotification()
    {
        NewInformationPanel.SetActive(ObjectiveTracker.instance.getNotification());
    }

    public void NewInformation()
    {
        NewEquipmentNotification.SetActive(true) ;
    }
    

    private void Start()
    {
        shopUI = shop.GetComponent<ShopUI>();
        Player.Instance.EvtInventoryChange.AddListener(Notification);
        ObjectiveTracker.instance.CheckHasNotificationEvent.AddListener(FactBoardNotification);
        // CloseMenu();
        Notification(Player.Instance.inventory);
        FactBoardNotification();
    }
    public void CloseConfirmationPanel()
    {
        VolumeManager.instance.PlaySFX(ReturnSFX);
        confirmationPanel.SetActive(false);
        CloseAllPanel();

    }
    public void OpenOptions()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        OpeningPanel(Panel.options);
        
    }

    public void CloseAllPanel()
    {
        OpeningPanel(Panel.MenuButton);

    }
    public void CanOpenMenu(bool canOpen)
    {
        CloseAllPanel();
        MenuButton.GetComponent<Button>().interactable= canOpen;

    }
    public void OpeningTutorialPanel()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        TutorialPanel.GetComponent<MenuTutorialList>().OnOpen();
        OpeningPanel(Panel.tutorial);
    }

    public void OpeningPanel(Panel panel)
    {
        GlobalEvents.Pause += disableShortcut;
        GlobalEvents.UnPause += enableShortcuts;
        GlobalEvents.Pause();
        CharacterStatusPanel.instance.UpdateCharacterStatus();
        switch (panel)
        {
            case Panel.equipment:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(true);
                //MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                OptionPanel.SetActive(false);
                confirmationPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                Player.Instance.playerData.newEquipment = false;
                if (NewEquipmentNotification.activeSelf)
                {
                    NewEquipmentNotification.SetActive(false);
                }
                break;
            case Panel.inventory:
                InventoryUI.SetActive(true);
                EquipmentPanel.SetActive(false);
                //MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                OptionPanel.SetActive(false);
                confirmationPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                Player.Instance.playerData.newItem = false;
                if (NewItemNotification.activeSelf)
                {
                    NewItemNotification.SetActive(false);
                }
                break;
            case Panel.MenuButton:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(true);
                Status.SetActive(false);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                GlobalEvents.UnPause.Invoke();
                break;
            case Panel.menuPanel:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                MenuPanel.SetActive(true);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                break;
            case Panel.shop:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                MenuPanel.SetActive(false);
                shop.SetActive(true);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                break;
            case Panel.Status:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                //MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(true);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                break;
            case Panel.confirmation:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                //MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                confirmationPanel.SetActive(true);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(false);
                break;
            case Panel.options:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                //MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(true);
                TutorialPanel.SetActive(false);
                break;

            case Panel.tutorial:
                InventoryUI.SetActive(false);
                EquipmentPanel.SetActive(false);
                MenuPanel.SetActive(false);
                shop.SetActive(false);
                MenuButton.SetActive(false);
                Status.SetActive(false);
                confirmationPanel.SetActive(false);
                OptionPanel.SetActive(false);
                TutorialPanel.SetActive(true);
                break;
            default:
                break;

        }


    }
    public void OpenStatus()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        OpeningPanel(Panel.Status);
    }

    public void BackToMenu()
    {
        VolumeManager.instance.PlaySFX(ReturnSFX);
        OpeningPanel(Panel.menuPanel);
    }

    private void Update()
    {if (!disableShortcuts)

        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                OpenMenu();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                OpenEquipment();
            }
            if (Input.GetKeyDown(KeyCode.I))
            {
                OpenCloseInventory();
            }
            MenuButton.SetActive(true);
        }
        else {
            MenuButton.SetActive(false);
        }
    }
    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.1f);
        Player.Instance.EvtInventoryChange.Invoke(Player.Instance.inventory);
    }

   public void OpenMenu()
    {
        if (!MenuPanel.activeInHierarchy)
        {
            VolumeManager.instance.PlaySFX(PauseSFX);
            OpeningPanel(Panel.menuPanel);
        }
        else
        {
            CloseMenu();
        }
    }
    public void CloseMenu()
    {
        VolumeManager.instance.PlaySFX(UnpauseSFX);
        OpeningPanel(Panel.MenuButton);
    }

    public void OpenQuitConfirmation()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        QuitConfirmation.SetActive(true);
    }

    public void BackToMainMenu()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        confirmationPanel.SetActive(true);
        ConfirmationPanel confirm = confirmationPanel.GetComponent<ConfirmationPanel>();
        confirm.EvtConfirm.AddListener(  FindObjectOfType<BattleInfo>().GoToMainMenu);
        confirm.text.text = "Are you sure you want to quit?";
        confirm.EvtConfirm.AddListener(CloseMenu);
        confirm.EvtCancel.AddListener(BackToMenu);


    }

    public void OpenInventory()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
        OpeningPanel(Panel.menuPanel);
        StartCoroutine(delay());
    }

    public void OpenCloseInventory()
    {
        Player.Instance.LoadInventory();
        //InventoryUI.SetActive(!InventoryUI.activeSelf);
        if (InventoryUI.activeSelf)
        {
            VolumeManager.instance.PlaySFX(ClickSFX);
            OpeningPanel(Panel.menuPanel);
        }
        else
        {
            VolumeManager.instance.PlaySFX(PauseSFX);
            OpeningPanel(Panel.inventory);
        }
        StartCoroutine(delay());
    }

   


    public void OpenEquipment()
    {
        VolumeManager.instance.PlaySFX(PauseSFX);
        StartCoroutine(delay());
        //   EquipmentPanel.gameObject.SetActive(!EquipmentPanel.activeSelf);
        //if (EquipmentPanel.activeSelf)
        //{
        //    OpeningPanel(Panel.menuPanel);
        //}
        //else
        //{
        //    OpeningPanel(Panel.equipment);
        //}
        OpeningPanel(Panel.equipment);
        StartCoroutine(delay());
        //PlayerStatsTest[] playerStats = FindObjectsOfType<PlayerStatsTest>();
        //foreach (PlayerStatsTest p in playerStats){
        //    if (EquipmentPanel.activeSelf)
        //    {
        //        p.AttachToManager();
        //    }
        //    else
        //    {
        //        p.DettachFromManager();

        //    }
        //}

    }

    public void PlayClickSFX()
    {
        VolumeManager.instance.PlaySFX(ClickSFX);
    }


    public void OpenShop(List<ItemType> ShopMenu)
    {

        OpeningPanel(Panel.shop);
        shopUI.OpenedShop(ShopMenu);
        shopUI.SetShopClosingDialog(null);

    }
    public void NurseShop(List<ItemType> ShopMenu, DialogSet dialogSet)
    {

        OpeningPanel(Panel.shop);
        shopUI.OpenedShop(ShopMenu);
        shopUI.SetShopClosingDialog(dialogSet);
    }

    public void QuitButton()
    {
        FindObjectOfType<SceneControl>().ReturnToMaoinMenu();
    }



    public void ClosePanel()
    {
        if (MenuPanel.activeInHierarchy)
        {
            OpeningPanel(Panel.menuPanel);
        }
        else
        {
            CloseAllPanel();
        }
    }
}
