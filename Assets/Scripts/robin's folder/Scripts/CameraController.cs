using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour

{

    public GameObject playerInfo;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void  Update()
    {

        if (playerInfo)
        {
            transform.position = new Vector3(playerInfo.transform.position.x, playerInfo.transform.position.y + 10, playerInfo.transform.position.z - 5);

            transform.LookAt(playerInfo.transform.position);
        }
        else 
        {
            playerInfo = GameObject.FindGameObjectWithTag("Player");
        }
    }

}
