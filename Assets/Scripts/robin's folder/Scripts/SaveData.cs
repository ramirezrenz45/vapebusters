using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour
{
    // Start is called before the first frame update
 public  Data  data;

    
    SceneControl sceneControl;
    private void Awake()
    {
       
            
          
        
    }

    public Vector3 GetPlayerDataPos()
    {
        if (SaveLoad.SaveExists("SavedData"))
        {

            data = SaveLoad.Load<Data>("SavedData");
            Debug.LogError("Loaded" + data.GetLasPos());
        }
        return data.GetLasPos();
    }

    private void Start()
    {
        sceneControl = FindObjectOfType<SceneControl>();
       
    }
    // string DefaltScene= "Map";
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            Loading();
        }
    }
    public void Saving(Inventory inventory , HashSet<string> CollectedIDs, playerData player, Vector3 lastPos)
    {

        data = new Data(inventory.equipmentIDs,inventory.StoredItems, player, sceneControl.GetsceneName(), CollectedIDs, lastPos);
        SaveLoad.Save<Data>(data,"SavedData");
    }
    public void Loading()
    {
        
        if (SaveLoad.SaveExists("SavedData"))
        {
            PlayerPrefs.SetFloat("Isloading", 1) ;
            data= SaveLoad.Load<Data>("SavedData");
            SaveLoad.Save<List<StoredItemData>>(data.equipments, "Equipments");
            SaveLoad.Save<List<StoredItemData>>(data.inventory, "Inventory");
            SaveLoad.Save<playerData>(data.playerData, "PlayerData");
            SaveLoad.Save<HashSet<string>>(data.collectibleSet, "CollectedItems");
            Debug.Log(data.GetLasPos());
            StartCoroutine(delay(data.SavedSceneName));
        }
        else
        {
            Debug.Log("NoSavedData");
        }
    
    }
    IEnumerator delay(string sceneName)
    {

        yield return new WaitForSeconds(0.1f);
        sceneControl.Gotolevel(sceneName);
    }

}
[System.Serializable]
public class Data
{
    float x;
    float y;
    float z;
    public  List<StoredItemData> inventory;
    public playerData playerData;
    public string SavedSceneName;
    public  List<StoredItemData> equipments;
    public  HashSet<string> collectibleSet;
  
   public Data(List<StoredItemData> Equipments, List<StoredItemData> Item, playerData player, string SceneName, HashSet<string> collectible, Vector3 lastPos)
    {
        equipments = Equipments;
        inventory = Item;
        playerData = player;
        SavedSceneName = SceneName;
        collectibleSet = new HashSet<string>( collectible);
        x = lastPos.x;
        y = lastPos.y;
        z = lastPos.z;
    
    }
    public Vector3 GetLasPos()
    {
        return new Vector3(x, y, z);
    }
}
