using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
public class ChaserController : MonoBehaviour
{
    Vector3 TargetPos = new Vector3();
    [SerializeField] int currIndex;
    public int Basespeed=3;
   [SerializeField] int currSpeed;
  public  List<GameObject> Waypoints;
    UnityEvent OnplayerReached = new UnityEvent();
    FieldOfView fieldOfView;
    Vector3 StartPos;
    NavMeshAgent navMesh;
    DialogSystem dialog;
    [SerializeField] DialogSet PrebattleDialog;
    public List<UnitStats> enemyList;
    float timer;
    public bool canMove = true;
  
    public Animator animator;

    public bool SpecificScene;
    public Environment BattleEnvironment;
    public AudioClip BattleMusic;

    Vector3 playerPos= new Vector3();
    TransitionPanel transitionscript;
    bool idle;
    // Start is called before the first frame update

    private void Awake()
    {
        navMesh = GetComponent<NavMeshAgent>();
        StartPos = transform.position;
        currSpeed = Basespeed;
        if(GetComponent<EnemyContainers>().IsBoss||GetComponent<FirstEnemy>()) canMove = false;
        if (!hasWaypoints())
        {
            canMove = false;
        }
        idle = canMove;
        
    }


    public bool hasWaypoints()
    {
        return Waypoints.Count > 1;
    }
    void Start()
    {
        transitionscript = FindObjectOfType<TransitionPanel>();
      //  StartPos = transform.position;
      //  StartPos = transform.position;
        fieldOfView = GetComponent<FieldOfView>();
     
            OnplayerReached.AddListener(EnemyDialog);
      
        GoToNearestWaypoint();
        StartCoroutine(NavUpdate());


    }
    public void CharacterSetUp()
    {
        GetComponent<EnemyContainers>().isDead = false;
        transform.position = StartPos;

        GoToNearestWaypoint();
    }
    // transform.rotation = StartPos.rotation;

    // Update is called once per frame
    void Update()
    {
        if (canMove)
        {
            Roam();
        }
        if ( Vector3.Distance(transform.position, navMesh.destination)>0.1&&canMove)
        {
            animator.SetFloat("Speed", navMesh.speed);

        }
        else 
        {
            animator.SetFloat("Speed", 0);
        }
    }

  public  void GoToNearestWaypoint()
    {
        TargetPos = Waypoints[0].transform.position;
            for (int i = 0; i < Waypoints.Count; i++)
            {
                if (Vector3.Distance(TargetPos, transform.position) > Vector3.Distance(Waypoints[i].transform.position, transform.position))
                {
                    TargetPos = Waypoints[i].transform.position;
                currIndex = i;
                }
            }
       
    }
    void Roam()
    {
        if (fieldOfView.canSeePlayer)
        {
            TargetPos = transform.position;
            Chase();
            UnityEngine.Random.Range(1, 0);
            GoToNearestWaypoint();
        }
        else
        {
            timer = 1;
            if (Vector3.Distance(TargetPos, transform.position) < 1)
            {
                //float x = transform.position.x + UnityEngine.Random.Range(-10.0f, 10.0f);
                //float z = transform.position.z + UnityEngine.Random.Range(-10.0f, 10.0f);
                currIndex++;
                currIndex = currIndex % Waypoints.Count;
                TargetPos = Waypoints[currIndex].transform.position;

            }
            else
            {
                Vector3 dir = (transform.position - TargetPos).normalized;
                if (navMesh.destination != TargetPos)
                {
                    navMesh.SetDestination(TargetPos);
                }
                //  transform.position -= dir * Time.deltaTime*currSpeed;
                transform.LookAt(TargetPos);
            }
        }
    }
    void Chase()
    {
        Vector3 dir = (transform.position - fieldOfView.playerRef.transform.position).normalized;
        //  transform.position -= dir * Time.deltaTime*currSpeed;
        if (navMesh.destination != fieldOfView.playerRef.transform.position)
        {
            navMesh.SetDestination(fieldOfView.playerRef.transform.position);
        }
       
        transform.LookAt(new Vector3(fieldOfView.playerRef.transform.position.x,0, fieldOfView.playerRef.transform.position.z));
        timer = 0.1f;
    }

    public void OnStop()
    {
        Basespeed = 3;
        navMesh.speed = 0;
      
    }

    public void UnStop()
    {
        navMesh.speed = 3;
        currSpeed = Basespeed;
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!GetComponent<EnemyContainers>().isDead)
            {
                OnplayerReached?.Invoke();
                Vector3 dir = (transform.position - other.transform.position).normalized;

                playerPos = other.gameObject.transform.position;
                playerPos.y += 2f;

                FindObjectOfType<BattleInfo>().GetEnemyList(enemyList);
            }
        }
    }


    public void EnemyDialog()
    {
        if (PrebattleDialog)
        {
            if (!dialog)
            {
                dialog = FindObjectOfType<DialogSystem>();
            }

            dialog.EvtDialogStart.Invoke(PrebattleDialog);
            dialog.EvtDialogEnd.AddListener(transitionscript.transitioningToBattleScene);
            transitionscript.TransitionToBattleSceneEnd.AddListener(BattlePhase);
        }
        else
        {
            transitionscript.transitioningToBattleScene();
            transitionscript.TransitionToBattleSceneEnd.AddListener(BattlePhase);


        }
    }
    void BattlePhase()
    {
        string BattleScene = "BattleScene";
        if (SpecificScene)
        {
            BattleScene = "RooftopScene";
        }
        dialog.EvtDialogEnd.RemoveListener(BattlePhase);
        Debug.LogWarning(transform.position);
        FindObjectOfType<BattleInfo>().BattleStart(GetComponent<EnemyContainers>().ID, playerPos, BattleScene, BattleEnvironment, BattleMusic);
        GetComponent<EnemyContainers>().EvtDied.Invoke(this.gameObject);
    }


    IEnumerator NavUpdate()
    {
        while( true)
        {
           // Roam();
            yield return new WaitForSeconds(timer); 
           

        }
    
    }
}
