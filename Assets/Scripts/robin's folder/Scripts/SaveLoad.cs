using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveLoad : MonoBehaviour
{
    void Start()
    {
        Debug.Log(Application.persistentDataPath);
    }



    // Start is called before the first frame update
    public static void Save<T>(T objectToSave, string key)
    {
        string path = Application.persistentDataPath + "/saves/";
        Directory.CreateDirectory(path);
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        using (FileStream fileStream = new FileStream(path + key + ".txt", FileMode.Create))
        {
            binaryFormatter.Serialize(fileStream,objectToSave);
        }
    }


    public static T Load<T>(  string key)
    {
        string path = Application.persistentDataPath + "/saves/";
        
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        T retrunValue = default(T);
        using (FileStream fileStream = new FileStream(path + key + ".txt", FileMode.Open))
        {
          //  Debug.LogError("file range" + fileStream.Length);
            retrunValue = (T)binaryFormatter.Deserialize(fileStream);
        }
        return retrunValue;
    }

    public static bool SaveExists(string key)
    {
        string path = Application.persistentDataPath + "/saves/"+key+".txt";
       
        return File.Exists(path);

    }

    public static void CreateSaveData()
    {
        string path = Application.persistentDataPath + "/saves/";
        Directory.CreateDirectory(path);
    }

    public static void DeleteAllSaveData()
    {
        string path = Application.persistentDataPath + "/saves/";
        DirectoryInfo directory = new DirectoryInfo(path);
        if(directory!=null) directory.Delete(true);
        Directory.CreateDirectory(path);
    }

    public static void DeleteData(string key)
    {
        string path = Application.persistentDataPath + "/saves/" + key + ".txt";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
        else
        {
            Debug.LogError(path+" Doesn't exists");
        }

    }
} 