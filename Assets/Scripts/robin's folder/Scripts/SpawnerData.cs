using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnemySpawnData
{
    public int num;
   public GameObject Prefab;
}

[CreateAssetMenu]
public class SpawnerData :ScriptableObject
{
    // Start is called before the first frame update

    public List<EnemySpawnData> enemySpawnDatas;

}
