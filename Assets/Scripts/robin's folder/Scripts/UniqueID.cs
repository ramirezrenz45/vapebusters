using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniqueID : MonoBehaviour
{
    // Start is called before the first frame update
    public string ID { get; private set; }
    private void Awake()
    {
        ID = transform.position.magnitude + name + "-" + transform.transform.GetSiblingIndex();
    }
}
