using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GlobalEvents : MonoBehaviour
{
    //public static GlobalEvents instance= null;
    public static System.Action EvtSaveCharacterState;
    public static System.Action SaveWorldState ;
    public static System.Action Pause;
    public static System.Action UnPause;

    public bool disableShortcuts= false;
   // private void Awake()
    //{
    //    if (instance != null)
    //    {
    //        Destroy(this.gameObject);
    //    }
    //    else
    //    {
    //        instance = this;
    //    }
    //}

  
    private void Awake ()
    {
        Pause = null;
        UnPause = null;
        SaveWorldState = null;

       

    }

    public void OnPause()
    {
        Pause?.Invoke();
    }
    public void OnUnpaused()
    {
        UnPause?.Invoke();
    }


    public void OnMapChange()
    {
       SaveWorldState?.Invoke();
    }
    public void onSaveInitiated()
    {
        EvtSaveCharacterState?.Invoke();
    }

   
     



}
