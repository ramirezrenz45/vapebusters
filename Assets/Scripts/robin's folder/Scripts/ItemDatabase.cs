using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public static ItemDatabase Instance= null;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);

        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    public List<ItemType> Database;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public ItemType GetFromID(int id)
    {
        return Database.Find(item => item.ID == id);

    }
    public ItemType GetFromName(string name)
    {
        return Database.Find(item => item.Name == name);

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
