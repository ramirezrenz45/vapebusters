using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
public class Shop : MonoBehaviour
{
    // Start is called before the first frame update
    public List<ItemType> ShopMenu;
    // public GameObject contanerPrefab;
    //   public GameObject ShopUIPanel;
    UIManager manager;
    private void Start()
    {
        manager =FindObjectOfType<UIManager>();

        Debug.LogError("shopOpen");
    }


    public void OpenShop()
    {
        manager.OpenShop(ShopMenu);

    }

    public void Close()
    {
      //  manager.OpenMenu();
    }
    public void Buy(ItemType item)
    {
        if (Player.Instance.playerData.Gold >=item.Cost)
        {
            Player.Instance.playerData.GoldChange(-item.Cost);
        
                

        }
        else
        {
            Debug.LogError("Not Enough Gold");
        }
    
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.LogError("Shop");
        if (collision.gameObject.CompareTag("Player"))
        {
            OpenShop();
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Close();
        }
    }
}
