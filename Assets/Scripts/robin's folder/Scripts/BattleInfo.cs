using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;


public class BattleInfo : MonoBehaviour
{
    public string CurrEnemyID;
    public string currMap;
    public List<UnitStats> EnemyList;
    public List<PlayerStatsTest> PlayerStats;
    public List<UnitStats> PlayerList;
    public List<Vector3> EscapeLocations;
    public Vector3 escapeLocation;
    public AudioClip LevelBGM;
    public AudioClip DefaultBattleMusic;
    AudioClip battleMusic;
    bool BattleScene;


    public bool LevelStarted = false;
    //list<enemies>();
    SceneControl sceneControl;
   public Vector3 PlayerPos;
    bool CameFromBatttle= false;
    public Environment BattleEnv;



    public void clearVariables()
    {
        CurrEnemyID = "";
        currMap = "";
        EnemyList = new List<UnitStats>();
        EscapeLocations = new List<Vector3>();
        PlayerPos = new Vector3(0, 1, 0);
        CameFromBatttle = false;

    }
    public void clearData()
    {
//        //GetComponent<CollectibleSet>().CollectedItems.Clear();
//        //GetComponent<Player>().inventory= new Inventory();
//       // data = SaveLoad.Load<Data>("SavedData");
//        SaveLoad.Save<List<StoredItemData>>(new List<StoredItemData>(), "Equipments");
//        SaveLoad.Save<List<StoredItemData>>( new List<StoredItemData>(), "Inventory");
//        SaveLoad.Save<playerData>(new playerData(), "PlayerData");
//        SaveLoad.Save<HashSet<string>>(new HashSet<string>(), "CollectedItems");
    

    }

    private void Awake()
    {
        EscapeLocations = new List<Vector3>();
        PlayerList = new List<UnitStats>();
        sceneControl = GetComponent<SceneControl>();
        SceneManager.sceneLoaded += OnTransition;

    }


    public void MoveToNewMap()
    {
        EscapeLocations = new List<Vector3>();
    }


    // Start is called before the first frame update
    void Start()
    {
        bool NewGame = true;
        if (SaveLoad.SaveExists("PlayerData"))
        {
            NewGame = false;
        }
        if(NewGame)
        {
            NewPlayerStats();
        }
        //NewPlayerStats();
        Player.Instance.SaveStat(PlayerStats);
    }

    public void TransitionToWorld(DialogSet dialog)
    {
        foreach (PlayerStatsTest p in PlayerStats)
        {
            p.EquipmentManagerCheck();
        }
            Debug.Log("TransitionStarted");
            TransitionPanel transition = FindObjectOfType<TransitionPanel>();
            if (transition)
            {
                GetComponent<GlobalEvents>().OnPause();
                transition.transitioningToWorldScene();
                transition.dialog = dialog;
                transition.TransitionToWorldSceneEnd.AddListener(TransitonEnd);
            
        }
        CheckPlayerModel();
    }

    public void TransitonEnd(DialogSet dialog)
    {
        Debug.Log("TransitionFinished");
        GetComponent<GlobalEvents>().OnUnpaused();
        if (dialog)
        {
            FindObjectOfType<DialogSystem>().EvtDialogStart.Invoke(dialog);
        }
        ObjectiveTracker.instance.GetUI();
    }

    public void CheckPlayerModel()
    {
        if (PlayerStats[0].currentPlayerStats.Position == 0)
        {
            GameObject.FindWithTag("Player").GetComponent<PlayerModel>().ChangePlayerModel(true);
        }
        else
        {
            GameObject.FindWithTag("Player").GetComponent<PlayerModel>().ChangePlayerModel(false);
        }
    }

    public void NewPlayerStats()
    {
        PlayerList.Clear();
        foreach(PlayerStatsTest p in PlayerStats)
        {
            PlayerList.Add(p.newStat());
        }

        //CheckPlayerModel();
    }

    public void UpdatePlayerStats()
    {
        foreach (PlayerStatsTest p in PlayerStats)
        {
            for (int y = 0; y < BattleManager.instance.PlayerList.Count; y++)
            {
                if (BattleManager.instance.PlayerList[y].Unit_Stats == p.currentPlayerStats)
                {
                    p.currentPlayerStats.CurrentHP = BattleManager.instance.PlayerList[y].currHP = BattleManager.instance.PlayerList[y].currHP;
                    break;
                }
                else
                {
                    p.currentPlayerStats.CurrentHP = 1;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    OnWin();

        //    Player.Instance.Save();
        //}

    }

    void OnTransition(Scene scene, LoadSceneMode mode)
    {

        if (scene.name == "BattleScene" || scene.name == "RooftopScene")
        {
            foreach (PlayerStatsTest p in PlayerStats)
            {
                Debug.Log("Leaving World Scene");
                p.LeavingWorldScene();
            }
            //AudioClip musicToPlay = (battleMusic == null) ? DefaultBattleMusic : battleMusic;
            //VolumeManager.instance.PlayBGM(battleMusic);
            //EnvironmentManager.instance.ChangeEnvironment(BattleEnv);
            BattleScene = true;
            //Invoke("StartBattle", 0.1f);
        }
        else
        {
            if (VolumeManager.instance)
            {
                VolumeManager.instance.StopBGM();
                VolumeManager.instance.PlayBGM(LevelBGM);
            }
            BattleScene = false;
            ObjectiveTracker.instance.GetUI();
        }
    }

    public void StartBattle()
    {

        BattleManager.instance.BattleSetup(PlayerList, EnemyList);
        BattleUIManager.instance.EventOccured("Enemies are blocking your way!");
        BattleUIManager.instance.OpenEvent();
        AudioClip musicToPlay = (battleMusic == null) ? DefaultBattleMusic : battleMusic; 
        VolumeManager.instance.PlayBGM(musicToPlay);
    }

    public void BattleStart(string id, Vector3 pos, string SceneName, Environment battleEnv, AudioClip BattleBGM)//list enemies)
    {
        VolumeManager.instance.PauseBGM();
        CurrEnemyID = id;
        currMap= sceneControl.GetsceneName();
        BattleEnv = battleEnv;
        battleMusic = BattleBGM;
        GlobalEvents.SaveWorldState.Invoke();
        sceneControl.Gotolevel(SceneName);
        PlayerPos = pos;
    }

    //on lose
    void onlost()
    {
        CurrEnemyID = "";
        VolumeManager.instance.StopBGM();
        Player.Instance.SaveStat(PlayerStats);
        sceneControl.Gotolevel(currMap);
    }

    public void OnWin()
    {
        VolumeManager.instance.StopBGM();
        Player.Instance.SaveStat(PlayerStats);
        sceneControl.Gotolevel(currMap);
    }

    public void OnEscape()
    {
        VolumeManager.instance.StopBGM();
        CurrEnemyID = "";
        UpdatePlayerStats();
        sceneControl.Gotolevel(currMap);
        PlayerPos = EscapeLocations[Random.Range(0, EscapeLocations.Count)];
    }

    public void GoToMainMenu()
    {
        VolumeManager.instance.StopBGM();
        sceneControl.Gotolevel("MainMenu");
        Destroy(this.gameObject);
    }

    public void GetEnemyList(List<UnitStats> enemyList)
    {
        EnemyList = enemyList;
    }

    public void GoToLevel(string sceneName, Vector3 newPosition)
    {
        sceneControl.Gotolevel(sceneName);
        PlayerPos = newPosition;

    }

    public void GiveReward(int recievedExp,int recievedGold)
    {
        foreach (PlayerStats item in PlayerList)
        {
            item.GainExp(recievedExp);
        }
        Player.Instance.playerData.GoldChange(recievedGold);
    }

    public void LoadCharacterData(playerData PlayerData)
    {
        Debug.Log("Loading Character Data");
        NewPlayerStats();

        Debug.Log(PlayerList.Count);
        for(int x = 0; x < PlayerList.Count; x++)
        {
            Debug.Log("Player List " + x + " = ");
            PlayerStats p = PlayerList[x] as PlayerStats;
            Debug.Log(p == null);
            Debug.Log("Player List " + x + " = "+ PlayerList[x] == null);
            Debug.Log(" Loading data for " + p.UnitName);
            if (p != null)
            {
                p.LoadData(PlayerData.partyDatas[x]);
            }
        }
    }


    //on lose

}
