using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class ItemType : ScriptableObject
{
    public int ID;
    public string Name;
    public bool Stackable;
    public Sprite ItemSprite;
    public int Cost;
    public string Description;
 
    public ItemType()
    { 
    
    }
  public virtual  bool Use()
    {
        return false;
    }

    // Start is called before the first frame update
   


}
