using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleSet : MonoBehaviour
{
    public static CollectibleSet Instance = null;
    public HashSet<string> CollectedItems { get; private set; } = new HashSet<string>();
    public int a;
    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this.gameObject);

        }
        else
        {
            Instance = this;
           
        }
       Load();
        a = CollectedItems.Count;
    }

    public void AddItem(string x)
    {
        CollectedItems.Add(x);
        Save();
        //GlobalEvents.SaveWorldState();
        a = CollectedItems.Count;
        string content = "content: ";
        foreach (string item in CollectedItems)
        {
            content += item + ",";
        }
        Debug.Log(content);
    }
    private void Start()
    {
        
        string content="content: ";
        foreach (string item in CollectedItems)
        {
             content+= item+",";
        }
        Debug.Log(content);
        GlobalEvents.SaveWorldState += Save;
        GlobalEvents.SaveWorldState();
    }
    public void Save()
    {
        
        SaveLoad.Save<HashSet<string>>(CollectedItems, "CollectedItems");
    }

    private void Update()
    {
        
    }
    public void Load()
    {
        if (SaveLoad.SaveExists("CollectedItems"))
        {
            CollectedItems= new HashSet<string>( SaveLoad.Load<HashSet<string> > ("CollectedItems"));


        }
  
    }
}
