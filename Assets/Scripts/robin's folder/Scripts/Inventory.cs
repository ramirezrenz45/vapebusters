using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Inventory
{
    // Start is called before the first frame update
   // public Dictionary<int, StoredItemData> items= new Dictionary<int, StoredItemData>();
   public List<StoredItemData> StoredItems= new List<StoredItemData>() ;
    public List<StoredItemData> equipmentIDs= new List<StoredItemData>();
    public Inventory()
    {
        //items = new List<ItemType>();

    }
    public void AddItem(ItemType item)
    {

        bool Stacked = false;
        if (item.Stackable)
        {
            foreach (StoredItemData x in StoredItems)
            {
                if (item.ID == x.ItemTypeID)
                {
                    x.Addamount(1);
                    Stacked = true;
                }
                


            }
           
        }
        if (!Stacked)
        {
            ItemType e = (ItemDatabase.Instance.GetFromID(item.ID));
            Equipment d = e as Equipment;
            if (d)
            {
                equipmentIDs.Add(new StoredItemData(d.ID, 1,d.name));
              
            }
            else
            {
                Debug.Log(item.ID);
                Debug.Log(item.name);
                StoredItems.Add(new StoredItemData(item.ID, 1, item.name));
                Debug.LogError(e.name +"x");
            }

        }
    }
    public void AddItems(List< StoredItemData> _items)
    {
        //  if (_items!=null)
        // {
        foreach (StoredItemData obj in _items)
        {
           StoredItems.Add(obj);

        }
        // }
    }
    public List<StoredItemData> GetItems()
    {

        return StoredItems;
    }


  
    public void Save()
    {
        SaveLoad.Save<List<StoredItemData>>(StoredItems, "Inventory");
        SaveLoad.Save<List< StoredItemData >> (equipmentIDs, "Equipments");
    }
    public void Load()
    {
       // items.Clear();
        if (SaveLoad.SaveExists("Inventory"))
        {
            //  AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));
            AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));

            foreach (StoredItemData item in StoredItems)
            {
                Debug.LogWarning(item.ItemTypeID+item.Amount);
            }
        }

    }

    public List<Equipment> GetEquipment()
    {
        List<Equipment> equipments= new List<Equipment>();
        foreach (StoredItemData item in equipmentIDs)
        {
          ItemType e=  ItemDatabase.Instance.GetFromID(item.ItemTypeID) ;

            Equipment a = e as Equipment;
           if (a)
            {
             
                equipments.Add(a);
                
            }
            
          
        }
        return equipments;
    }

    public bool Isavailable(ItemType Item)
    {
        bool available= false;
        Equipment I = Item as Equipment;

        if (I!=null)
        {   foreach (StoredItemData ite in equipmentIDs)
            {
                if (ite!=null)
                {
                    if (ite.ItemTypeID.ToString() + ite.name == (I.ID.ToString() + I.name))
                    {
                        available = true;
                        break;
                    }
                }
            }

        }
         
        else
        {
            foreach (StoredItemData item in StoredItems)
            {
                if (item != null)
                {
                    if (item.ItemTypeID.ToString() + item.name == (Item.ID.ToString() + Item.name))
                    {
                        available = true;
                        break;
                    }
                }
            }
        }
        return available;
    }

    public void Remove(ItemType Item)
    {
       
        for (int x=0;x< StoredItems.Count;x++)
        {
            if (StoredItems[x].ItemTypeID.ToString()+ StoredItems[x].name == (Item.ID.ToString() + Item.name))
            {
                if (StoredItems[x].Amount > 1)
                {
                    StoredItems[x].Addamount(-1);
                 
                }
                 else
                {
                   
                        StoredItems.Remove(StoredItems[x]);
                    

                }
                break;
            }
        }
        for (int x = 0; x < equipmentIDs.Count; x++)
        {
            if (equipmentIDs[x].ItemTypeID.ToString() + equipmentIDs[x].name == (Item.ID.ToString() + Item.name))
            {

                Debug.LogError("Removed"+ equipmentIDs[x].name);

                equipmentIDs.Remove(equipmentIDs[x]);

                break;


            }
        }

    }
}
[System.Serializable]
public class StoredItemData
{
   public int ItemTypeID { get; private set; }
    public int ID;
    public string name;
   public int Amount { get; private set; }
   //public Sprite Sprite { get; private set; }
   // public bool Stackable { get; private set; }
    public StoredItemData(int id, int amount, string Name)
    {
        ItemTypeID = id;
        Amount = amount;
        name = Name;
        //Sprite = sprite;
        //Stackable = stackable;
    }
    

    public void Addamount(int addedAmount)
    {
       Amount+= addedAmount;
    }
}



