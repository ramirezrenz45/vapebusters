using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    float dir;
    float maxdistance;
    Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        maxdistance = 1f;
        dir = 1;
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.up *2* dir*Time.deltaTime);
        dir = Vector3.Distance(startPos, transform.position) < -1*maxdistance || Vector3.Distance(startPos, transform.position) > maxdistance ?dir*-1:dir;
    }
}
