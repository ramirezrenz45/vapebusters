using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{

    public GameObject InteractableIndicator;
    public Animator animator; 
    CharacterController controller;
    Vector3 velocity;
    Vector3 forcedMovementDirection;
    float speed=5;
    Interactable interactable;
    public bool CanMove=true;
    DialogSystem dialogSystem;
   [SerializeField] LayerMask interactableLayer;
    bool paralyzed;
    // Start is called before the first frame update
    void Start()
    {
        paralyzed = false;
        dialogSystem = FindObjectOfType<DialogSystem>();
        //dialogSystem.EvtDialogStart.AddListener(OnPlayerPause);
        GlobalEvents.Pause+= OnPlayerPause;

       // dialogSystem.EvtDialogEnd.AddListener(OnPlayerUnPause);
        GlobalEvents.UnPause += OnPlayerUnPause;

        controller = GetComponent<CharacterController>();

    }
    private void Move()
    {
        if (animator != null) animator.SetFloat("Speed", 0);
        Vector3 dir;
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            dir = transform.TransformDirection(0f, 0f, 1f);
            if (animator != null)
            {
                animator.SetFloat("Speed", speed);
            }
        }
        else
        {
            dir = Vector3.zero;
        }
       
        //if (Input.GetAxis("Horizontal") > 0)
        //{

        //    transform.LookAt(Vector3.right);
        //}
        //else if (Input.GetAxis("Horizontal") < 0)
        //{
        //    transform.LookAt(-Vector3.right);

        //}
        //if (Input.GetAxis("Vertical") > 0)
        //{
        //    transform.LookAt(Vector3.forward);
        //}
        //else if (Input.GetAxis("Vertical") < 0)
        //{
        //    transform.LookAt(-Vector3.forward);
        //}
        
        transform.LookAt(new Vector3(Input.GetAxis("Horizontal")+transform.position.x,transform.position.y, Input.GetAxis("Vertical")+transform.position.z));
       

        if (controller.isGrounded)
        {
            velocity.y = -1;
        }
        else
        {
            velocity.y -= 9.8f *2.0f * Time.deltaTime;
        }
        controller.Move(dir * speed * Time.deltaTime);
        controller.Move(velocity*Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (CanMove && !paralyzed)
        {
            Move();
        }
    }

    public void paralysis(Vector3 pos)
    {
        paralyzed = true;
        Vector3 dir = (transform.position - pos).normalized;
        dir.Normalize();
        controller.Move(dir*5);
        StartCoroutine(ParalysisDuration());
    }

    IEnumerator ParalysisDuration()
    {
        int  timer = 5;
        while (timer > 0)
        {
            yield return new WaitForSeconds(0.3f);
            if (CanMove)
            {
                timer--;
            }
        }
        paralyzed = false;

    }

    public void CheckForInteractable()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 2.0f, interactableLayer))
        {
            InteractableIndicator.SetActive(true);
            if (hit.collider.gameObject)
            {
                if (interactable == null)
                {
                    interactable = hit.collider.gameObject.GetComponent<Interactable>();
                }
                else if (Input.GetKeyDown(KeyCode.F))
                {
                    if(interactable)
                    interactable?.interact.Invoke();
                }
            }
            

        }
        else
        {
            InteractableIndicator.SetActive(false);
            if (interactable)
            {
                interactable?.ExitInteraction.Invoke();
                interactable = null;
            }
        
        }
    
    
    }

    public void Stop()
    {

    }

    public void ForceMove(Vector3 direction)
    {
        CanMove = false;
        FindObjectOfType<DialogSystem>().EvtDialogEnd.AddListener(StartingMovement);
        forcedMovementDirection = direction;
    }

    public void StartingMovement()
    {
        StopAllCoroutines();
        StartCoroutine(ForcingMovement(forcedMovementDirection));
    }

    IEnumerator ForcingMovement(Vector3 direction)
    {
        
        int x = 0;
        transform.LookAt(direction);
        while (x < 50)
        {
            if (controller.isGrounded)
            {
                velocity.y = -1;
            }
            else
            {
                velocity.y -= 9.8f * 2.0f * Time.deltaTime;
            }
            controller.Move(direction * speed * Time.deltaTime);
            controller.Move((velocity * Time.deltaTime));
            yield return new WaitForSeconds(0.01f);
            x++;
        }
        yield return new WaitForSeconds(4.0f);
        Debug.Log("Restarting Movement");
        gameObject.GetComponent<CharacterController>().enabled = true;
        CanMove = true;
    }

    private void LateUpdate()
    {
        if (CanMove && !paralyzed)
        {
            CheckForInteractable();
        }
    }
    public void OnPlayerPause()
    {

        CanMove = false;
    }

    public void OnPlayerUnPause()
    {
        CanMove = true;
    }


}
