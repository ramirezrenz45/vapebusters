using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        //SaveLoad.DeleteAllSaveData();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void refreshLevel()
    {
      //  SaveLoad.DeleteAllSaveData();
       // SaveLoad.DeleteData("CollectedItems");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
   public void Gotolevel(string name)
    {
        SceneManager.LoadScene(name);

        GlobalEvents g = GetComponent<GlobalEvents>();
        if (g)
        {
            g.OnMapChange();
        }
    }

    public string GetsceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void OnQuit()
    {
        Application.Quit();

    }

    public void ReturnToMaoinMenu()
    {
        GameObject gameObject = FindObjectOfType<BattleInfo>().gameObject;
        if (gameObject)
        {
            Destroy(gameObject);
        }
        SceneManager.LoadScene("MainMenu");
    }
}
