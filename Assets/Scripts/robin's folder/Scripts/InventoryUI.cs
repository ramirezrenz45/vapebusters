using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class InventoryUI : MonoBehaviour
{
    public GameObject Prefab;
    List<GameObject> Items = new List<GameObject>();
    [SerializeField] GameObject LayoutPanel;
    [SerializeField] GameObject confirmationPanel;
    [SerializeField] GameObject ChooseUnitPanel;
    BattleInfo battleInfo;
    public AudioClip ItemUseSFX;
    public GameObject DescriptionPanel;
    public TextMeshProUGUI DescriptionText;

    [Header("Player 1")]
   [SerializeField] TextMeshProUGUI HPA;
    [SerializeField]Image HPBarA;
   [SerializeField] TextMeshProUGUI LPA;
    [SerializeField] Image LPBarA;
    [SerializeField] TextMeshProUGUI LevelA;
    [SerializeField] TextMeshProUGUI nextLevelA;
    [SerializeField] Image SpriteA;

    [Header("Player 2")]
    [SerializeField] TextMeshProUGUI HPB;
    [SerializeField] Image HPBarB;
    [SerializeField] TextMeshProUGUI LPB;
    [SerializeField] Image LPBarB;
    [SerializeField] TextMeshProUGUI LevelB;
    [SerializeField] TextMeshProUGUI nextLevelB;
    [SerializeField] Image SpriteB;
    ItemType selectedItem;

    private void Awake()
    {

        battleInfo = FindObjectOfType<BattleInfo>();
        StartCoroutine(delay());

    }
    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.01f);
        setUpStats();
    }


    public void setUpStats()
    {

        PlayerStatsTest stat = battleInfo.PlayerStats[0];
        HPA.text= stat.currentPlayerStats.CurrentHP.ToString()+"/"+ stat.currentPlayerStats.MaxHP.ToString();
        HPBarA.fillAmount = stat.currentPlayerStats.CurrentHP / stat.currentPlayerStats.MaxHP;
        LPA.text = stat.currentPlayerStats.CurrentSP.ToString() + "/" + stat.currentPlayerStats.MaxSp.ToString(); 
        LPBarA.fillAmount = stat.currentPlayerStats.CurrentSP / stat.currentPlayerStats.MaxSp;
        LevelA.text = stat.currentPlayerStats.Level.ToString();
        nextLevelA.text = (stat.currentPlayerStats.Level < 20) ? ((stat.currentPlayerStats.ExpRequirement - stat.currentPlayerStats.Exp).ToString()) : " Max Level ";
        SpriteA.sprite = stat.currentPlayerStats.unitImage;
        stat = battleInfo.PlayerStats[1];
        HPB.text = stat.currentPlayerStats.CurrentHP.ToString() + "/" + stat.currentPlayerStats.MaxHP.ToString(); ;
        HPBarB.fillAmount = stat.currentPlayerStats.CurrentHP /stat.currentPlayerStats.MaxHP;
        LPB.text = stat.currentPlayerStats.CurrentSP.ToString() + "/" + stat.currentPlayerStats.MaxSp.ToString();
        LPBarB.fillAmount = stat.currentPlayerStats.CurrentSP / stat.currentPlayerStats.MaxSp;
        LevelB.text = stat.currentPlayerStats.Level.ToString();
        nextLevelB.text = (stat.currentPlayerStats.Level < 20) ? ((stat.currentPlayerStats.ExpRequirement - stat.currentPlayerStats.Exp).ToString()) : " Max Level ";
        if ((stat.currentPlayerStats.ExpRequirement - stat.currentPlayerStats.Exp) < 0){
            nextLevelB.text = " 0 ";
            nextLevelA.text = " 0 ";
        }
        SpriteB.sprite = stat.currentPlayerStats.unitImage;
    }


    public void CloseCharacterSelectPanel()
    {
        VolumeManager.instance.PlaySFX(ItemUseSFX);
        ChooseUnitPanel.SetActive(false);
    }


    public void OnUseButtonPressed()
    {
        if(selectedItem)
        {
            ConsumableItemType con = selectedItem as ConsumableItemType;
            bool canRemove;
            if (con)
            {

                // canRemove=    con.UseConsumable(FindObjectOfType<BattleInfo>().PlayerList);
                canRemove = false;
               
                    ChooseUnitPanel.SetActive(true);
             
                    ChooseUnitPanel.GetComponent<ChooseUnit>().GetinventoryUI(this.GetComponent<InventoryUI>());
                
                    ChooseUnitPanel.GetComponent<ChooseUnit>().SetupUIs(con);
               
               

            }
            else
            {
             canRemove=   selectedItem.Use();
            }
            if (canRemove)
            {
                Player.Instance.RemoveIteminInventory(selectedItem);
            }

         
        }
        selectedItem = null;
        confirmationPanel.SetActive(false);
    }

    public void OnCancelButtonPressed()
    {
        confirmationPanel.SetActive(false);
        selectedItem = null;
    
    }
   
    private void Start()
    {
        Player.Instance.EvtInventoryChange.AddListener(RefreshInventory);
    }

    // Start is called before the first frame update
    void RefreshInventory(Inventory inventory)
    {
        foreach (GameObject item in Items)
        {
            Destroy(item.gameObject);
        }
        Items.Clear();
 
        foreach (StoredItemData item in inventory.StoredItems)
        {

            GameObject pic = Instantiate(Prefab);
            Sprite sprite;
            Debug.Log("Item is = null: " + (item == null));
            sprite = ItemDatabase.Instance.GetFromID(item.ItemTypeID).ItemSprite;
           
            if(LayoutPanel!=null)
            pic.transform.SetParent(LayoutPanel.transform);
           Items.Add(pic);
            InventoryItems _item = pic.GetComponent<InventoryItems>();

          _item.Setup(item,this);
            _item.EvtItemSlotSelect.AddListener(OnItemPressed);
        }
     
        
    }

    public void OnHover(ItemType item)
    {
        DescriptionPanel.SetActive(true);
     
        ConsumableItemType c = item as ConsumableItemType;
        
        if(c)
        DescriptionText.text = c.name + "\n" + c.Description + "\n";
    }

    public void LeftHover()
    {
        DescriptionPanel.SetActive(false);
        DescriptionText.text = " ";
    }

    public void OnItemPressed(ItemType item)
    {
        ConsumableItemType con = selectedItem as ConsumableItemType;

      
      
            confirmationPanel.SetActive(true);
            confirmationPanel.transform.Find("Description").GetComponent<TextMeshProUGUI>().text = "Use " + item.name + " ?";
            selectedItem = item;
        
    
    }
}
