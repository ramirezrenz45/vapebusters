using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer : MonoBehaviour
{
    
    public ItemType itemType;
    public AudioClip PickUpSFX;
    CollectibleSet collectibleSet;
    UniqueID uniqueID;
    private void Awake()
    {
        
    }
    private void Start()
    {
        uniqueID = GetComponent<UniqueID>();
        collectibleSet = FindObjectOfType<CollectibleSet>();

        foreach (var item in collectibleSet.CollectedItems)
        {
           // Debug.LogError(uniqueID.ID + "+" + item);
        }

        if (collectibleSet.CollectedItems.Contains(uniqueID.ID))
        {

            


            Destroy(this.gameObject);
            return;
        }
    }


    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("Player"))
        {
            VolumeManager.instance.PlaySFX(PickUpSFX);
            // GlobalEvents.instance.onSaveInitiated();
            collectibleSet.AddItem(uniqueID.ID);
            FindObjectOfType<DialogSystem>().Notification("You picked up a " + itemType.Name);
            Player.Instance.GotItem(itemType);
            ObjectiveTracker.instance.CheckItem(itemType);
            FindObjectOfType<GlobalEvents>().onSaveInitiated();
            FindObjectOfType<GlobalEvents>().OnMapChange();
            Destroy(this.gameObject);
        }

    }
}
