using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public static Player Instance = null;
   

    public Inventory inventory; //{ get; protected set; }
    [SerializeField]
    public playerData playerData; //{ get; private set; }
   public UnityEvent<Inventory> EvtInventoryChange = new UnityEvent<Inventory>();
   // bool Spawned= false;
    // Start is called before the first frame update
    Vector3 x;
    private void Awake()
    {
   
        if (Instance != null)
        {
            Destroy(this.gameObject);

        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }


        inventory = new Inventory();
        //playerData = new playerData();
        //SaveLoad.DeleteAllSaveData();
      
        //Load();


     
    }
    void Start()
    {
        
        GlobalEvents.EvtSaveCharacterState += inventory.Save;
        Load();
        StartCoroutine(Delay());
    

        Debug.LogError(inventory.equipmentIDs.Count + "Equipments");
       
    }
    IEnumerator Delay()
    {
        for (int i = 0; i < 6; i++)
        {
            EvtInventoryChange.Invoke(inventory);
            yield return new WaitForSeconds(0.01f);
      
        }
      //  SaveLoad.DeleteAllSaveData();
    }

   
    // Update is called once per frame
    void Update()
    {
     //   Debug.LogError(transform.position + " " + x);

        //transform.position = x;
        
    }

    public void SaveStat(List<PlayerStatsTest> units)
    { if (EquipmentManager.instance)
        {
            if (playerData.partyDatas == null)
            {
                playerData = new playerData();
            }

            Debug.Log("Making new PlayerData");
            playerData.partyDatas = new List<CharacterData>();
            playerData.partyDatas.Add(new CharacterData(EquipmentManager.instance.p1Equipment, units[0]));
            playerData.partyDatas.Add(new CharacterData(EquipmentManager.instance.p2Equipment, units[1]));
            Debug.Log(playerData.partyDatas.Count);
        }
    }
    
    public void LoadStat( ref List<UnitStats> units)
    {
        if (playerData.partyDatas.Count != 0)
        {
            foreach (CharacterData item in playerData.partyDatas)
            {
                foreach ( UnitStats unit in units)
                {
                   if( unit.UnitName== item.UnitName)
                    {
                        unit.getStat(item);

                    }
                }
            }
        }
    
    }
    
    public void GotItem(ItemType item)
    {  
      
        inventory.AddItem(item);
       
        Equipment equip = item as Equipment;
        if (equip)
        {
            // EquipmentManager.instance.EquipmentInventory.Add(equip);
           playerData.newEquipment = true;

        }
        else
        {
           playerData.newItem=true;
        }
        EvtInventoryChange.Invoke(inventory);
        FindObjectOfType<GlobalEvents>().onSaveInitiated();
    }
    public void RemoveIteminInventory(ItemType item)
    {
        if (inventory.Isavailable(item ))
        {
            Debug.LogError(item.name+" is Removed");
            inventory.Remove(item);
            EvtInventoryChange.Invoke(inventory);

            
            FindObjectOfType<GlobalEvents>().onSaveInitiated();

        }
        else
        {
            Debug.Log("DontHave Item");
        }
    }
    public void Save()
    {
        SaveLoad.Save<List<StoredItemData>>(inventory.StoredItems, "Inventory");
        SaveLoad.Save<List<StoredItemData>>(inventory.equipmentIDs, "Equipments");
        SaveStat(GetComponent<BattleInfo>().PlayerStats);
        playerData.ObjectiveIndex = ObjectiveTracker.instance.GetObjectiveIndex();
        SaveLoad.Save<playerData>(playerData, "PlayerData");
    }
    public void Load()
    {
        if (SaveLoad.SaveExists("Inventory"))
        {

           
            // inventory.AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));
            inventory.StoredItems = new List<StoredItemData>(SaveLoad.Load<List<StoredItemData>>("Inventory"));
           

            EvtInventoryChange.Invoke(inventory);
           Debug.LogWarning(inventory.StoredItems[0].Amount);
          //  Debug.LogError(inventory.StoredItems.Count);
        }


        if (SaveLoad.SaveExists("Equipments"))
        {
            inventory.equipmentIDs = new List<StoredItemData>(SaveLoad.Load<List<StoredItemData>>("Equipments"));
          
            EvtInventoryChange.Invoke(inventory);
        }

            if (SaveLoad.SaveExists("PlayerData"))
        {

            //  AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));
            playerData = (SaveLoad.Load<playerData>("PlayerData"));
            if(ObjectiveTracker.instance != null)
            {
                ObjectiveTracker.instance.LoadData(playerData.ObjectiveIndex);
            }
            FindObjectOfType<BattleInfo>().LoadCharacterData(playerData);
            
            if (EquipmentManager.instance)
            {
                EquipmentManager.instance.getEquipment(playerData.partyDatas[0].EquipmentID, playerData.partyDatas[1].EquipmentID);
            }
           
        }
    }

    public void LoadEquipment()
    {
        if (EquipmentManager.instance)
        {
            EquipmentManager.instance.getEquipment(playerData.partyDatas[0].EquipmentID, playerData.partyDatas[1].EquipmentID);
        }
    }

    public void LoadInventory()
    {
        if (SaveLoad.SaveExists("Inventory"))
        {


            // inventory.AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));
            inventory.StoredItems = new List<StoredItemData>(SaveLoad.Load<List<StoredItemData>>("Inventory"));


            EvtInventoryChange.Invoke(inventory);
            //  Debug.LogWarning(inventory.StoredItems[0].Amount);
            //  Debug.LogError(inventory.StoredItems.Count);
        }
    }


    public bool GetBossDefeated(int bossNumber)
    {
        if(bossNumber == 1)
        {
            return playerData.Boss1Defeated;
        }
        else if (bossNumber == 2)
        {
            return playerData.Boss2Defeated;
        }
        else
        {
            Debug.LogError("Invalid Boss Number");
            return false;
        }
    }

    public void SetBossDefeated(int bossNumber)
    {
        if (bossNumber == 1)
        {
            if(playerData.Boss1Defeated != true && ObjectiveTracker.instance.objectiveIndex == 4) ObjectiveTracker.instance.ObjectiveComplete();
            playerData.Boss1Defeated = true;
        }
        else if (bossNumber == 2)
        {
            if (playerData.Boss2Defeated != true && ObjectiveTracker.instance.objectiveIndex == 9) ObjectiveTracker.instance.ObjectiveComplete();
            playerData.Boss2Defeated = true;

        }
        else
        {
            
        }
    }
    



}


[System.Serializable]
public class playerData
{
    public List<CharacterData> partyDatas;
    public int Gold { get; private set; }
    public int ObjectiveIndex;
    public bool Boss1Defeated;
    public bool Boss2Defeated;
    public bool newEquipment;
    public bool newItem ;
    public playerData()
    {
        Gold = 20;
        newEquipment = false;
        newItem = false;
    }
    public void GoldChange(int gold)
    {
        Gold += gold;

    }

}
[System.Serializable]
public class PartyData
{ 
  


}
[System.Serializable]
public class CharacterData
{
    int CurrentSP;
    int CurrHp;
 
  
    public string UnitName;
    public Stat Attack;
    public Stat Defense;
    public Stat Speed;
    public float currHP;
    public float MaxHP;
    public float MaxSp;
    public int Level;
    public int Position;
   
    public int Exp;
   
    public int[] EquipmentID;
    public CharacterData(Equipment[] equipments,PlayerStatsTest unit)
    {
        if (equipments.Length!=0)
        {
            EquipmentID = new int[4];
            for (int x = 0; x < equipments.Length; x++)
            {
                if (equipments[x] != null)
                {
                    EquipmentID[x] = equipments[x].ID;
                }
                else
                {

                    EquipmentID[x] = -1;
                }
        
            }
        }
        UnitName = unit.currentPlayerStats.UnitName;
        Attack = unit.currentPlayerStats.Attack;
        Defense = unit.currentPlayerStats.Defense;
        Speed = unit.currentPlayerStats.Speed;
        MaxHP = unit.currentPlayerStats.MaxHP;
        MaxSp = unit.currentPlayerStats.MaxSp;
        currHP = unit.currentPlayerStats.CurrentHP;
        Level = unit.currentPlayerStats.Level;
        Position = unit.currentPlayerStats.Position;
        Exp = unit.currentPlayerStats.Exp;


    }
}