using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour
{
    // public SpawnerData data;

    BattleInfo battleInfo;
 public string LastEnemyID { get; private set; }
   public GameObject PlayerPrefab;
    DialogSystem dialogSystem;
    public Dictionary<string, EnemyData> EnemyDatas= new Dictionary<string, EnemyData>();
     public List<GameObject> enemies = new List<GameObject>();
   public List<GameObject> SpawnedObjects;
    private void Awake()
    {
        dialogSystem = FindObjectOfType<DialogSystem>();
       // dialogSystem.EvtDialogStart.AddListener(OnPause);
        GlobalEvents.Pause += OnPause;
        GlobalEvents.UnPause += OnUnpause;
        dialogSystem.EvtDialogEnd.AddListener(OnUnpause);
        EnemyContainers[] enemies = FindObjectsOfType<EnemyContainers>();
       // Debug.LogError(enemies.Length);
        Load();
        battleInfo = FindObjectOfType<BattleInfo>();
         LastEnemyID = battleInfo.CurrEnemyID;
        Vector3 pos = battleInfo.PlayerPos;
        if(PlayerPrefs.GetFloat("Isloading")==1)
        {
            pos = FindObjectOfType<SaveData>().GetPlayerDataPos();
            PlayerPrefs.SetFloat("Isloading", 0);

        }
       


        Instantiate(PlayerPrefab,pos , Quaternion.identity);//1.7922
        if (EnemyDatas.ContainsKey(LastEnemyID))
        {
            if (EnemyDatas[LastEnemyID].isBoss)
            {
                // collectibleSet = FindObjectOfType<CollectibleSet>();
                CollectibleSet.Instance.AddItem(LastEnemyID);
              //  collectibleSet.Save();
                if (CollectibleSet.Instance.CollectedItems.Count == 0)
                {
                    Debug.LogError("no IDs");
                }
                foreach (string n in CollectibleSet.Instance.CollectedItems)
                {
                    Debug.Log("ID " + n);
                }
                if (CollectibleSet.Instance.CollectedItems.Contains(LastEnemyID))
                {
                    Debug.Log("contains");
                }
                else
                {
                    Debug.Log("notContain");
                }
                Debug.LogError("added" + LastEnemyID);

            }
            else
            {
                EnemyDatas[LastEnemyID].CooldownTime += 10;
            }
        }


    }
    public void Disable(GameObject gameObject)
    {

        gameObject.SetActive(false);

    }

    public void OnPause()
    {
        foreach (GameObject item in enemies)
        {
            item.GetComponent<ChaserController>().OnStop();
        }    
    }
    public void OnUnpause()
    {
        foreach (GameObject item in enemies)
        {
            item.GetComponent<ChaserController>().UnStop();
        }

    }

    public void RemoveFromList(GameObject g)
    {
        enemies.Remove(g);
    }


    public void SaveEemeySaveState()
    {
        SaveLoad.Save< Dictionary<string, EnemyData>>(EnemyDatas, "SpawnerData");

    }
    public void Load()
    {
        // items.Clear();
        if (SaveLoad.SaveExists("SpawnerData"))
        {
            //  AddItems(SaveLoad.Load<List<StoredItemData>>("Inventory"));
            EnemyDatas=(SaveLoad.Load<Dictionary<string, EnemyData>>("SpawnerData"));

         //   foreach (Dictionary<int, EnemyData> item in StoredItems)
          //  {
            //    Debug.LogWarning(item.ItemTypeID + item.Amount);
           // }
        }

    }

    public EnemyData GetEnemyData(string id,GameObject enemy, bool isBoss)
    {
        EnemyData data = new EnemyData();
        if (EnemyDatas.ContainsKey(id))
        {
            data = EnemyDatas[id];
        }
        else
        {
            EnemyDatas.Add(id, data);
        }

        data.isBoss = isBoss;

        enemies.Add(enemy);
        return data;


    }
   

    // Start is called before the first frame update


    void Start()
    {
        GlobalEvents.SaveWorldState += SaveEemeySaveState;
        GlobalEvents.Pause += OnPause;
        GlobalEvents.UnPause += OnUnpause;
    }


   public void Respawning(GameObject enemy)
    {
        StartCoroutine(Respawn(enemy));
    }


    IEnumerator Respawn(GameObject enemy)
    {
        int x = enemy.GetComponent<EnemyContainers>().enemyData.CooldownTime;


        while (enemy.GetComponent<EnemyContainers>().enemyData.CooldownTime > 0)
        {

            enemy.SetActive(false);

            yield return new WaitForSeconds(1.0f);
            enemy.GetComponent<EnemyContainers>().enemyData.CooldownTime--;
            //enemy.GetComponent<EnemyContainers>().isDead = false;
            enemy.GetComponent<ChaserController>().CharacterSetUp();
            enemy.SetActive(true);
        }
      //  enemy.GetComponent<EnemyContainers>().enemyData.CooldownTime = x;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }



}

[System.Serializable]
public class EnemyData
{

    public int CooldownTime;
    public bool isBoss;
}