using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopUI : MonoBehaviour
{
    [SerializeField] GameObject ContainerPrefab;
    [SerializeField] GameObject ShopPanel;
    [SerializeField] GameObject Shop;
    [SerializeField] TextMeshProUGUI GoldText;
    [SerializeField] GameObject ConfirmationPanel;
    [SerializeField] GameObject NotificationPanel;
    DialogSet ShopClosingDialog;
    UIManager uIManager;
    DialogSystem dialogSystem;
    public List<GameObject> Containers = new List<GameObject>();

    public void SetShopClosingDialog(DialogSet dialog)
    {
        ShopClosingDialog = dialog;
    }
    private void Start()
    {
    dialogSystem =  FindObjectOfType<DialogSystem>();
        
        uIManager = FindObjectOfType<UIManager>();
    }
    public void CloseConfirmationPanel()
    {
        ConfirmationPanel.GetComponent<ConfirmationPanel>().text.text = "";

        ConfirmationPanel.GetComponent<ConfirmationPanel>().EvtConfirm.RemoveAllListeners();
        ConfirmationPanel.SetActive(false);
    }
   
    public void CloseNoficationPanel()
    {
        NotificationPanel.SetActive(false);

    }
    public void OpenedShop(List<ItemType> items)
    {
        Shop.SetActive(false);
        foreach (GameObject item in Containers)
        {
            Destroy(item.gameObject);
        }
        Containers.Clear();
        Shop.SetActive(true);

        Debug.LogError(items.Count);
        foreach (ItemType item in items)
        {
            GameObject pic = Instantiate(ContainerPrefab);
            pic.transform.Find("ItemSprite").GetComponent<Image>().sprite = item.ItemSprite;
            pic.transform.Find("ItemName").GetComponent<TextMeshProUGUI>().text = item.name;
            pic.transform.Find("PriceText").GetComponent<TextMeshProUGUI>().text = item.Cost.ToString();

            pic.transform.SetParent(ShopPanel.transform);
            pic.transform.localScale = new Vector3(1, 1, 1);
            GoldText.text = Player.Instance.playerData.Gold.ToString();

            Buy buyScipt = pic.GetComponent<Buy>();
            if (buyScipt)
            {
                buyScipt.item = item;
                buyScipt.onClick.AddListener(onBuy);
                buyScipt.confirmationPanel = ConfirmationPanel;
                buyScipt.NotificationPanel = NotificationPanel;
            }
            Containers.Add(pic);

        }
    }
    public void CloseShop()
    {
        //Shop.SetActive(false);
        //foreach (GameObject item in Containers)
        //{
        //    Destroy(item.gameObject);
        //}
        //Containers.Clear();
        if (!ShopClosingDialog)
        {
            uIManager.CloseAllPanel();
        }
        else
        {
            dialogSystem.EvtDialogStart.Invoke(ShopClosingDialog);
            dialogSystem.EvtDialogEnd.AddListener(CloseShopDialog);
        }
    }


    public void CloseShopDialog()
    {
        dialogSystem.EvtDialogEnd.RemoveListener(CloseShopDialog);
        uIManager.CloseAllPanel();
    }



    void onBuy(ItemType item)
    {
        if (Player.Instance.playerData.Gold >= item.Cost)
        {
            Player.Instance.playerData.GoldChange(-item.Cost);
            Debug.LogError(" Gold :" + Player.Instance.playerData.Gold);
            Player.Instance.GotItem(item);
            // FindObjectOfType<GlobalEvents>().onSaveInitiated();

            GoldText.text = Player.Instance.playerData.Gold.ToString();
        }
        else
        {
            Debug.LogError("Not Enough Gold");
        }
    }
}
