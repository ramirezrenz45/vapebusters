using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class InventoryItems : MonoBehaviour
{

   public UnityEvent<ItemType> EvtItemSlotSelect = new UnityEvent<ItemType>();
    ItemType Item;
    InventoryUI inventoryUI;

    private void Start()
    {

    }
    // Start is called before the first frame update
    public void Setup(StoredItemData item,InventoryUI UI)
    {

        inventoryUI = UI;
        Item = ItemDatabase.Instance.GetFromID(item.ItemTypeID);
        transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "x " + item.Amount.ToString();
        transform.Find("Sprite").GetComponent<Image>().sprite = Item.ItemSprite;
        transform.Find("Name").GetComponent<TextMeshProUGUI>().text = item.name;
       
        if (item.Amount > 1)
        {

        }
        else
        {
            transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "";
        }
    }
    public void OnSlotPressed()
    {
        EvtItemSlotSelect?.Invoke(Item);
    }

    public void OnHover()
    {
        inventoryUI.OnHover(Item);
    }

    public void OnExit()
    {
        inventoryUI.LeftHover();
    }
}
