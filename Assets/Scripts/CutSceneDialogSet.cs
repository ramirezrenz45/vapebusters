using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class CutSceneDialogSet : ScriptableObject
{
    public List<CutSceneDialogs> dialogs;
}
[System.Serializable]
public class CutSceneDialogs
{
   // public Sprite sprite;
    public Sprite BG;
   // public string Speaker;
    public string Dialog;
   public AvatarType avatarType;

    public string GetString()
    {
        string speaker=" ";
        if (avatarType)
        {
            speaker = avatarType.Name;
        }

        return speaker + "\n" + Dialog;
    }


}