using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackType
{
    NoType = 0,
    Educative = 1,
    Emotive = 2,
    Persuasive = 3
}

public enum TargetType { Single, Multi };
[CreateAssetMenu]
public class Skill : ScriptableObject
{
    public float Strength;
    public float StrengthMultiplier = 1;
    public float Cost;
    public string SkillName;
    public string AnimationName;
    public TargetType SkillType;
    public AttackType AttackType;
    public bool isSupportive = false;
    public int PositionRequirement; // 0 for front position, 1 for back position
    public AudioClip SkillSFX;
    public string Description;
    public string Dialogue;

    //For Single Target Skills
    public virtual void Activate(float Attack, Unit Target)
    {
        Debug.Log("Single Target Activating");
        float damage = (Attack + Strength) * StrengthMultiplier;
        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        damage = Mathf.Ceil(damage);
        Target.TakeDamage(damage,AttackType);
        BattleUIManager.instance.EventOccured("Attacked " + Target.name + " for " + damage + " damage");
    }

    //For MultiTarget skills
    public virtual void Activate(float Attack, List<Unit> TargetList)
    {
        Debug.Log("Multi Target Activating");
        float damage = (Attack + Strength) * StrengthMultiplier;
        damage = Mathf.Ceil(damage);
        BattleUIManager.instance.EventOccured(BattleManager.instance.CurrentUnit.name + " used " + SkillName + "!");
        foreach (Unit t in TargetList)
        {
            t.TakeDamage(damage,AttackType);
            BattleUIManager.instance.EventOccured("Attacked " + t.name + " for " + damage + " damage");
        }

        VolumeManager.instance.PlaySFX(SkillSFX);

    }
}
