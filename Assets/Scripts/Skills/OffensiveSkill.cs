using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skill/OffensiveSkill")]
public class OffensiveSkill : Skill
{
    //For Single Target Skills
    public override void Activate(float Attack, Unit Target)
    {
        Debug.Log("Single Target Activating");
        BattleUIManager.instance.EventOccured(BattleManager.instance.CurrentUnit.name + " used "+ SkillName +" on " + Target.name);
        float damage = (Attack + Strength) * StrengthMultiplier;
        damage = Mathf.Ceil(damage);
        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        Target.TakeDamage(damage, AttackType);
        VolumeManager.instance.PlaySFX(SkillSFX);

    }

    public override void Activate(float Attack, List<Unit> TargetList)
    {
        Debug.Log("Multi Target Activating");
        float damage = (Attack + Strength) * StrengthMultiplier;
        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        damage = Mathf.Ceil(damage);
        BattleUIManager.instance.EventOccured(BattleManager.instance.CurrentUnit.name + " used " + SkillName + "!");
        foreach (Unit t in TargetList)
        {
            t.TakeDamage(damage, AttackType);
            BattleUIManager.instance.EventOccured("Attacked " + t.name + " for " + damage + " damage");
        }

        VolumeManager.instance.PlaySFX(SkillSFX);

    }
}
