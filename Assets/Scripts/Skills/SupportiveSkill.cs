using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skill/SupportiveSkill")]
public class SupportiveSkill :Skill
{
    public Buff BuffType;


    public override void Activate(float Attack, Unit Target)
    {

        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        Buff buffToApply = Instantiate(BuffType);
        Target.ApplyBuff(buffToApply);
        BattleUIManager.instance.EventOccured(BattleManager.instance.CurrentUnit.name + " used " + SkillName + " on " + Target.name);
        BattleUIManager.instance.EventOccured(Target.name + " recieved " + buffToApply.name);
        VolumeManager.instance.PlaySFX(SkillSFX);
    }

    public override void Activate(float Attack, List<Unit> TargetList)
    {
        Debug.Log("Multi Target Activating");
        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        BattleUIManager.instance.EventOccured(BattleManager.instance.CurrentUnit.name + " used " + SkillName + "!");
        foreach (Unit t in TargetList)
        {
            Buff buffToApply = Instantiate(BuffType);
            t.ApplyBuff(buffToApply);
            BattleUIManager.instance.EventOccured(t.name + " recieved " + buffToApply.name);
        }

        VolumeManager.instance.PlaySFX(SkillSFX);

    }
}
