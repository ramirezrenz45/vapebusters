using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Skill/HealingSkill")]
public class HealingSkill : SupportiveSkill
{
    public override void Activate(float Attack, Unit Target)
    {
        Debug.Log("Single Target Activating");
        float heal = (Attack + Strength) * StrengthMultiplier;
        BattleManager.instance.CurrentUnit.ConsumeSP(Cost);
        Target.Heal(heal);
        BattleUIManager.instance.EventOccured("Restored " + Target.name + "'s Conviction by " + heal + " points");
        VolumeManager.instance.PlaySFX(SkillSFX);
    }
}
