using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    DialogSystem dialog;
       [SerializeField] DialogSet dialogSet;
    // Start is called before the first frame update
    void Start()
    {
       dialog = FindObjectOfType<DialogSystem>();
    }


    // Update is called once per frame
    void Update()
    {
        
    }

   public void StartDialog(Controller controller)
    {

        if (dialog)
        {
            if (dialogSet)
            {
                dialog.EvtDialogStart?.Invoke(dialogSet);

                dialog.EvtDialogEnd.AddListener(onEnd);
            }
        }
    }
    public void onEnd()
    {
        dialog.EvtDialogEnd.RemoveListener(onEnd);
        Destroy(this.gameObject);

    }




}
