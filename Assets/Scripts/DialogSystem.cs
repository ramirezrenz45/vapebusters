using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using UnityEngine;



public class DialogSystem : MonoBehaviour
{
   [SerializeField] GameObject DiaologBox;
    [SerializeField] TextMeshProUGUI dialogText;
    public TextMeshProUGUI DialogSpeaker;
    [SerializeField] Image Image;
   public UnityEvent<DialogSet> EvtDialogStart;
    public UnityEvent EvtDialogEnd= new UnityEvent();
    bool DialogMode= false;
    bool OnPressed = false;
    public DialogSet itemDialog;
    public UnityEvent TriggerDialog;

    public DialogSet StartingDialog;
    public DialogSet EndDialog;

    [SerializeField] int currIndex=0;

    
    public void StartDialog(DialogSet dialog)
    {
        Debug.Log("Dialogue Started");
        UIManager uIManager = FindObjectOfType<UIManager>();
        uIManager.CloseAllPanel();
        currIndex = 0;
        if (dialog.dialogs.Count > 0)
        {
            GlobalEvents.Pause?.Invoke();
            DiaologBox.SetActive(true);

            DialogSpeaker.text = dialog.dialogs[currIndex].GetSpeaker();
            dialogText.text = dialog.dialogs[currIndex].GetString();
            if (dialog.dialogs[currIndex].GetAvatar())
            {
                Image.enabled = true;
                Image.sprite = dialog.dialogs[currIndex].GetAvatar();
            }
            else
            {
                Image.enabled = false;
                Image.sprite = null;

            }
            DialogMode = true;
            StartCoroutine(dalayDialog(dialog));
        }
    }
  public  IEnumerator dalayDialog(DialogSet dialog)
    {

    
        while (DialogMode)
        {
            yield return new WaitForSeconds(0.001f);

            if (OnPressed)
            {
                OnPressed = false;
                if (dialog.dialogs.Count > currIndex)
                {
                    DialogSpeaker.text = dialog.dialogs[currIndex].GetSpeaker();
                    dialogText.text = dialog.dialogs[currIndex].GetString();
                    if (dialog.dialogs[currIndex].GetAvatar())
                    {
                        Image.enabled = true;
                        Image.sprite = dialog.dialogs[currIndex].GetAvatar();
                    }
                    else
                    {
                        Image.enabled = false;
                        Image.sprite = null;

                    }
                    currIndex++;
                 

                }
                else
                {
                    DialogMode = false;
                    OnPressed = false;
                    dialogText.text = "";
                    DiaologBox.SetActive(false);
                    EvtDialogEnd.Invoke();
                    GlobalEvents.UnPause?.Invoke();
                }
            }
        }
    }

    public void Notification(string notifText)
    {
        Debug.Log("Notification");
        UIManager uIManager = FindObjectOfType<UIManager>();
        uIManager.CloseAllPanel();
        GlobalEvents.Pause?.Invoke();
        currIndex = 0;
        DiaologBox.SetActive(true);
        itemDialog.dialogs[0].Dialog = notifText;
        dialogText.text = itemDialog.dialogs[currIndex].GetString();
        DialogMode = true;
        StartDialog(itemDialog);
        
    }




    void OnEndDialog()
    {
        EvtDialogEnd.Invoke();

       

    }

    private void Awake()
    {
        EvtDialogStart.AddListener(StartDialog);
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!(FindObjectOfType<BattleInfo>().LevelStarted))
        {
           // EvtDialogStart.Invoke(StartingDialog);
            FindObjectOfType<BattleInfo>().LevelStarted = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (DialogMode)
        {
            if (Input.GetMouseButtonDown(0)||Input.GetKeyDown(KeyCode.Space))
            {
                OnPressed = true;
            }
        }
    }

}



