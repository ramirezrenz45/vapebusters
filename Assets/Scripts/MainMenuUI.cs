using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MainMenuUI : MonoBehaviour
{

    [SerializeField] GameObject OptionsPanel;
    public Slider SFX;
    public Slider BGM;
    [SerializeField] GameObject Loadbutton;
    [SerializeField] GameObject confirmationPanel;
    public AudioClip MainMenuTheme;
    public AudioClip CheckSFX;

    // Update is called once per frame
    private void Start()
    {
        BattleInfo bI = GameObject.FindObjectOfType<BattleInfo>();
        Destroy(bI);

        //Loadbutton.GetComponent<Button>().interactable = SaveLoad.SaveExists("SavedData");
        if (Loadbutton!= null) Loadbutton.SetActive(SaveLoad.SaveExists("SavedData"));

        if(SFX !=null) SFX.value = VolumeManager.instance.GetSFXVolume();
        if(BGM !=null) BGM.value = VolumeManager.instance.GetBGMVolume();
        VolumeManager.instance.PlayBGM(MainMenuTheme);
        BGM.onValueChanged.AddListener(delegate { ChangeBGMVolume(); });
        SFX.onValueChanged.AddListener(delegate { ChangeSFXVolume(); });
    }

    void AddListeners()
    {

    }


    void Update()
    {
        
    }


    public void ChangeSFXVolume()
    {
        VolumeManager.instance.SFXVolumeChange(SFX.value);
        VolumeManager.instance.CheckingSFX(CheckSFX);
    }

    public void ChangeBGMVolume()
    {
        VolumeManager.instance.BGMVolumeChange(BGM.value);
    }



    public void OnNewGame()
    {
        if (!SaveLoad.SaveExists("SavedData"))
        {
            SaveLoad.CreateSaveData();
            FindObjectOfType<SceneControl>().Gotolevel("StartCutScene");

            BattleInfo battleInfo= FindObjectOfType<BattleInfo>();
 SaveLoad.DeleteAllSaveData();
            SaveLoad.Save<HashSet<string>>(new HashSet<string>(), "CollectedItems");
            if (battleInfo)
            {
               
                battleInfo.clearVariables();
                battleInfo.clearData();
            }
        }
        else
        {
            confirmationPanel.SetActive(true);

        }
    }


    public void ConfirmationButtonstriggerd(bool answer)
    {
        confirmationPanel.SetActive(false);
        if (answer)
        {
            SaveLoad.DeleteAllSaveData();
            FindObjectOfType<SceneControl>().Gotolevel("StartCutScene");
        }
        else
        {
           
        }
    }




    public void OpenOptionsPanel()
    {
        OptionsPanel.SetActive(true);
    }

    public void CloseOptionsPanel()
    {
        OptionsPanel.SetActive(false);
    }



}
