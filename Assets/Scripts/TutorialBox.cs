using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialBox : MonoBehaviour
{
    public GameObject TutorialToStart;


    void Start()
    {
        //if (SaveLoad.SaveExists("SavedData"))
        //{
        //   
        //}
        if (CollectibleSet.Instance.CollectedItems.Contains(TutorialToStart.name))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("StartingTutorial");
            CollectibleSet.Instance.AddItem(TutorialToStart.name);
            Instantiate(TutorialToStart);
            Destroy(this.gameObject);
        }
    }
}
