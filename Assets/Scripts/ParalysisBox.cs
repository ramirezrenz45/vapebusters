using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalysisBox : MonoBehaviour
{
    TriggerBox triggerBox;
    
    // Start is called before the first frame update
    void Start()
    {
        triggerBox = GetComponent<TriggerBox>();
        triggerBox.onHitEvt.AddListener(ParalyzePlayer);
    }
    public void ParalyzePlayer(Controller Playercontroller)
    {
        Playercontroller.paralysis(transform.position);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
