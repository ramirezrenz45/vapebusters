using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PlayerStatsTest : MonoBehaviour
{
    public PlayerStats basePlayerStats;
    public PlayerStats currentPlayerStats;
    public int PlayerID;


    void Start()
    {

    }

    public void EquipmentManagerCheck()
    {
        if (EquipmentManager.instance)
        {
            Debug.Log("EquipmentManagerFound");
            EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
        }
    }
    public void LeavingWorldScene()
    {
        EquipmentManager.instance.onEquipmentChanged -= OnEquipmentChanged;
    }

    public void MakeNewStat()
    {
        currentPlayerStats = Object.Instantiate(basePlayerStats);
    }

    public UnitStats newStat()
    {
        currentPlayerStats = Object.Instantiate(basePlayerStats);
        currentPlayerStats.Start();
        return currentPlayerStats;
    }

    public void AttachToManager()
    {
        EquipmentManager.instance.onEquipmentChanged += OnEquipmentChanged;
    }

    public void DettachFromManager()
    {
        EquipmentManager.instance.onEquipmentChanged -= OnEquipmentChanged;
    }


    void OnEquipmentChanged(Equipment newItem, Equipment oldItem, int playerID)
    {
        Debug.Log("Equipment Is Changed Updating Stats");
        if (playerID == PlayerID)
        {
            Debug.Log("ID check complete");
            if (newItem != null)
            {
                currentPlayerStats.Attack.AddModifier(newItem.AttackMod);
                currentPlayerStats.Defense.AddModifier(newItem.DefenseMod);
                currentPlayerStats.Speed.AddModifier(newItem.SpeedMod);
                Debug.Log("Modifiers added");
            }
            if (oldItem != null)
            {
                currentPlayerStats.Attack.RemoveModifier(oldItem.AttackMod);
                currentPlayerStats.Defense.RemoveModifier(oldItem.DefenseMod);
                currentPlayerStats.Speed.RemoveModifier(oldItem.SpeedMod);
            }
        }
    }




}
