using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SavePoint : MonoBehaviour
{

 
   public CollectibleSet collectible;
    Controller player;
    GameObject confirmationPanel;
    GlobalEvents globalEvents;
    UIManager uIManager;
    private void Awake()
    {
        globalEvents = FindObjectOfType<GlobalEvents>();
    }

    private void Start()
    {
        BattleInfo battleInfo = FindObjectOfType<BattleInfo>();
        if (!battleInfo.EscapeLocations.Contains(new Vector3(transform.position.x, 2, transform.position.z)))
        {
            battleInfo.EscapeLocations.Add(new Vector3(transform.position.x, 2, transform.position.z));
        }
        uIManager = FindObjectOfType<UIManager>();
        confirmationPanel = uIManager.confirmationPanel;
    }

    // Start is called before the first frame update
    public void OnSave()
    {
        confirmationPanel.SetActive(true);
        globalEvents.OnPause();
        ConfirmationPanel  confirm= confirmationPanel.GetComponent<ConfirmationPanel>();
        confirm.EvtConfirm.AddListener(Save);
        confirm.EvtConfirm.AddListener(uIManager.CloseAllPanel);
        confirm.EvtCancel.AddListener(uIManager.CloseAllPanel);
        confirm.EvtCancel.AddListener(globalEvents.OnUnpaused);
        confirm.EvtConfirm.AddListener(globalEvents.OnUnpaused);
        confirm.text.text = "Are you sure you want to overwrite data?";
    }
    public void Save()
    {
        confirmationPanel.SetActive(false);
       
        confirmationPanel.GetComponent<ConfirmationPanel>().text.text = "";
        collectible = FindObjectOfType<CollectibleSet>();
        player = FindObjectOfType<Controller>();
        Player.Instance.Save();

        Inventory inventory = Player.Instance.inventory;
        FindObjectOfType<SaveData>().Saving(inventory, collectible.CollectedItems, Player.Instance.playerData, player.gameObject.transform.position);
        Debug.Log("Game Saved");

    }

}
