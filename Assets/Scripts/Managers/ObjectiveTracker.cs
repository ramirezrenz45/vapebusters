using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


[System.Serializable]
public class Objective
{
    public string ObjectiveDescription;
    public ItemType KeyItem;

}

public class ObjectiveTracker : MonoBehaviour
{
    public List<Objective> Objectives;
    public ItemType currentKeyItem;
    public EnemyContainers levelBoss;
    public AudioClip CompleteSFX;
    ObjectivePanel panel;
    public int objectiveIndex = 0;


    public fact_board factScripts= null;
    public UnityEvent CheckHasNotificationEvent = new UnityEvent();
    bool HasNotification=false;


#region Singleton
    public static ObjectiveTracker instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }
   
 #endregion 
    public bool getNotification()
    {
        HasNotification = false;
        if (factScripts)
        {
            HasNotification = (!(factScripts.CheckUncheckedFacts().Count == 0));

        }
        return HasNotification;
    }
    // Start is called before the first frame update
    void Start()
    {
       // factScripts = null;
        Invoke("GetUI",0.1f);
        levelBoss = GameObject.FindWithTag("LevelBoss").GetComponent<EnemyContainers>();
        if (levelBoss)
        {
            levelBoss.EvtDied.AddListener(BossDefeated);
        }

      //  getNotification();
    }

    public void BossDefeated(GameObject defeatedBoss)
    {
        levelBoss.EvtDied.RemoveListener(BossDefeated);
        ObjectiveComplete();
    }


    public void GetUI()
    {
        if (GameObject.Find("ObjectivePanel") != null)
        {
            panel = GameObject.Find("ObjectivePanel").GetComponent<ObjectivePanel>();
            if (objectiveIndex >= Objectives.Count) objectiveIndex = Objectives.Count - 1;

            if (objectiveIndex == 2 || objectiveIndex == 7)
            {
                panel.ObjectiveText.text = "Current Objective: " + Objectives[objectiveIndex-1].ObjectiveDescription;
                currentKeyItem = Objectives[objectiveIndex].KeyItem;
                panel.SetSecondaryObjective(Objectives[objectiveIndex].ObjectiveDescription);
            }
            else
            {
                panel.ObjectiveText.text = "Current Objective: " + Objectives[objectiveIndex].ObjectiveDescription;
            }
        }
        if (factScripts)
        {
            HasNotification = (!(factScripts.CheckUncheckedFacts().Count == 0));
            CheckHasNotificationEvent?.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.M))
        //{
        //    ObjectiveComplete();
        //}
        //if (Input.GetKeyDown(KeyCode.N))
        //{
        //    KeyItemFound();
        //}
    }

    public void KeyItemFound()
    {
        VolumeManager.instance.PlaySFX(CompleteSFX);
        Debug.Log("KeyItemFound");
        panel.CompleteSecondaryObjective();
        Debug.Log("RaisingIndex");
        currentKeyItem = null;
        objectiveIndex++;
        panel.ObjectiveText.text = "Current Objective: " + Objectives[objectiveIndex].ObjectiveDescription;
    }

    public void ObjectiveComplete()
    {
        Debug.Log("ObjectiveComplete");
        Debug.Log("Raising Index");
        VolumeManager.instance.PlaySFX(CompleteSFX);
        objectiveIndex++;
        if (factScripts)
        {
            HasNotification = (!(factScripts.CheckUncheckedFacts().Count == 0));
            CheckHasNotificationEvent?.Invoke();
        }
       
       ChangeObjective();
    }
    public void notificationCaller()
    {
        if (factScripts)
        {
            HasNotification = (!(factScripts.CheckUncheckedFacts().Count == 0));
            CheckHasNotificationEvent?.Invoke();
        }
    }
    void ChangeObjective()
    {
        if (objectiveIndex < Objectives.Count)
        {
            if (Objectives[objectiveIndex].KeyItem != null)
            {
                Debug.Log("ObjectiveHasItem");
                currentKeyItem = Objectives[objectiveIndex].KeyItem;
                panel.SetObjectiveText(Objectives[objectiveIndex - 1].ObjectiveDescription);
                panel.SetSecondaryObjective(Objectives[objectiveIndex].ObjectiveDescription);
            }
            else
            {
                Debug.Log("NoKeyItem");
                Debug.Log(panel == null);
                panel.SetObjectiveText(Objectives[objectiveIndex].ObjectiveDescription);
            }
        }
        else
        {
            panel.SetObjectiveText("None");
        }

    }

    public int GetObjectiveIndex()
    {
        return objectiveIndex;
    }

    public void SetObjectiveIndex(int index)
    {
        objectiveIndex = index;
        if(objectiveIndex == 2 || objectiveIndex == 7)
        {
            Debug.Log("ObjectiveWithKey");
            panel.SetObjectiveText(Objectives[objectiveIndex - 1].ObjectiveDescription);
            panel.SetSecondaryObjective(Objectives[objectiveIndex].ObjectiveDescription);
        }
        else
        {
            ChangeObjective();
        }
        //Invoke("ChangeObjective",0.1f);
    }

    public void LoadData(int index)
    {
        objectiveIndex = index;
        panel.ObjectiveText.text = "Current Objective: " + Objectives[objectiveIndex].ObjectiveDescription;
    }

    public void CheckItem(ItemType item)
    {
        Debug.Log("Checking Item");
        if (currentKeyItem == item)
        {
            Debug.Log("Correct Item");
            KeyItemFound();
        }
    }

    public int GetObjectiveMarker()
    {
        return objectiveIndex;
    }
}
