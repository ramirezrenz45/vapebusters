using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResourceBarManager : MonoBehaviour
{
    public Image HPBar;
    public Image SPBar;
    public TextMeshProUGUI HPtext;
    public TextMeshProUGUI SPtext;
    float maxHp;
    float maxSp;

    public void SetupBars(float HP, float SP)
    {
        maxHp = HP;
        maxSp = SP;
        HPtext.text = maxHp + "/" + maxHp;
        SPtext.text = maxSp + "/" + maxSp; 
        UpdateBars(maxHp, maxSp);
    }

    // Update is called once per frame
    public void UpdateBars(float HP, float SP)
    {
        HPBar.fillAmount = HP / maxHp;
        SPBar.fillAmount = SP / maxSp;
        HPtext.text = HP + "/" + maxHp;
        SPtext.text = SP + "/" + maxSp;
    }

    public void UpdateHealth(float HP)
    {
        HPBar.fillAmount = HP / maxHp;
        HPtext.text = HP + "/" + maxHp;
    }
}
