using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Environment { Hallway, Classroom, Gym, ComputerLab}

public class EnvironmentManager : MonoBehaviour
{
    public GameObject HallwayEnvironment;
    public GameObject ClassroomEnvironment;
    public GameObject GymEnvironment;
    public GameObject ComputerLabEnvironment;
    #region Singleton

    public static EnvironmentManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    #endregion

    public void Start()
    {
        ChangeEnvironment(GameObject.FindWithTag("GameManager").GetComponent<BattleInfo>().BattleEnv);

    }

    public void ChangeEnvironment(Environment env)
    {
        switch (env)
        {
            case Environment.Hallway:
                {
                    HallwayEnvironment.SetActive(true);
                    ClassroomEnvironment.SetActive(false);
                    GymEnvironment.SetActive(false);
                    ComputerLabEnvironment.SetActive(false);
                    break;
                }
            case Environment.Classroom:
                {
                    HallwayEnvironment.SetActive(false);
                    ClassroomEnvironment.SetActive(true);
                    GymEnvironment.SetActive(false);
                    ComputerLabEnvironment.SetActive(false);
                    break;
                }
            case Environment.Gym:
                {
                    HallwayEnvironment.SetActive(false);
                    ClassroomEnvironment.SetActive(false);
                    GymEnvironment.SetActive(true);
                    ComputerLabEnvironment.SetActive(false);
                    break;
                }
            case Environment.ComputerLab:
                {
                    HallwayEnvironment.SetActive(false);
                    ClassroomEnvironment.SetActive(false);
                    GymEnvironment.SetActive(false);
                    ComputerLabEnvironment.SetActive(true);
                    break;
                }
        }
    }
}
