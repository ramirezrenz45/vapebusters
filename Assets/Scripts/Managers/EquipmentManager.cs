using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class EquipmentManager : MonoBehaviour
{
    public delegate void OnEquipmentChanged(Equipment newItem, Equipment oldItem,int Playerid);
    public OnEquipmentChanged onEquipmentChanged;
    public Equipment[] p1Equipment;
    public Equipment[] p2Equipment;
    public Equipment[] currentEquipment;
    public UnityEvent<int> OnStatChange = new UnityEvent<int>();
    public List<Sprite> EquipmentIcons;
    public int currentCharacterID = 1;

    [Header("Audio")]
    public AudioClip EquipSFX;
    public AudioClip UnequipSFX;

    [Header("Sprites")]
    public Sprite HeadSprite;
    public Sprite BodySprite;
    public Sprite LegsSprite;
    public Sprite weaponSprite;



    public List<Equipment> EquipmentInventory;


    // UI Stuff move to UI Manager Later
    public EquipmentPanel EquipmentPanel;
    public GameObject EquipmentInventoryPanel;
    public Text CharacterNameText;
    public List<Text> CurrentEquipmentText;
    public List<Image> CurrentEquipmentSprite;
    public GameObject EquipmentText;

    #region Singleton

    public static EquipmentManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        int numSlots = System.Enum.GetNames(typeof(EquipSlot)).Length;
        p1Equipment = new Equipment[numSlots];
        p2Equipment = new Equipment[numSlots];
    }
    #endregion

    public void getEquipment(int [] equipmentIDA,int [] equipmentIDB)
    {
        Debug.Log("Getting Equipment");
        for (int x = 0; x < equipmentIDA.Length; x++)
        {
            Debug.Log("Equipment Found");
            Equipment a = (Equipment)ItemDatabase.Instance.GetFromID(equipmentIDA[x]);
            if (a) {
                Debug.Log(a.Name);
            p1Equipment[x] = a;
            }

        }
        for (int x = 0; x < equipmentIDB.Length; x++)
        {
            Equipment a = (Equipment)ItemDatabase.Instance.GetFromID(equipmentIDB[x]);
            if (a)
                p2Equipment[x] = a;
       

        }
      
        UpdateEquipmentList();
    }


    void Start()
    {
        currentEquipment = p1Equipment;
      
        if (SaveLoad.SaveExists("Equipments"))
        {
            Debug.Log("EquipmentFound");
           Player.Instance.inventory.equipmentIDs = new List<StoredItemData>(SaveLoad.Load<List<StoredItemData>>("Equipments"));

            Player.Instance.EvtInventoryChange.Invoke(Player.Instance.inventory);
        }
        Player.Instance.EvtInventoryChange.AddListener(OnChange);
        StartCoroutine(delay());

        UpdateEquipmentList();

    }

    

    IEnumerator delay()
    {

        yield return new WaitForSeconds(0.1f);
        //Debug.Log("PlayerData is null: " + (Player.Instance.playerData == null));
        //Debug.Log("PartyData is null: " + (Player.Instance.playerData.partyDatas[0] == null));
        //Debug.Log("PlayerID1 is null: " + (Player.Instance.playerData.partyDatas[0].EquipmentID == null) + "PlayerID2 is null: " + (Player.Instance.playerData.partyDatas[1].EquipmentID == null));
        getEquipment(Player.Instance.playerData.partyDatas[0].EquipmentID, Player.Instance.playerData.partyDatas[1].EquipmentID);

    }

    public Sprite GetSprite(int x)
    {
        Sprite _sprite;
        switch (x)

        {
            case 0:
                _sprite = HeadSprite;
                break;

            case 1:
                _sprite = BodySprite;
                break;
            case 2:
                _sprite = LegsSprite;
                break;
            case 3:
                _sprite = weaponSprite;
                break;
            default:
                _sprite = null;
                break;
        }
        return _sprite;
    }
    public void OnChange(Inventory inventory)
    {
        EquipmentInventory = new List<Equipment>(inventory.GetEquipment());
      
        UpdateEquipmentList();
    }

    public void Equip(Equipment newItem)
    {
        VolumeManager.instance.PlaySFX(EquipSFX);
        int slotIndex = (int)newItem.Slot;

        Equipment oldItem = null;

        if(currentEquipment[slotIndex] != null)
        {
            oldItem = currentEquipment[slotIndex];
            EquipmentInventory.Add(oldItem);
            Player.Instance.GotItem(oldItem);
            Player.Instance.playerData.newEquipment = false;
            Player.Instance.EvtInventoryChange?.Invoke(Player.Instance.inventory);
        }
        if(onEquipmentChanged != null)
        {
            onEquipmentChanged.Invoke(newItem, oldItem,currentCharacterID);
        }

    //    if (EquipmentInventory.Contains(newItem))
    if(Player.Instance.inventory.Isavailable(newItem))
        {
            Debug.LogError("Equiping"+ newItem.Name);
            EquipmentInventory.Remove(newItem);
            Player.Instance.RemoveIteminInventory(newItem);
            Player.Instance.EvtInventoryChange?.Invoke(Player.Instance.inventory);
             UpdateEquipmentList();
        }
        currentEquipment[slotIndex] = newItem;
        Invoke("UpdateEquipmentList",0.1f);
        UpdateEquip();
        Player.Instance.EvtInventoryChange?.Invoke(Player.Instance.inventory);
        Player.Instance.Save();
        getEquipment(Player.Instance.playerData.partyDatas[0].EquipmentID, Player.Instance.playerData.partyDatas[1].EquipmentID);
        if (EquipmentPanel != null)
        {
            EquipmentPanel.UpdateStat();
        }

    }

    void UpdateEquip()
    {
        for (int x = 0; x < CurrentEquipmentText.Count; x++)
        {
            if (currentEquipment[x] != null)
            {
                CurrentEquipmentText[x].text = currentEquipment[x].Name;
            }
            else
            {
                CurrentEquipmentText[x].text = " (UNEQUIPPED) ";
            }
        }


         for (int x = 0; x < CurrentEquipmentSprite.Count; x++)
        {
            if (currentEquipment[x] != null)
            {
                CurrentEquipmentSprite[x].sprite = currentEquipment[x].ItemSprite;
                CurrentEquipmentSprite[x].enabled = true;
            }
            else
            {
                CurrentEquipmentSprite[x].sprite = null;
                CurrentEquipmentSprite[x].enabled = false;
            }
        }
        
    }

    public void ChangeSelection(int currentPlayer)
    {
        switch (currentPlayer) {
            case 1:
                {
                    currentEquipment = p1Equipment;
                    CharacterNameText.text = "Jerry";
                    UpdateEquip();
                    currentCharacterID = 1;
                    break;
                }
            case 2:
                {
                    currentEquipment = p2Equipment;
                    CharacterNameText.text = "Sam";
                    UpdateEquip();
                    currentCharacterID = 2;
                    break;
                }

        }
    }
   
    public void Unequip(int slotIndex)
    {
        VolumeManager.instance.PlaySFX(UnequipSFX);
        if (currentEquipment[slotIndex] != null)
        {

            Equipment oldItem = currentEquipment[slotIndex];
            currentEquipment[slotIndex] = null;
            EquipmentInventory.Add(oldItem);
            Player.Instance.GotItem(oldItem);
            if (onEquipmentChanged != null)
            {
                onEquipmentChanged.Invoke(null, oldItem,currentCharacterID);
            }
        }
        Player.Instance.EvtInventoryChange?.Invoke(Player.Instance.inventory);
        UpdateEquip();
        Player.Instance.Save();
    }

    public void UpdateEquipmentList()
    {
        Debug.Log("UpdatingEquipmentList");
        foreach (Transform t in EquipmentInventoryPanel.transform)
        {
            Debug.Log("Checking Transform");
            if (t.gameObject.GetComponent<EquipmentButton>() != null)
            {
                Debug.Log("DeletingButton");
                t.gameObject.GetComponent<EquipmentButton>().Reset();
            }
        }
        foreach(Equipment e in EquipmentInventory)
        {
            GameObject g = Instantiate (EquipmentText) as GameObject;
            g.transform.SetParent(EquipmentInventoryPanel.transform);
            g.GetComponent<Button>().onClick.AddListener(()=> Equip(e));
            g.GetComponent<EquipmentButton>().ChangeEquipmentText(e.Name);
            g.GetComponent<EquipmentButton>().equipment = e;
            g.transform.Find("Image").GetComponent<Image>().sprite = e.ItemSprite;//GetSprite((int)e.Slot);
        }
    }

    public void OnHover(string Description)
    {
        if(EquipmentPanel) EquipmentPanel.TurnOnEquipmentDescription(Description);
    }

    public void OnExit()
    {

        if (EquipmentPanel) EquipmentPanel.TurnOffEquipmentDescription();
    }

    public void GetUI(EquipmentPanel equipmentPanel, GameObject equipmentInventoryPanel, List<Text> currentEquipmentText,List<Image> _currentEquipmentSprite, Text characterNameText)
    {
        EquipmentPanel = equipmentPanel;
        EquipmentInventoryPanel = equipmentInventoryPanel;
        CurrentEquipmentText = currentEquipmentText;
        CharacterNameText = characterNameText;
        CurrentEquipmentSprite = _currentEquipmentSprite;
    }
}
