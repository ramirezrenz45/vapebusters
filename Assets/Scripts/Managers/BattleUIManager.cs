using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class BattleUIManager : MonoBehaviour
{
    public static BattleUIManager instance;

    [Header("References")]
    public BattleManager BattleManager;
    public GameObject EscapeButton;
    public GameObject GameOverScreen;
    public Button ContinueButton;
    public Text GameOverText;
    public TextMeshProUGUI ExpText;
    public TextMeshProUGUI ItemText;
    public TextMeshProUGUI MoneyText;
    public TextMeshProUGUI BillyLevelText;
    public TextMeshProUGUI MandyLevelText;
    public GameObject TargetListWindow;
    public GameObject SkillListWindow;
    public GameObject ItemWindow;
    public GameObject BattleMenu;
    public GameObject EventWindow;
    public GameObject DescriptionWindow;
    public GameObject ExitConfirmationWindow;
    public Text DescriptionText;
    public Text EventText;
    public Text NameText;
    public GameObject SelectionCursor;
    public GameObject WinButton;
    public GameObject LoseButtons;
    public GameObject TurnPanel;//adafsdaf
    public BattleDialog BattleDialogue;
    public KeyboardControls Controls;
    public TargetIndicator Indicator;
    public GameObject HintWindow;
    public Vector3 P1Position;
    public Vector3 P2Position;

    [Header("Audio")]
    public AudioClip SelectSFX;
    public AudioClip ConfirmSFX;
    public AudioClip LevelUpSFX;
    public AudioClip PlayerTurnSFX;


    [Header("BackendVariables")]
    public UnitCardHolder TurnOrderList;
    public List<Unit> TargetList;
    public List<GameObject> TargetButtons;
    public List<GameObject> SkillButtonList;
    public List<Button> MenuButtons;
    public int selectionIndex;
    private int recievedExp;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        if(TurnPanel)
        TurnPanel.SetActive(false);
    }

    void Start()
    {
        StartCoroutine(SwitchingWindow());
    }

    public void GetExp()
    {
        recievedExp = 0;
        foreach (Unit u in BattleManager.EnemyList)
        {
            recievedExp += u.Unit_Stats.Exp;
        }
        ExpText.text = recievedExp.ToString() + " EXP";
    }

    void ChangeSelection(int addToIndex)
    {
        selectionIndex += addToIndex;
        if(selectionIndex< 0)
        {
            selectionIndex = 0;
        }
        else if (selectionIndex > (MenuButtons.Count - 1))
        {
            selectionIndex = MenuButtons.Count - 1;
        }
        SelectionCursor.transform.SetParent(MenuButtons[selectionIndex].transform);
        SelectionCursor.transform.localPosition = new Vector3(0, 0, 0);
    }

    public void EventOccured(string newEventText)
    {
      EventText.text += newEventText+ "\n";
    }

    public void OpenEvent()
    {
        Controls.enabled = false;
        EventWindow.SetActive(true);
    }

    public void ConfirmTarget(int targetNumber)
    {
        PlayConfirm();
        Debug.Log("Button Pressed");
        if (targetNumber < TargetList.Count)
        {
            if(TargetList[targetNumber].outline) TargetList[targetNumber].outline.enabled = false;
            BattleManager.UseSkill(TargetList[targetNumber]);
        }
        else
        {
            Debug.Log("Wrong number assignment for Button");
        }
        BattleMenu.SetActive(false);
        ActivatePanel(null);
        Indicator.Off();
    }


    public void TargetListUpdate(string unitTag)
    {

        //Debug.Log(BattleManager.CurrentSkill.SkillName + BattleManager.CurrentSkill.isSupportive);
        switch (BattleManager.CurrentSkill.isSupportive)
        {
            case false:
                if (unitTag == "Player")
                {
                    TargetList = BattleManager.EnemyList;
                }
                else if (unitTag == "Enemy")
                {
                    TargetList = BattleManager.PlayerList;
                }
                else
                {
                    Debug.Log("Unit is misstagged");
                }
                break;
            case true:
                if (unitTag == "Player")
                {
                    TargetList = BattleManager.PlayerList;
                }
                else if (unitTag == "Enemy")
                {
                    TargetList = BattleManager.EnemyList;
                }
                else
                {
                    Debug.Log("Unit is misstagged");
                }
                break;
        }

        foreach (GameObject g in TargetButtons)
        {
            g.SetActive(false);
        }
        switch (BattleManager.CurrentUnit.Position)
        {
            case 0:
                {
                    bool vanguardReady = false;
                    foreach (Unit u in TargetList)
                    {
                        if(u.Position == 0)
                        {
                            vanguardReady = true;
                        }
                    }
                    for (int x = 0; x < TargetList.Count; x++)
                    {
                        if (vanguardReady)
                        {
                            if (TargetList[x].Position == 0)
                            {
                                TargetButtons[x].GetComponent<Text>().text = TargetList[x].name;
                                TargetButtons[x].SetActive(true);
                            }
                        }
                        else
                        {
                            TargetButtons[x].GetComponent<Text>().text = TargetList[x].name;
                            TargetButtons[x].SetActive(true);
                        }
                    }
                    break;
                }
            case 1:
                {
                    for (int x = 0; x < TargetList.Count; x++)
                    {
                        TargetButtons[x].GetComponent<Text>().text = TargetList[x].name;
                        TargetButtons[x].SetActive(true);
                    }
                    break;
                }
        }
    }

    public void OpenBattleMenu()
    {
        if(!Controls.enabled) Controls.enabled = true;
        VolumeManager.instance.PlaySFX(PlayerTurnSFX);
        BattleMenu.SetActive(true);
        Controls.SetActiveWindow(BattleMenu);
    }

    public void CloseBattleMenu()
    {
        BattleMenu.SetActive(false);
    }

    public void ChangeName(string unitName)
    {
        NameText.text = unitName;
    }

    public void OpenDescription(string text)
    {
        DescriptionWindow.SetActive(true);
        DescriptionText.text = text;
    }

    public void CloseDescription()
    {
        DescriptionWindow.SetActive(false);
        DescriptionText.text = "";
    }

    public void PlaySelect()
    {
        VolumeManager.instance.PlaySFX(SelectSFX);
    }

    public void PlayConfirm()
    {
        VolumeManager.instance.PlaySFX(ConfirmSFX);
    }

    public void PlayLevelUp()
    {
        VolumeManager.instance.PlaySFX(LevelUpSFX);
    }

    public void BattleEnd(string GOText,bool PlayerWon)
    {
        Controls.enabled = false;
        GameOverScreen.SetActive(true);
        Invoke("CloseEvent",0.1f);
        //GameOverText.text = GOText;
        if (PlayerWon)
        {
            LoseButtons.gameObject.SetActive(false);
            ExpText.gameObject.SetActive(true);
            ItemText.gameObject.SetActive(true);
            MoneyText.gameObject.SetActive(true);
            WinButton.gameObject.SetActive(true);
            MoneyText.text += " "+BattleManager.GoldDrop;
            //add to player wallet
            foreach (ItemType drop in BattleManager.GetDropList())
            {
                ItemText.text += "You recieved " + drop.name + ".\n";
                Player.Instance.GotItem(drop);
            }
            BattleInfo battleInfo = FindObjectOfType<BattleInfo>();
            battleInfo.UpdatePlayerStats();
            battleInfo.GiveReward(recievedExp, BattleManager.GoldDrop);
            BillyLevelText.text = battleInfo.PlayerList[0].Level.ToString();
            MandyLevelText.text = battleInfo.PlayerList[1].Level.ToString();
        }
        else
        {
            ExpText.gameObject.SetActive(false);
            ItemText.gameObject.SetActive(false);
            MoneyText.gameObject.SetActive(false);
            LoseButtons.gameObject.SetActive(true);
            WinButton.gameObject.SetActive(false);
        }
    }

    public void CloseEvent()
    {
        EventWindow.SetActive(false);
    }


    public void SkillClicked(int index)
    {
        if (index < BattleManager.SkillList.Count) 
        {
            PlayConfirm();
            BattleManager.CurrentSkill = BattleManager.SkillList[index];
            DescriptionWindow.SetActive(false);
            ActivatePanel(TargetListWindow);
            TargetListUpdate(BattleManager.CurrentUnit.tag);
        }
        else
        {
            Debug.Log("Wrong number assignment");
        }
    }

    public void SkillListUpdate(List<Skill> SkillList)
    {
        int x = 0;
        foreach (GameObject g in SkillButtonList)
        {
            g.SetActive(false);
        }
        foreach (Skill s in SkillList)
        {
            if (x < SkillButtonList.Count)
            {
                if (s.PositionRequirement == BattleManager.CurrentUnit.Position)
                {
                    SkillButtonList[x].GetComponent<SkillButton>().SetSkill(s);
                    SkillButtonList[x].GetComponent<Text>().text = s.SkillName;
                    SkillButtonList[x].SetActive(true);
                    SkillButtonList[x].GetComponent<SkillButton>().CheckCost();
                }
            }
            else
            {
                Debug.Log("Not Enough Buttons");
            }
            x++;
        }
    }


    public void ActivateItemPanel()
    {
        PlaySelect();
        ActivatePanel(ItemWindow);
    }

    public void ActivatePanel(GameObject panelToBeActivated)
    {
        MenuButtons.Clear();
        GameObject ActivePanel = panelToBeActivated;
        SkillListWindow.SetActive(SkillListWindow.Equals(panelToBeActivated));
        ItemWindow.SetActive(ItemWindow.Equals(panelToBeActivated));
        TargetListWindow.SetActive(TargetListWindow.Equals(panelToBeActivated));
        if (BattleMenu.Equals(panelToBeActivated))
        {
            BattleMenu.SetActive(true);
        }


        if (ActivePanel != null)
        {
            foreach (Transform b in ActivePanel.transform)
            {
                if (b.GetComponent<Button>() != null && b.gameObject.activeInHierarchy) MenuButtons.Add(b.GetComponent<Button>());
            }
        }
        Controls.SetActiveWindow(panelToBeActivated);
    }

    public void AttackButton()
    {
        PlaySelect();
        BattleManager.ReturnSkillToDefault();
        TargetListUpdate(BattleManager.CurrentUnit.tag);
        ActivatePanel(TargetListWindow);
    }

    public void SkillButton()
    {
        PlaySelect();
        SkillListUpdate(BattleManager.SkillList);
        ActivatePanel(SkillListWindow);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            OpenDialogue("testing");
        }
    }

    public void OpenWindow(GameObject WindowToOpen)
    {
        WindowToOpen.SetActive(true);
    }

    IEnumerator OpeningWindow()
    {
        yield return new WaitForSeconds(2.0f);
        Debug.Log("Window Opened");
    }

    IEnumerator ClosingWindow()
    {
        yield return new WaitForSeconds(2.0f);
        Debug.Log("Window Closed");
    }

    IEnumerator SwitchingWindow()
    {
        yield return StartCoroutine(ClosingWindow());
        StartCoroutine(OpeningWindow());
    }

    public void CloseWindow(GameObject WindowToClose)
    {
        WindowToClose.SetActive(false);
    }

    public void ExitBattle()
    {
        BattleManager.PlayWinSFX();
        FindObjectOfType<BattleInfo>().OnWin();
    }

    public void MainMenu()
    {
        PlayConfirm();
        FindObjectOfType<BattleInfo>().GoToMainMenu();
    }

    public void Reload()
    {
        PlayConfirm();
        FindObjectOfType<SaveData>().Loading();
    }

    public void OpenExitConfirmation(bool open)
    {
        PlaySelect();
        ExitConfirmationWindow.SetActive(open);
    }



    public void EscapeBattle()
    {
        BattleManager.PlayLoseSFX();
        FindObjectOfType<BattleInfo>().OnEscape();
    }

    public void Swap()
    {
        PlaySelect();
        BattleMenu.SetActive(false);
        ActivatePanel(null);
        if (BattleManager.instance.PlayerList.Count > 1)
        {
            BattleManager.instance.PlayerList[0].Movement.MoveTo(BattleManager.instance.PlayerList[1].Movement.GetPosition(), null);
            BattleManager.instance.PlayerList[1].Movement.MoveTo(BattleManager.instance.PlayerList[0].Movement.GetPosition(), null);
        }
        else if(BattleManager.PlayerList[0].transform.position != P1Position)
        {
            BattleManager.PlayerList[0].Movement.MoveTo(P1Position,null);
        }
        else
        {
            BattleManager.PlayerList[0].Movement.MoveTo(P2Position, null);
        }
    }

    public void UpdateTurnOrder()
    {
        StopAllCoroutines();
       
        StartCoroutine("TurnListAnimation");
    }

    public void FinishGame()
    {
        SceneManager.LoadScene("EndCutScene");
    }

    IEnumerator TurnListAnimation()
    {
        TurnOrderList.transform.localPosition = new Vector3(TurnOrderList.transform.localPosition.x, 0, TurnOrderList.transform.localPosition.z);
        if(TurnPanel)
        TurnPanel.SetActive(true);
        while (TurnOrderList.transform.localPosition.y < 200)
        {
            TurnOrderList.transform.position += (new Vector3(0, 100, 0) * 30f * Time.deltaTime);
            yield return new WaitForSeconds(0.01f);
        }
        TurnOrderList.transform.localPosition = new Vector3(TurnOrderList.transform.localPosition.x, 200, TurnOrderList.transform.localPosition.z);
        TurnOrderList.TurnOrderUpdate(BattleManager.TurnOrder);
        yield return new WaitForSeconds(0.05f);
        while (TurnOrderList.transform.localPosition.y > 0)
        {
            TurnOrderList.transform.localPosition -= new Vector3(0,100, 0) * 30f * Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
        TurnOrderList.transform.localPosition = new Vector3(TurnOrderList.transform.localPosition.x, 0 , TurnOrderList.transform.localPosition.z);

    }


    public void OpenDialogue(string dialogue)
    {
        Indicator.Close();
        Debug.Log("Current Unit Position : " + BattleManager.instance.CurrentUnit.transform.position);
        BattleDialogue.gameObject.SetActive(true);
        BattleDialogue.OpenDialog(dialogue, BattleManager.instance.CurrentUnit.transform.position);
    }

    public void MoveCursor(Transform targetObject)
    {
        Controls.MoveCursor(targetObject);
    }

    public void OpenHintWindow(bool Opening)
    {
        HintWindow.SetActive(Opening);
    }
}
