using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManager : MonoBehaviour
{
    public Animator animator;
    private Action onAnimationComplete;




    public void StartAttackAnimation(string animationName, Action OnAnimationComplete)
    {
        Debug.Log("Playing Animation");
        animator.Play(animationName, 0, 0.0f);
        onAnimationComplete = OnAnimationComplete;
    }

    public void PlayAnimation(string animationName)
    {
        animator.Play(animationName, 0, 0.0f);
    }

    public void AnimationComplete()
    {
        Debug.Log("Animation Complete");
        onAnimationComplete();
    }

    public void VapeVFX()
    {
        GameObject fx = Instantiate(Resources.Load("VapeVFX")) as GameObject;

        fx.transform.SetParent(gameObject.transform.Find("Joint_Group/hips/spine_1/spine_2/spine_3/head").transform);
        fx.transform.localPosition = new Vector3 (0,-0.05f,0);
        fx.transform.SetParent(null);
        fx.transform.Rotate(0.0f, -90f, 0f);
        Destroy(fx, 1.0f);
    }

    public void HitEffect()
    {
        GameObject fx = Instantiate(Resources.Load("HitEffect")) as GameObject;

        fx.transform.SetParent(this.gameObject.transform);
        fx.transform.localPosition = new Vector3(0, 1, 0.4f);
        fx.transform.localEulerAngles = new Vector3(0, 90, 45);

        Destroy(fx, 0.7f);
    }
}
