using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    public GameObject WorldTutorialCanvas;
    public GameObject BattleTutorialCanvas;


    void Start()
    {
        SceneManager.sceneLoaded += SceneChange;
    }


    public void SceneChange(Scene scene, LoadSceneMode mode)
    {
        
        if (scene.name == "BattleScene")
        {
           Debug.Log("Instantiating Tutorial");
           Instantiate(BattleTutorialCanvas);
        }
        else
        {
            FindObjectOfType<DialogSystem>().EvtDialogEnd.AddListener(BeginningDialogFinished);
        }
        SceneManager.sceneLoaded -= SceneChange;
    }

    public void Destroy()
    {
        SceneManager.sceneLoaded -= SceneChange;
    }

    public void BeginningDialogFinished()
    {
        Instantiate(WorldTutorialCanvas);
        FindObjectOfType<DialogSystem>().EvtDialogEnd.RemoveListener(BeginningDialogFinished);
        Destroy(this.gameObject);
    }



}
