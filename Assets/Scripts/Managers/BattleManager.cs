using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : MonoBehaviour
{
    public static BattleManager instance;

    public BattleUIManager UIManager;
    public List<Unit> p1UnitList;
    public List<Unit> p2UnitList;
    public List<Unit> EnemyList;
    public List<Unit> PlayerList;
    public List<Unit> TurnOrder;
    public List<Skill> SkillList;
    public Skill CurrentSkill;
    public Skill PlayerDefaultSkill;
    public Skill EnemyDefaultSkill;
    public Unit CurrentUnit;
    public Buff StunDebuff;
    public List<UnitStats> nemyList;


    [Header("Audio")]
    public AudioClip Battle_WinSFX;
    public AudioClip Battle_LoseSFX;


    public List<Unit> unitList;
    List<ItemType> dropList;
    public int GoldDrop;
    private bool battleOngoing = false;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void EventBoxClosed()
    {
        UIManager.EventText.text = "";
        if (!battleOngoing)
        {
            DetermineTurnOrder();
            UIManager.GetExp();
            battleOngoing = true;
            UIManager.UpdateTurnOrder();
        }
        else
        {
            NextTurn();
        }
    }

    public void PlayWinSFX()
    {
        VolumeManager.instance.PlaySFX(Battle_WinSFX);
    }

    public void PlayLoseSFX()
    {
        VolumeManager.instance.PlaySFX(Battle_LoseSFX);
    }

    public void SetDropList()
    {
        GoldDrop = 0;
        dropList = new List<ItemType>();
        foreach (Enemy e in EnemyList)
        {
            if(e.Drop != null)
            {
                dropList.Add(e.Drop);
            }
            GoldDrop += e.GoldDrop;
        }
    }

    public List<ItemType> GetDropList()
    {
        return dropList;
    }

    void Start()
    {
        TurnOrder = new List<Unit>();
        GameObject.FindWithTag("GameManager").GetComponent<BattleInfo>().StartBattle();
    }

    public void BattleSetup(List<UnitStats> playerData, List<UnitStats> enemyData)
    {
        Debug.Log("Calling BattleSetup");
        Debug.Log("enemyData is null = " + (enemyData == null));
        nemyList = enemyData;
        int i1 = 0;
        int i2 = 0;
        PlayerList.Clear();
        EnemyList.Clear();
        //playerData.Sort((p1, p2) => p1.Position.CompareTo(p2.Position));
        //Debug.Log(enemyData == null);
        //Debug.Log(enemyData.Count);
        //if (enemyData.Count > 1)
        //{
        //    enemyData.Sort((p1, p2) => p1.Position.CompareTo(p2.Position));
        //}

        foreach (Unit u in p1UnitList)
        {
            u.gameObject.SetActive(false);
        }

        foreach (Unit u in p2UnitList)
        {
            u.gameObject.SetActive(false);
        }

        foreach (UnitStats u in playerData)
        {
            Unit unitBase = null;
            if(u.Position == 0)
            {
                unitBase = p1UnitList[i1];
                i1++;
            }
            else if (u.Position == 1)
            {
                unitBase = p2UnitList[i2];
                i2++;
            }
            unitBase.gameObject.SetActive(true);
            unitBase.Setup(u);
            PlayerList.Add(unitBase);
            unitList.Add(unitBase);

        }

        foreach(UnitStats u in enemyData)
        {
            Unit unitBase = null;
            Debug.Log(u == null);
            if (u.Position == 0)
            {
                unitBase = p1UnitList[i1];
                i1++;
            }
            else if (u.Position == 1)
            {
                unitBase = p2UnitList[i2];
                i2++;
            }
            unitBase.gameObject.SetActive(true);
            unitBase.Setup(u);
            EnemyList.Add(unitBase);
            unitList.Add(unitBase);
        }


        List<string> ListOfNames = new List<string>();
        int numOfSimilarNames = 0;
        foreach (Enemy e in EnemyList)
        {
            if (!ListOfNames.Contains(e.name))
            {
                ListOfNames.Add(e.name);
                bool SimilarNameExists = false;
                foreach (Enemy i in EnemyList)
                {
                    if(i.name == e.name && i != e)
                    {
                        SimilarNameExists = true;
                        break;
                    }
                }
                if (SimilarNameExists) e.name += " A";
            }
            else
            {
                foreach(string s in ListOfNames)
                {
                    if(e.name == s)
                    {
                        numOfSimilarNames++;
                    }
                }
                char c = (char)(65 + numOfSimilarNames);
                e.name += " " + c.ToString();
            }
        }

        UIManager.GetExp();
        SetDropList();
    }

    void Update()
    {
        //if (Input.GetKeyDown("i"))
        //{
        //    DetermineTurnOrder();
        //}
        //if (Input.GetKeyDown("u"))
        //{
        //    NextTurn();
        //}
    }

    public void UnitCheck(string defeatedUnit, Unit unitDefeated)
    {
        TurnOrder.Remove(unitDefeated);
        if (defeatedUnit == "Player")
        {
            PlayerList.Remove(unitDefeated);
            foreach (Unit u in PlayerList)
            {
                if (u.IsKOd == false)
                {
                    return;
                }
            }
            GameOver("Player");
        }
        else if (defeatedUnit == "Enemy")
        {
            EnemyList.Remove(unitDefeated);
            Enemy e = unitDefeated as Enemy;
            if (e)
            {
                e.ActivateTypeIndicator(false);
            }
            foreach (Unit u in EnemyList)
            {
                if (u.IsKOd == false)
                {
                    return;
                }
            }
            GameOver("Enemy");
        }
    }

    public void GameOver(string losingTeam)
    {
        if (losingTeam == "Player")
        {
            PlayLoseSFX();
            UIManager.BattleEnd("You Lose...",false);
        }
        else if (losingTeam == "Enemy")
        {
            PlayWinSFX();
            UIManager.BattleEnd("You Win!",true);
        }
    }



    public void NextTurn()
    {
        // Makes sure it cycles if unit is KO'd/Remove unit from list if KO'd
        // Removes the current unit and readds at the end of the turn order
        CurrentUnit.TurnFinished();
        TurnOrder.Add(CurrentUnit);
        TurnOrder.RemoveAt(0);
        CurrentUnit = TurnOrder[0];
        UIManager.ChangeName(CurrentUnit.name);
        ReturnSkillToDefault();
        SkillList = CurrentUnit.Unit_Stats.SkillList;
        UIManager.UpdateTurnOrder();
        Debug.Log("Current Unit: " + CurrentUnit.name);
        foreach (Enemy i in EnemyList)
        {
            i.ActivateTypeIndicator(CurrentUnit.name == "Mandy");
        }
        CurrentUnit.TakeTurn();
    }

    void BattleStart()
    {
        CurrentUnit = TurnOrder[0];
        CurrentUnit.TakeTurn();
    }

    void DetermineTurnOrder()
    {
        Debug.Log("StartingBattle");
        foreach (Unit u in unitList)
        { 
            if (TurnOrder.Count > 0)
            {
                for (int x = 0; x < TurnOrder.Count; x++)
                {
                    // < Speed = 1st turn 
                    if (TurnOrder[x].currSpeed < u.currSpeed)
                    {
                        TurnOrder.Insert(x, u);
                        break;
                    }

                }
                if (!TurnOrder.Contains(u))
                {
                    TurnOrder.Add(u);
                }
            }
            else
            {
                TurnOrder.Add(u);
            }
        }
        CurrentUnit = TurnOrder[0];
        UIManager.ChangeName(CurrentUnit.name);
        SkillList = CurrentUnit.Unit_Stats.SkillList;
        CurrentUnit.TakeTurn();
    }

    public void ReturnSkillToDefault()
    {
        if (CurrentUnit.gameObject.tag == "Player") CurrentSkill = PlayerDefaultSkill;
        else if (CurrentUnit.gameObject.tag == "Enemy") CurrentSkill = EnemyDefaultSkill;
        else if (CurrentUnit.gameObject.tag != "Enemy" && CurrentUnit.gameObject.tag != "Player")
        {
            if (CurrentSkill == null) CurrentSkill = PlayerDefaultSkill;
        }

    }

    public void GiveTurn(Unit unitBeingGivenTurn)
    {
        //reAdds the current Unit at the end of the turn order
        TurnOrder.Add(CurrentUnit);
        TurnOrder.RemoveAt(0);
        TurnOrder.Remove(unitBeingGivenTurn);
        TurnOrder.Insert(0, unitBeingGivenTurn);
        CurrentUnit = TurnOrder[0];
    }

    public void UseSkill(Unit target)
    {
        StartCoroutine(UsingSkill(target));
    }

    IEnumerator UsingSkill(Unit target)
    {
        if(CurrentSkill!=null) BattleUIManager.instance.OpenDialogue(CurrentSkill.Dialogue);
        yield return new WaitForSeconds(3);
        Debug.Log(CurrentUnit.name + " is attacking " + target.name);
        switch (CurrentSkill.SkillType)
        {
            case TargetType.Single:
                if (CurrentUnit.Position == 0)
                {
                    if (CurrentUnit.gameObject.tag == "Enemy")
                    {
                        CurrentUnit.animator.StartAttackAnimation("Vaping", () => {
                            CurrentUnit.Movement.Attack(CurrentSkill.AnimationName, target,
                            () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), target); });
                        });
                    }
                    else
                    {
                        CurrentUnit.Movement.Attack(CurrentSkill.AnimationName, target, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), target); });
                    }
                }
                else
                {
                    if (CurrentUnit.gameObject.tag == "Enemy")
                    {
                        CurrentUnit.animator.StartAttackAnimation("Vaping", () =>
                        {
                            CurrentUnit.animator.StartAttackAnimation(CurrentSkill.AnimationName, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), target); BattleUIManager.instance.OpenEvent(); });
                        });
                    }
                    else
                    {
                        CurrentUnit.animator.StartAttackAnimation(CurrentSkill.AnimationName, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), target); BattleUIManager.instance.OpenEvent(); });
                    }
                }
                break;
            case TargetType.Multi:
                List<Unit> TargetList = new List<Unit>();
                if (target.gameObject.tag == "Player")
                {
                    TargetList = PlayerList;
                }
                else if (target.gameObject.tag == "Enemy")
                {
                    TargetList = EnemyList;
                }
                if (CurrentUnit.Position == 0)
                {
                    if (CurrentUnit.gameObject.tag == "Enemy")
                    {
                        CurrentUnit.animator.StartAttackAnimation("Vaping", () =>
                        {
                            CurrentUnit.Movement.Attack(CurrentSkill.AnimationName, target, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), TargetList); });
                        });
                    }
                    else
                    {
                        CurrentUnit.Movement.Attack(CurrentSkill.AnimationName, target, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), TargetList); });
                    }
                }
                else
                {
                    Unit currentTarget = CheckIntervention(target);
                    if (CurrentUnit.gameObject.tag == "Enemy")
                    {
                        CurrentUnit.animator.StartAttackAnimation("Vaping", () =>
                        {
                            CurrentUnit.animator.StartAttackAnimation(CurrentSkill.AnimationName, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), TargetList); BattleUIManager.instance.OpenEvent(); });
                        });
                    }
                    else
                    {
                        CurrentUnit.animator.StartAttackAnimation(CurrentSkill.AnimationName, () => { CurrentSkill.Activate(CurrentUnit.GetAttack(), TargetList); BattleUIManager.instance.OpenEvent(); });
                    }
                }
                break;
        };
    }


    public Unit CheckIntervention(Unit Target)
    {
        //foreach (Unit u in EnemyList)
        //{
        //    if(u.Position == 0)
        //    {
        //        if(Random.Range(0,100) < 20)
        //        {
        //            return u;
        //        }
        //    }
        //}
        // u blocked the Attack for Target
        return Target;
    }
    
    IEnumerator Swapped()
    {
        yield return new WaitForSeconds(1.0f);
        CurrentUnit.TakeTurn();
    }

}

