using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Background { Hallway, Classroom, ComputerRoom, Gym, Rooftop}

public class BackgroundManager : MonoBehaviour
{
    public List<GameObject> Environments;







    public void SetBackground(Background backgroundRequired)
    {
        foreach (GameObject g in Environments)
        {
            g.SetActive(false);
        }

        Environments[(int)backgroundRequired].SetActive(true);
    }

}
