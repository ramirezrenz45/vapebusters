using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTrigger : MonoBehaviour
{
    public string NextSceneName;
    public int ObjectiveNumber;
    public Vector3 LevelExit;


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (ObjectiveTracker.instance.GetObjectiveIndex() <= ObjectiveNumber) ObjectiveTracker.instance.objectiveIndex++;
            FindObjectOfType<BattleInfo>().clearVariables();
           // FindObjectOfType<BattleInfo>().GoToLevel(NextSceneName,new Vector3(0,1.5f,0));
            FindObjectOfType<BattleInfo>().GoToLevel(NextSceneName, LevelExit);
            Debug.Log("Level Exited At: " + LevelExit);
        }
    }
}
