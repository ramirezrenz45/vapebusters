using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoor : MonoBehaviour
{
  [SerializeField]  ItemType KeyItem;
    public GameObject ItemHolder;
    UniqueID uniqueID;
    CollectibleSet collectibleSet;
    public DialogSet DoorDialog;
    public Animator Door1Animator;
    public Animator Door2Animator;
    public Vector3 KeyItemSpawnLocation;
    public EnemyContainers Boss;
    public int ObjectiveIndex;
    public int UnlockedIndex;
    public AudioClip UnlockSFX;
    GameObject SpawnedItem;


    void Awake()
    {
        SpawnedItem = null;
    }


    public void OpenDoor()
    {
        if (Player.Instance.inventory.Isavailable(KeyItem))
        {
            VolumeManager.instance.PlaySFX(UnlockSFX);
            collectibleSet.AddItem(uniqueID.ID);
            Destroy(this.gameObject);
            if(ObjectiveTracker.instance.GetObjectiveIndex() <= UnlockedIndex) ObjectiveTracker.instance.ObjectiveComplete();
            FindObjectOfType<GlobalEvents>().OnMapChange();
            SpawnBoss();
            if (Door1Animator != null)
            {
                Door1Animator.Play("OpenDoor1");
                Door2Animator.Play("OpenDoor2");
            }
        }
        else
        {
            FindObjectOfType<DialogSystem>().EvtDialogStart.Invoke(DoorDialog);
            if (ObjectiveTracker.instance.GetObjectiveIndex() <= ObjectiveIndex)
            {
                ObjectiveTracker.instance.SetObjectiveIndex(ObjectiveIndex);
                ObjectiveTracker.instance.ObjectiveComplete();
            }
            SpawnItem();

        }

    }

    public void SpawnItem()
    {
        Debug.Log("Spawning Item");
        if (SpawnedItem == null) SpawnedItem = Instantiate(ItemHolder);
        Debug.Log(KeyItemSpawnLocation);
        SpawnedItem.transform.position = KeyItemSpawnLocation;

    }



    public void SpawnBoss()
    {
        if (!Player.Instance.GetBossDefeated(Boss.BossNumber))
        {
            Boss.gameObject.SetActive(true);
            Boss.isDead = false;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        uniqueID = GetComponent<UniqueID>();
        collectibleSet = FindObjectOfType<CollectibleSet>();

         if (collectibleSet.CollectedItems.Contains(uniqueID.ID))
        {
            //Destroy(this.gameObject);
            return;
        }
        Debug.Log("Objective Index on Start = " + ObjectiveTracker.instance.GetObjectiveIndex());
        Invoke("CheckObjectiveIndex", 0.1f);
    }

    void CheckObjectiveIndex()
    {
        Debug.Log("Objective Index after delay = " + ObjectiveTracker.instance.GetObjectiveIndex());
        if (ObjectiveTracker.instance.GetObjectiveIndex() == ObjectiveIndex + 1) SpawnItem();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDestroy()
    {
        if (Door1Animator != null)
        {
            Door1Animator.Play("OpenDoor1");
            Door2Animator.Play("OpenDoor2");
        }
    }
}
