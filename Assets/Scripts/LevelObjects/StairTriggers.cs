using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairTriggers : MonoBehaviour
{
     [SerializeField] GameObject Boss;
     public int BossNumber;
     public bool FirstTimeOnStairs;
     public DialogSet StairDialog1;
     public DialogSet StairDialog2;
     string ID;

    // Start is called before the first frame update
    void Start()
    {
        
        ID = Boss.GetComponent<UniqueID>().ID;
        //StartCoroutine(Delay());
        
    }
    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.1f);
        if (FindObjectOfType<Spawner>().GetEnemyData(ID,Boss,true).CooldownTime>0)
        {
            Debug.Log("Boss is Dead");
            Destroy(this.gameObject);
        }

    }

    public void CheckBoss(GameObject bossToCheck)
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (Player.Instance.GetBossDefeated(BossNumber)) 
            { 
                Destroy(this.gameObject);
                return;
            }
            if (FirstTimeOnStairs)
            {
                foreach (StairTriggers s in GameObject.FindObjectsOfType<StairTriggers>())
                {
                    s.FirstTimeOnStairs = false;
                }
                FindObjectOfType<DialogSystem>().EvtDialogStart.Invoke(StairDialog1);
            }
            else {
                FindObjectOfType<DialogSystem>().EvtDialogStart.Invoke(StairDialog2);
            }
            other.GetComponent<Controller>().ForceMove((other.transform.position - this.transform.position).normalized);
        }
    }
   
}
