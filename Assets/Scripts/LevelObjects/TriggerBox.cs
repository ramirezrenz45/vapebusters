using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class TriggerBox : MonoBehaviour
{
   public UnityEvent<Controller> onHitEvt;
    string Id;

    CollectibleSet collectibles;
    // Start is called before the first frame update
    void Start()
    {
        collectibles = FindObjectOfType<CollectibleSet>();
        Id = transform.parent.GetComponent<UniqueID>().ID;
        if (collectibles.CollectedItems.Contains(Id))
        {
            Destroy(this.gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) 
        {
            collectibles.AddItem(Id);
            onHitEvt?.Invoke(other.transform.GetComponent<Controller>());
            Destroy(this.gameObject, 0.1f);
        }
    }



}
