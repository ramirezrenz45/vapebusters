using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SkillButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    public float SkillCost;
    public Skill skill;
    public string SkillDescription;


    public void SetCost(float cost)
    {
        SkillCost = cost;
    }

    public void SetSkill(Skill newSkill)
    {
        skill = newSkill;
        SkillCost = skill.Cost;
        SkillDescription = skill.Description;
    }

    public void CheckCost()
    {
        if (BattleManager.instance.CurrentUnit.currSP < SkillCost)
        {
            GetComponent<Button>().interactable = false;
        }
        else
        {
            GetComponent<Button>().interactable = true;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        BattleUIManager.instance.MoveCursor(this.transform);
        BattleUIManager.instance.OpenDescription(SkillDescription);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        BattleUIManager.instance.CloseDescription();
    }

    public void OnSelect(BaseEventData eventData)
    {
        BattleUIManager.instance.OpenDescription(SkillDescription);
    }


    public void OnDeselect(BaseEventData eventData)
    {
        BattleUIManager.instance.CloseDescription();
    }

}
