using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UnitButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    public int ButtonNumber;
    public void OnPointerEnter(PointerEventData eventData)
    {
        SetOutlineOn(true);
        BattleUIManager.instance.MoveCursor(this.transform);
        BattleUIManager.instance.Indicator.MoveToPosition(BattleUIManager.instance.TargetList[ButtonNumber].transform.position);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        SetOutlineOn(false);
        BattleUIManager.instance.MoveCursor(this.transform);
        BattleUIManager.instance.Indicator.Off();
    }

    public void OnSelect(BaseEventData eventData)
    {
        SetOutlineOn(true);
        BattleUIManager.instance.Indicator.MoveToPosition(BattleUIManager.instance.TargetList[ButtonNumber].transform.position);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        SetOutlineOn(false);
        BattleUIManager.instance.Indicator.Off();
    }

    public void SetOutlineOn(bool outlineOn)
    {
        if(BattleUIManager.instance.TargetList[ButtonNumber]!=null)
        BattleUIManager.instance.TargetList[ButtonNumber].outline.enabled = outlineOn;
    }
}
