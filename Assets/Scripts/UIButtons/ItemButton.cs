using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, ISelectHandler, IDeselectHandler
{
    public ItemType Item;
    public string ItemDescription;

    // Start is called before the first frame update
    void Start()
    {
        if(Item !=null) gameObject.GetComponent<Button>().onClick.AddListener(ButtonPressed);
    }

    public void ButtonPressed()
    {
        BattleInventory.instance.ItemButtonPressed(Item);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        BattleUIManager.instance.MoveCursor(this.transform);
        BattleUIManager.instance.OpenDescription(ItemDescription);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        BattleUIManager.instance.CloseDescription();
    }

    public void OnSelect(BaseEventData eventData)
    {
        BattleUIManager.instance.OpenDescription(ItemDescription);
    }


    public void OnDeselect(BaseEventData eventData)
    {
        BattleUIManager.instance.CloseDescription();
    }
}
