using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattlePlayerButton : MonoBehaviour
{
    public Unit unit;
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SetUp(Unit target)
    {
        unit = target;
        transform.Find("Name").GetComponent<Text>().text = unit.name;
        transform.Find("HP").GetComponent<Text>().text = "CNV " + unit.currHP.ToString() + " / " + unit.Unit_Stats.MaxHP + "  " + "INS " + unit.currSP.ToString() + " / " + unit.Unit_Stats.MaxSp;
    }

    public void OnPress()
    {
        BattleInventory.instance.UseItem(unit);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
