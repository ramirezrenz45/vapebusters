using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EquipmentButton : MonoBehaviour
{
    public Text EquipmentText;
    public Equipment equipment;

    public void ChangeEquipmentText(string equipmentName)
    {
        EquipmentText.text = equipmentName;
    }

    public void Reset()
    {
        GetComponent<Button>().onClick.RemoveAllListeners();
        Destroy(this.gameObject);
    }

    public void OnEnter()
    {
        string description = equipment.name + "(" + equipment.Slot.ToString() + ")" + "\n" + "ATK Boost: " + equipment.AttackMod + "  DEF Boost: " + equipment.DefenseMod + "  SPD Boost: " + equipment.SpeedMod;
        EquipmentManager.instance.OnHover(description);
    }

    public void OnExit()
    {
        EquipmentManager.instance.OnExit();
    }

    public void OnDestroy()
    {
        EquipmentManager.instance.OnExit();
    }
}
