using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventButton : MonoBehaviour
{
    public bool TutorialOngoing = false;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)|| Input.GetMouseButtonDown(0))
        {
            CloseFeedbackText();
        }
    }

    public void CloseFeedbackText()
    {
        if (TutorialOngoing) return;
        gameObject.SetActive(false);
        BattleManager.instance.EventBoxClosed();
    }
}
