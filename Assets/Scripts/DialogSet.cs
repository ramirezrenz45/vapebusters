using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DialogSet : ScriptableObject
{
    public List<Dialogs> dialogs;
}
[System.Serializable]
public class Dialogs
{
    public AvatarType avatarType;
    public string Dialog;

    public string GetString()
    {
      
        return Dialog;
    }

    public string GetSpeaker()
    {  if (avatarType)
        {
            return avatarType.Name;
        }
        else
        {
            return "";
        }
    }
    public Sprite GetAvatar()
    {
        if (avatarType)
        {
            return avatarType.sprite;
        }
        else
        {
            return null;
        }
    }

}