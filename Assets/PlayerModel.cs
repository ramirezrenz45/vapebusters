using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel : MonoBehaviour
{
    public GameObject BillyModel;
    public GameObject MandyModel;
    public Animator BillyAnimator;
    public Animator MandyAnimator;
    Controller control;
    BattleInfo battleInfo;


    void Start()
    {
        control = this.gameObject.GetComponent<Controller>();
        battleInfo = GameObject.FindWithTag("GameManager").GetComponent<BattleInfo>();
        Invoke("delayedCheck",0.1f);
    }

    void delayedCheck()
    {
        ChangePlayerModel(battleInfo.PlayerStats[0].currentPlayerStats.Position == 0);
    }

    public void ChangePlayerModel(bool isBilly)
    {
        if (isBilly)
        {
            BillyModel.SetActive(true);
            MandyModel.SetActive(false);
            control.animator = BillyAnimator;
        }
        else
        {
            MandyModel.SetActive(true);
            BillyModel.SetActive(false);
            control.animator = MandyAnimator;
        }
    }
}
