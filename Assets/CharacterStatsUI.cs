using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class CharacterStatsUI : MonoBehaviour
{
    // Start is called before the first frame update
   public List<TextMeshProUGUI> PlayerStats;
    BattleInfo battleInfo;


    void Start()
    {
        battleInfo= FindObjectOfType<BattleInfo>();
    }



    public void OpenPlayerStat()
    {
        List<PlayerStatsTest> stats = new List<PlayerStatsTest>( battleInfo.PlayerStats);
        for (int i = 0; i < stats.Count; i++)
        {
            PlayerStats[i].text = "Name " + stats[i].currentPlayerStats.UnitName + "\n" + "HP " + stats[i].currentPlayerStats.MaxHP + "\n" + "LP " + stats[i].currentPlayerStats.MaxSp + "\n" + "ATK " + stats[i].currentPlayerStats.Attack + "\n" + "DEF " + stats[i].currentPlayerStats.Defense + "\n" + "SPD " + stats[i].currentPlayerStats.Speed;
        }
       



    }
  




    // Update is called once per frame
    void Update()
    {
        
    }
}
