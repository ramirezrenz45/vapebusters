using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class AvatarType : ScriptableObject
{
  public  string Name;
  public   Sprite sprite;
}
