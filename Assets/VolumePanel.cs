using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VolumePanel : MonoBehaviour
{
    [SerializeField] Slider BGMSlider;
    [SerializeField] Slider SFX;
    public AudioClip CheckSFX;

    // Start is called before the first frame update
    void Start()
    {
        
        BGMSlider.value = (PlayerPrefs.GetFloat("BG")) > 0 ?PlayerPrefs.GetFloat("BG"):0.0001f;
        SFX.value = (PlayerPrefs.GetFloat("SFX")) > 0 ? PlayerPrefs.GetFloat("SFX") : 0.0001f;
        BGMSlider.onValueChanged.AddListener(delegate { onBGMChange(); });
        SFX.onValueChanged.AddListener(delegate { OnSFXChange(); });
    }


    public void onBGMChange()
    {
        VolumeManager.instance.BGMVolumeChange(BGMSlider.value);
    }
    public void OnSFXChange()
    {
        VolumeManager.instance.SFXVolumeChange(SFX.value);
        if (!VolumeManager.instance.SFXSource.isPlaying) VolumeManager.instance.CheckingSFX(CheckSFX);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
