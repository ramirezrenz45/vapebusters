using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStatusPanel : MonoBehaviour
{
    public static CharacterStatusPanel instance = null;

    public CharacterStatus character1;
    public CharacterStatus character2;
    public BattleInfo gameManager;
    public GameObject GameManager;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameObject.FindWithTag("GameManager");
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<BattleInfo>();
        Invoke("UpdateCharacterStatus", 0.1f);
    }

    // Update is called once per frame
    public void UpdateCharacterStatus()
    {
        character1.StatusUpdate(gameManager.PlayerStats[0].currentPlayerStats);
        character2.StatusUpdate(gameManager.PlayerStats[1].currentPlayerStats);
    }
}
