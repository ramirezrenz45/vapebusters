using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class ConsumableUsed : MonoBehaviour
{
    public UnityEvent<PlayerStatsTest> onCharacterChosen= new UnityEvent<PlayerStatsTest>();
    public PlayerStatsTest playerStat;
    // Start is called before the first frame update
    void Start()
    {
       
    }
    public void SetUp(PlayerStatsTest stats)
    {
        playerStat = stats;
        transform.Find("Name").GetComponent<Text>().text= playerStat.currentPlayerStats.UnitName;
        transform.Find("HP").GetComponent<Text>().text ="CNV " + playerStat.currentPlayerStats.CurrentHP.ToString()+"/" + playerStat.currentPlayerStats.MaxHP +"  "+ "INS " + playerStat.currentPlayerStats.CurrentSP.ToString() + "/" + playerStat.currentPlayerStats.MaxSp;


    }

    public void OnPress()
    {
        onCharacterChosen.Invoke(playerStat);


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
