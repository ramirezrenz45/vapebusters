using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FactBoardPanel : MonoBehaviour
{
   public Image image;
   public List<Sprite> CurrentSprites;
//    public DictionaryItems BossAFacts;
//    public DictionaryItems BossBFacts;
    public List<DictionaryItems> facts;
 public Sprite lockedFacts;
    //124679
    public GameObject ButtonPrefab;
    public List<int> uncheckedFacts;
    public List<int> LockedFacts;
    public GameObject backButton;
    public GameObject NextButton;
    public int index = 0;
    List<GameObject> ButtonInstatiated = new List<GameObject>();
    [SerializeField] GameObject listPanel;
     public CollectibleSet collectableset;
    // Start is called before the first frame update
    void Start()
    {
        GlobalEvents.Pause.Invoke();
        //BossAFacts.x = 1;
        //        BossBFacts.x = 2;
        //image.sprite = CurrentSprites[0];



        //OnSetUp();

        
    }

    IEnumerator DelayButtonSpawn()
    {
        yield return new WaitForSeconds(0.1f);
        SetupObject();
    }
    public void SetupObject()
    {


        for (int i = 0; i < facts.Count; i++)
        {
            
            //CurrentSprites = facts[i].sprites;

            //124679
          


            GameObject buttons = Instantiate(ButtonPrefab);
            GameObject g = buttons.transform.Find("Notificationsss").gameObject;
         
            if (uncheckedFacts.Contains(i))
            {
              
                
                    g.SetActive(true);
                
             
              


                //on notification
            }
            else
            {
                g.SetActive(false);
            }
            
           if (LockedFacts.Contains(i))
            {
                Debug.LogError("aaaaaaaaaaaaaaaaaaaaaaa" + uncheckedFacts.Count);
                buttons.GetComponent<Button>().interactable= false;
              //  buttons.transform.Find("Notificationsss").gameObject.SetActive(false);
            }
            buttons.transform.Find("Text (TMP)").GetComponent<TextMeshProUGUI>().text =  facts[i].name;

            Debug.Log("button" + i+ facts[i].name);
            buttons.transform.SetParent(listPanel.transform);
           
                Debug.Log("Staaaaaaaaaap" + i);

            Button BUTTONS = buttons.GetComponent<Button>();
            DictionaryItems fact= facts[i];
            if (BUTTONS&& facts[i]!=null)
            {
                BUTTONS.onClick.AddListener(() => Onselect(fact));
            }
            else
            {
                Debug.Log("adsfafdsafsadfsadfsadfsdafdsafsdafdsadfsdafdsafdsafdsafdsfdsafdsfdsafdsafdsafadsfdsafdsaf");
            }
               buttons.GetComponent<Button>().onClick.AddListener(() => OnSelectA(buttons));
            ButtonInstatiated.Add(buttons);

          //  Onselect(facts[i]);
        }
        CurrentSprites = facts[0].sprites;
        OnSetUp();
    }

    public void hasNotification()
    { 
    
    }
    public void OnSetUp()
    {
        index = 0;
        if (CurrentSprites.Count > 0)
        {

            image.sprite = CurrentSprites[index];
            backButton.SetActive(false);
            NextButton.SetActive(true);
        }


        if (CurrentSprites.Count <= 1)

        {
            backButton.SetActive(false);
            NextButton.SetActive(false);
        }

    }
    public void MovePage(int x)
    {
        index += x;
        if (index >= 0 && index < CurrentSprites.Count)
        {
            image.sprite = CurrentSprites[index];
        }
        if (index <= 0)
        {
            backButton.SetActive(false);
        }
        else
        {
            backButton.SetActive(true);
        }
        if (index >= CurrentSprites.Count - 1)
        {
            NextButton.SetActive(false);
        }
        else
        {
            NextButton.SetActive(true);
        }

    }


        public void Onselect(DictionaryItems items)
    {
      
            CurrentSprites = items.sprites;
        if (CurrentSprites.Count > 0)
        {

            image.sprite = CurrentSprites[0];
        }
            if(!collectableset.CollectedItems.Contains("facts"+ items.name))
             {
        
          collectableset.AddItem("facts" + items.name);
          //  GlobalEvents.DontDes;
             }


        OnSetUp();
    }
    public void OnSelectA(GameObject gameObject)
    {
        GameObject x = gameObject.transform.Find("Notificationsss").gameObject;
     
           x.SetActive(false);
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }


    public void ExitFactboard()
    {
        ObjectiveTracker.instance.notificationCaller();
        GlobalEvents.UnPause.Invoke();
        Destroy(this.gameObject);
    }
}
