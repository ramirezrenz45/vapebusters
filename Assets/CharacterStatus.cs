using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterStatus : MonoBehaviour
{
    public TextMeshProUGUI HPValues;
    public TextMeshProUGUI LPValues;
    public TextMeshProUGUI PlayerLevel;
    public TextMeshProUGUI PlayerName;
    public Image CharacterImage;
    public Image HPBar;
    public Image LPBar;

    

    public void StatusUpdate(PlayerStats playerStats)
    {
        PlayerName.text = playerStats.UnitName;
        HPValues.text = playerStats.CurrentHP + " / " + playerStats.MaxHP;
        LPValues.text = playerStats.CurrentSP + " / " + playerStats.MaxSp;
        PlayerLevel.text = "Level " + playerStats.Level;
        HPBar.fillAmount = playerStats.CurrentHP / playerStats.MaxHP;
        LPBar.fillAmount = playerStats.CurrentSP / playerStats.MaxSp;
        CharacterImage.sprite = playerStats.unitImage;
    }
}
