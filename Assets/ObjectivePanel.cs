using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ObjectivePanel : MonoBehaviour 
{
    public TextMeshProUGUI ObjectiveText;
    public TextMeshProUGUI SecondaryObjectiveText;
    public Image StrikeThroughImage;
    public GameObject SecondaryObjectivePanel;
    public GameObject ObjectiveCompletePanel;
    public TextMeshProUGUI ObjectiveCompleteText;

    

    string nextObjectiveText;

    public void Awake()
    {
        SecondaryObjectivePanel.SetActive(false);
    }

    public void SetObjectiveText(string newObjectiveText)
    {
        Debug.Log("calling Objective Text");
        SecondaryObjectivePanel.SetActive(false);
        ObjectiveCompleteText.color = Color.black;
        if(SecondaryObjectivePanel.activeInHierarchy) SecondaryObjectivePanel.SetActive(false);
        nextObjectiveText = "Current Objective: " + newObjectiveText;
        StartCoroutine("ObjectiveCompleteAnim");
    }

    public void SetSecondaryObjective(string newObjectiveText)
    {
        SecondaryObjectivePanel.SetActive(true);
        StrikeThroughImage.gameObject.SetActive(true);
        StrikeThroughImage.fillAmount = 0;
        SecondaryObjectiveText.text = newObjectiveText;
    }

    void Update()
    {

    }

    public void CompleteSecondaryObjective()
    {
        StartCoroutine("Strikethrough");
    }


    IEnumerator Strikethrough()
    {
        float renderedWidth = SecondaryObjectiveText.preferredWidth;
        RectTransform rt = StrikeThroughImage.rectTransform;
        while(StrikeThroughImage.fillAmount < renderedWidth / rt.sizeDelta.x)
        {
            StrikeThroughImage.fillAmount += 1.0f * Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
        StrikeThroughImage.fillAmount = renderedWidth / rt.sizeDelta.x;
    }


    IEnumerator ObjectiveCompleteAnim()
    {
        Vector3 startPos = ObjectiveCompletePanel.transform.localPosition;
        Debug.Log(startPos);

        while(ObjectiveCompletePanel.transform.localPosition.x > 0)
        {
            ObjectiveCompletePanel.transform.localPosition -= new Vector3(100, 0, 0) * 20 * Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }

        
        ObjectiveText.text = nextObjectiveText;
        float x = 0;
        while (x < 5)
        {
            ObjectiveCompleteText.color = Color.blue;
            yield return new WaitForSeconds(0.1f);
            ObjectiveCompleteText.color = Color.green;
            yield return new WaitForSeconds(0.1f);
            ObjectiveCompleteText.color = Color.red;
            yield return new WaitForSeconds(0.1f);
            ObjectiveCompleteText.color = Color.magenta;
            yield return new WaitForSeconds(0.1f);
            ObjectiveCompleteText.color = Color.cyan;
            yield return new WaitForSeconds(0.1f);
            ObjectiveCompleteText.color = Color.white;
            yield return new WaitForSeconds(0.1f);
            x += 1;
        }

        while(ObjectiveCompletePanel.transform.localPosition.x < startPos.x)
        {
            ObjectiveCompletePanel.transform.localPosition += new Vector3(100, 0, 0) * 20 * Time.deltaTime;
            yield return new WaitForSeconds(0.01f);
        }
    }


}

