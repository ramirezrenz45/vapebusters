using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nurse : MonoBehaviour
{
    public  List<DialogSet> StartingDialogs;
    public List<DialogSet> EndDialog;
    public List<ItemType> ShopMenu;
    UIManager manager;
    DialogSystem dialogSystem;
    // Start is called before the first frame update
    void Start()
    {
        dialogSystem = FindObjectOfType<DialogSystem>();
        manager = FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TalkToNurse()
    {
        dialogSystem.EvtDialogEnd.AddListener(OpenShop);
        if (StartingDialogs.Count > 0)
            dialogSystem.EvtDialogStart.Invoke(StartingDialogs[0]);
    
    }
    public void OpenShop()
    {
        dialogSystem.EvtDialogEnd.RemoveListener(OpenShop);
        manager.NurseShop(ShopMenu,EndDialog[0]);

    }
}
