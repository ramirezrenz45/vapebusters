using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Item/Consumable")]

public class ConsumableItemType : ItemType
{
   public float HealthIncrease;
    public float SPIncreased;

    // Start is called before the first frame update
    public  bool UseConsumable(PlayerStatsTest unit)
    {
        bool isUsed=false;
        if (unit)
        {
            Debug.Log("Player had " + unit.currentPlayerStats.CurrentHP);
            Debug.Log("Gained " + HealthIncrease);
            unit.currentPlayerStats.CurrentHP = (int)Mathf.Min(unit.currentPlayerStats.MaxHP,unit.currentPlayerStats.CurrentHP + HealthIncrease);
            unit.currentPlayerStats.CurrentSP = (int)Mathf.Min(unit.currentPlayerStats.MaxSp, unit.currentPlayerStats.CurrentSP + HealthIncrease);
            Debug.Log("Player now has " + unit.currentPlayerStats.CurrentHP);



            isUsed = true;
            //increasestat
        }



        return isUsed;
    }

    public bool UseConsumable(Unit unit)
    {
        bool isUsed = false;
        if (unit)
        {
            Debug.Log("Used Consumable");
            unit.currHP = (int)Mathf.Min(unit.Unit_Stats.MaxHP, unit.currHP + HealthIncrease);
            unit.currSP = (int)Mathf.Min(unit.Unit_Stats.MaxSp, unit.currSP + HealthIncrease);




            isUsed = true;
            //increasestat
        }



        return isUsed;
    }
}
