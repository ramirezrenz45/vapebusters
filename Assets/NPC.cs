using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
   public List< DialogSet> dialogs;
    DialogSystem dialogSystem;    // Start is called before the first frame update
    void Start()
    {
        dialogSystem = FindObjectOfType<DialogSystem>();   
    }
    public void OninterAct()
    {
        dialogSystem.EvtDialogStart.Invoke(dialogs[Random.Range(0, dialogs.Count)]);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
