using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class VolumeManager : MonoBehaviour
{
    float SFXVolume;
    float BGMVolume;
    public AudioSource BGMSource;
    public AudioSource SFXSource;
    public AudioClip CheckSFX;

    // Start is called before the first frame update
    public static VolumeManager instance = null;


    private void Awake()
    {

        if (instance != null)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        if (PlayerPrefs.GetFloat("SFX") > 0)
        {
           SFXVolume = PlayerPrefs.GetFloat("SFX");
        }
        else
        {
            SFXVolume = 1;
            PlayerPrefs.SetFloat("SFX", 1);
        }


        if (PlayerPrefs.GetFloat("BG") > 0)
        {
            BGMVolume = PlayerPrefs.GetFloat("BG");
        }
        else
        {
            BGMVolume = 1;
            PlayerPrefs.SetFloat("BG", 1);
        }
        BGMSource.volume = BGMVolume;
        SFXSource.volume = SFXVolume;
    }

    public void Start()
    {

    }

    public float GetSFXVolume()
    {
        return SFXVolume;
    }

    public float GetBGMVolume()
    {
        return BGMVolume;
    }

    public void PlaySFX(AudioClip SFXClip)
    {
         SFXSource.PlayOneShot(SFXClip);
    }

    public void CheckingSFX(AudioClip SFXClip)
    {
        SFXSource.Stop();
        SFXSource.clip=SFXClip;
        SFXSource.Play();
    }

    public void PlayBGM(AudioClip BGMClip)
    {
        BGMSource.clip = BGMClip;
        BGMSource.Play();
    }

    public void SFXVolumeChange(float Value)
    {
        PlayerPrefs.SetFloat("SFX", Value);
        SFXVolume = Value;
        SFXSource.volume = Value;
    }
    public void BGMVolumeChange(float Value)
    {
        PlayerPrefs.SetFloat("BG", Value);
        BGMVolume = Value;
        BGMSource.volume = Value;
    }

    public void StopBGM()
    {
        BGMSource.Stop();
    }

    public void PauseBGM()
    {
        BGMSource.Pause();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
