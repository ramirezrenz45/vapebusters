using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class fact_board : MonoBehaviour
{

   // List<string> facts;

   // public Sprite Locked;
   // public List<Sprite> facts;
    public GameObject factPanel;
   public List<DictionaryItems> factlist;
    CollectibleSet CollectibleSet;
    // Start is called before the first frame update

    private void Awake()
    {
       
    }
    public List<int> CheckUncheckedFacts()
    {
        List<int> x= new List<int>();
        Debug.LogError("objective Index"+ ObjectiveTracker.instance.GetObjectiveIndex());
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 0&&!CollectibleSet.CollectedItems.Contains("facts"+factlist[0].name))
        {
            
                x.Add(0);
             


        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 1&& !CollectibleSet.CollectedItems.Contains("facts" + factlist[1].name))
        {
            
                x.Add(1);
           
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 4 && !CollectibleSet.CollectedItems.Contains("facts" + factlist[2].name))
        {

            x.Add(2);

        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 6 && !CollectibleSet.CollectedItems.Contains("facts" + factlist[3].name))
        {


            x.Add(3);
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 7 && !CollectibleSet.CollectedItems.Contains("facts" + factlist[4].name))
        {


            x.Add(4);
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() > 9 && !CollectibleSet.CollectedItems.Contains("facts" + factlist[5].name))
        {

            x.Add(5);

        }

        return x;
    }
    public List<int> CheckLockedFacts()
    {
        List<int> x = new List<int>();
        if (ObjectiveTracker.instance.GetObjectiveIndex() <= 0 )
        {

            x.Add(0);

        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() < 2 )
        {


            x.Add(1);
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() < 5 )
        {

            x.Add(2);

        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() < 7 )
        {


            x.Add(3);
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() < 8)
        {


            x.Add(4);
        }
        if (ObjectiveTracker.instance.GetObjectiveIndex() < 10)
        {

            x.Add(5);

        }

        return x;
    }
    void Start()
    {
        GameObject gameObject = Instantiate(factPanel);


        gameObject.SetActive(false);
              factlist = new List<DictionaryItems>(gameObject.GetComponent<FactBoardPanel>().facts);
        //  Destroy(gameObject,0.1f);

        ObjectiveTracker.instance.factScripts = this;
        CollectibleSet = FindObjectOfType<CollectibleSet>();
        
    }
    IEnumerable delay()
    {
        yield return new WaitForSeconds(0.1f);
        ObjectiveTracker.instance.notificationCaller();
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void OpenPanel()
    {
        //List<Sprite> Sprites = new List<Sprite>();
        //for (int x = 0; x < 2; x++)
        //{
        //    //if(bossdefeated>x)
        //    Sprites.Add(facts[x]);
        //    //else
        //    //sprite.Add(Locked);
        //}
        //if (Player.Instance.playerData.Boss1Defeated)
        //{
        //    Sprites.Add(facts[0]);
        //}
        //else
        //{
        //    Sprites.Add(Locked);
        //}
        //if (Player.Instance.playerData.Boss2Defeated)
        //{
        //    Sprites.Add(facts[1]);
        //}
        //else
        //{
        //    Sprites.Add(Locked);
        //}
        GameObject gameObject= Instantiate(factPanel);

        FactBoardPanel boardPanel = gameObject.GetComponent<FactBoardPanel>();
        boardPanel.uncheckedFacts = new List<int>(CheckUncheckedFacts());
        boardPanel.LockedFacts = new List<int>(CheckLockedFacts());
        boardPanel.collectableset = CollectibleSet;
        boardPanel.SetupObject();
        //TutorialCanvas factCanvas = gameObject.GetComponent<TutorialCanvas>();
        //factCanvas.tutorialImages =new List<Sprite>( Sprites);
        //factCanvas.CurrentImage.sprite = factCanvas.tutorialImages[0];
        //factCanvas.ExitEvt.AddListener(GetPrize);
    }



    public void closePanel()
    {

    }
  

    IEnumerator delayNotif(string items)
    {
        yield return new WaitForSeconds(0.01f);
        FindObjectOfType<DialogSystem>().Notification("You Recieved " + items);
    }

}
